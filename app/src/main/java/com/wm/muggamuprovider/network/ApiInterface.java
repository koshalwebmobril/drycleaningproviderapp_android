package com.wm.muggamuprovider.network;
import com.wm.muggamuprovider.Models.AddAvailibityModel.PostAvailbiltyResponse;
import com.wm.muggamuprovider.Models.GetAllAppoiments.GetAllAppoimentResponse;
import com.wm.muggamuprovider.Models.acceptrejectmodel.AcceptRejectResponse;
import com.wm.muggamuprovider.Models.AddSubItemProviderPriceResponse.AddSubItemProviderPriceResponse;
import com.wm.muggamuprovider.Models.availabilitymanagmentmodel.AvailabilityManagmentResponse;
import com.wm.muggamuprovider.Models.bidacceptmodel.BidAcceptedResponse;
import com.wm.muggamuprovider.Models.bookingdetailsmodel.BookingDetailsResponse;
import com.wm.muggamuprovider.Models.contactusmodel.ContactusResponse;
import com.wm.muggamuprovider.Models.documentdeletemodel.DocumentDeleteResponse;
import com.wm.muggamuprovider.Models.getCategorymodel.GetCategoryResponse;
import com.wm.muggamuprovider.Models.getallbookingmodel.GetAllBookingResponse;
import com.wm.muggamuprovider.Models.getallpostmodel.GetAllPostResponse;
import com.wm.muggamuprovider.Models.getbookingmodel.GetBookingResponse;
import com.wm.muggamuprovider.Models.getdocumentmodel.GetDocumentResponse;
import com.wm.muggamuprovider.Models.updateappoimentmodel.UpdateAppoimentResponse;
import com.wm.muggamuprovider.Models.updateprofilemodel.GetProfileResponse;
import com.wm.muggamuprovider.Models.addservicemodel.AddServiceResponse;
import com.wm.muggamuprovider.Models.changepasswordmodel.ChangePasswordResponse;
import com.wm.muggamuprovider.Models.getprovidersubitemsmodel.GetProviderSubItemResponse;
import com.wm.muggamuprovider.Models.getreviewmodel.GetReviewResponse;
import com.wm.muggamuprovider.Models.getservicecategorymodel.GetServiceCategoryResponse;
import com.wm.muggamuprovider.Models.getserviceslistmodel.GetServiceListResponse;
import com.wm.muggamuprovider.Models.getservicemodel.GetServiceResponse;
import com.wm.muggamuprovider.Models.loginresponse.LoginResponse;
import com.wm.muggamuprovider.Models.notificationmodel.NotificationResponse;
import com.wm.muggamuprovider.Models.paymentlistmodel.PaymentListResponse;
import com.wm.muggamuprovider.Models.postonbidmodel.PostOnBidResponse;
import com.wm.muggamuprovider.Models.postwithbidinfomodel.PostWithBidInfoResponse;
import com.wm.muggamuprovider.Models.otpmodel.RegisterOtpResponse;
import com.wm.muggamuprovider.Models.bidrejectedmodel.BidRejectedResponse;
import com.wm.muggamuprovider.Models.registermodel.ResponseAuthentication;
import com.wm.muggamuprovider.Models.updateprofilemodel.UpdateProfileResponse;
import com.wm.muggamuprovider.Models.updatestatusmodel.UpdateStatusResponse;
import com.wm.muggamuprovider.Models.uploaddocumentmodel.UploadDocumentResponse;
import com.wm.muggamuprovider.Utils.UrlApi;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface
{
    @FormUrlEncoded
    @POST(UrlApi.REGISTER)
    Call<ResponseAuthentication> RegisterUser(@Field("name") String username,
                                              @Field("email") String email,
                                              @Field("mobile") String mobile,
                                              @Field("type") String type,
                                              @Field("password") String password,
                                              @Field("address") String address,
                                              @Field("device_type") String deviceType,
                                              @Field("device_token") String deviceToken,
                                              @Field("latitude") String latitude,
                                              @Field("longitude") String longitude,
                                              @Field("category_id") String category_id);


    @FormUrlEncoded
    @POST(UrlApi.LOGIN)
    Call<LoginResponse> LoginUser(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("type") String type,
                                  @Field("device_type") String deviceType,
                                  @Field("device_token") String deviceToken,
                                  @Field("latitude") String latitude,
                                  @Field("longitude") String longitude);


    @GET(UrlApi.ALLSERVICE)
    Call<GetServiceListResponse> GetServiceList(@Header("Authorization") String token);


    @FormUrlEncoded
    @POST(UrlApi.FORGOTPASSWORD)
    Call<ResponseAuthentication> ForgotPassword (@Field("email") String email,
                                              @Field("type") String type);


    @FormUrlEncoded
    @POST(UrlApi.RESENDOTP)
    Call<ResponseAuthentication> ResendOtp (@Field("email") String email,
                                                 @Field("type") String type);

    @FormUrlEncoded
    @POST(UrlApi.GETPROVIDERSUBITEM)
    Call<GetProviderSubItemResponse> GetProviderSubitems(@Header("Authorization") String token,@Field("service_id") String service_id);


    @FormUrlEncoded
    @POST(UrlApi.ADDSUBITEMAPI)
    Call<AddSubItemProviderPriceResponse> addSubItemProvider(@Header("Authorization") String token, @Field("service_id") String service_id, @Field("subitem_id") String subitem_id, @Field("price") String price);



    @FormUrlEncoded
    @POST(UrlApi.VERIFYOTP)
    Call<ResponseAuthentication> VerifyOtp(@Field("email") String email,
                                                    @Field("otp") String otp);



    @FormUrlEncoded
    @POST(UrlApi.ADDAVAILIBITY)
    Call<PostAvailbiltyResponse> postAvailibity(
                                              @Header("Authorization") String token,
                                              @Field("date") String date,
                                              @Field("time") String strCheckedItem,
                                              @Field("type") String type);


    @FormUrlEncoded
    @POST(UrlApi.VERIFYOTP)
    Call<RegisterOtpResponse>  VerifyRegisterOtp(@Field("email") String email,
                                         @Field("otp") String otp);


    @FormUrlEncoded
    @POST(UrlApi.RESETPASSWORD)
    Call<ResponseAuthentication> ResetPassword (@Field("email") String email, @Field("type") String type,
                                                @Field("password") String password,
                                                @Field("confirm_password") String confirm_password);


    @GET(UrlApi.GETNOTIFICATION)
    Call<NotificationResponse> getNotification(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(UrlApi.GETTIMESLOTS)
    Call<AvailabilityManagmentResponse> getAvailability(@Header("Authorization") String token,
                                                        @Field("date") String date);

    @GET(UrlApi.GETPROFILE)
    Call<GetProfileResponse> GetProfile(@Header("Authorization") String token);


    @GET(UrlApi.GETCATEGORY)
    Call<GetCategoryResponse> GetCategory();


    @GET(UrlApi.GETDOCUMENTS)
    Call<GetDocumentResponse>GetDocument(@Header("Authorization") String token);

    @GET(UrlApi.GETSERVICECATEGORYAPI)
    Call<GetServiceCategoryResponse> getservicecategoryapi();


    @GET(UrlApi.ALLPOSTAPI)
    Call<GetAllPostResponse> getallpostapi(@Header("Authorization") String token);

    @GET(UrlApi.GETALLBOOKINGSServices)
    Call<GetAllBookingResponse> getallbookingServicesapi(@Header("Authorization") String token);


    @GET(UrlApi.GETACCEPTEDAPPOIMENTS)
    Call<GetAllAppoimentResponse> getallappoimenthomeapi(@Header("Authorization") String token);


    @GET(UrlApi.GETALLAPPOIMENT)
    Call<GetAllAppoimentResponse> getallappoimentapi(@Header("Authorization") String token);




    @GET(UrlApi.GETCURRENTSERVICES)
    Call<GetAllBookingResponse> getcurrentServicesapi(@Header("Authorization") String token);

    @GET(UrlApi.GETCOMPLETEDSERVICES)
    Call<GetAllBookingResponse> getCompletedServicesapi(@Header("Authorization") String token);

    @GET(UrlApi.GETINPROGRESSSERVICES)
    Call<GetAllBookingResponse> getInProgressServicesapi(@Header("Authorization") String token);

    @GET(UrlApi.BIDACCEPTEDAPI)
    Call<BidAcceptedResponse> BidAcceptedApi(@Header("Authorization") String token);

    @GET(UrlApi.BIDREJECTEDAPI)
    Call<BidRejectedResponse> BidRejectedApi(@Header("Authorization") String token);

    @GET(UrlApi.GETREVIEWAPI)
    Call<GetReviewResponse> getProviderReview(@Header("Authorization") String token);

    @GET(UrlApi.GETALLBOOKINGS)
    Call<GetBookingResponse> getBookings(@Header("Authorization") String token);


    @GET(UrlApi.PAYMENTTRANSACTION)
    Call<PaymentListResponse> getPaymentList(@Header("Authorization") String token);


    @FormUrlEncoded
    @POST(UrlApi.CHANGEPASSWORDAPI)
    Call<ChangePasswordResponse> changepassword(@Header("Authorization") String token,@Field("old_password") String old_password, @Field("password") String password);


    @FormUrlEncoded
    @POST(UrlApi.SEARCHCATEGORYAPI)
    Call<GetServiceCategoryResponse> searchcategory(@Header("Authorization") String token,@Field("keyword") String keyword);


    @FormUrlEncoded
    @POST(UrlApi.BOOKINGDETAILS)
    Call<BookingDetailsResponse> bookingDetailsApi(@Header("Authorization") String token, @Field("booking_id") String booking_id);


    @FormUrlEncoded
    @POST(UrlApi.AcceptRejectApi)
    Call<AcceptRejectResponse> acceptRejectApi(@Header("Authorization") String token, @Field("booking_id") String booking_id,@Field("action") String action);


    @FormUrlEncoded
    @POST(UrlApi.ACCEPTREJECTAPPOINTMENT)
    Call<AcceptRejectResponse> acceptRejectappointmentApi(@Header("Authorization") String token, @Field("appointment_id") String booking_id,@Field("action") String action);




    @FormUrlEncoded
    @POST(UrlApi.UPDATEBOOKINGSTATUS)
    Call<UpdateStatusResponse> updatestatus(@Header("Authorization") String token, @Field("booking_id") String booking_id, @Field("booking_status") String booking_status);

    @FormUrlEncoded
    @POST(UrlApi.PostBID)
    Call<PostOnBidResponse> postonbid(@Header("Authorization") String token, @Field("post_id") String post_id, @Field("bid_amount") String bid_amount);

    @FormUrlEncoded
    @POST(UrlApi.DELETEDOCUMENT)
    Call<DocumentDeleteResponse> documentdelete(@Header("Authorization") String token, @Field("document_id") int document_id);


    @FormUrlEncoded
    @POST(UrlApi.POSTWITHBIDINFO)
    Call<PostWithBidInfoResponse> postwithbidinfo(@Header("Authorization") String token, @Field("post_id") String post_id);



    @FormUrlEncoded
    @POST(UrlApi.CONTACTUS)
    Call<ContactusResponse> contactus(@Header("Authorization") String token, @Field("subject") String subject, @Field("comments") String comments);





    @GET(UrlApi.GETSERVICE)
    Call<GetServiceResponse> getservice(@Header("Authorization") String token);



    @FormUrlEncoded
    @POST(UrlApi.DeleteServiceApi)
    Call<AddServiceResponse> deleteserviceapi(@Header("Authorization") String token, @Field("provider_service_id") String category_id);


    @Multipart
    @POST(UrlApi.ADDSERVICE)
    Call<AddServiceResponse> addservice(@Header("Authorization") String token,
                                        @Part("category_id")  RequestBody  category_id,
                                        @Part("service_id") RequestBody serviceid,
                                        @Part MultipartBody.Part image);


    @Multipart
    @POST(UrlApi.UPDATESERVICEAPI)
    Call<AddServiceResponse> UPDATESERVICE(@Header("Authorization") String token,
                                        @Part("service_id")  RequestBody  service_id,
                                        @Part MultipartBody.Part image);


    @Multipart
    @POST(UrlApi.UPLOADDOCUMENT)
    Call<UploadDocumentResponse> uploaddocment(@Header("Authorization") String token,
                                                @Part MultipartBody.Part image);

    @Multipart
    @POST(UrlApi.UPDATEPROFILE)
    Call<UpdateProfileResponse> updateprofileresponse(@Header("Authorization") String token,
                                                      @Part("name")  RequestBody  name,
                                                      @Part("address") RequestBody address,
                                                      @Part("open_time") RequestBody open_time,
                                                      @Part("close_time") RequestBody close_time,
                                                      @Part("business_days") RequestBody business_days,
                                                      @Part MultipartBody.Part image,
                                                      @Part("latitude") RequestBody latitude,
                                                      @Part("longitude") RequestBody longitude);



    @FormUrlEncoded
    @POST(UrlApi.UPDATETOKEN)
    Call<ChangePasswordResponse> updatetoken(@Header("Authorization") String token ,
                                            @Field("device_type") String deviceType,
                                            @Field("device_token") String deviceToken);


    @FormUrlEncoded
    @POST(UrlApi.UPDATEAPPOIMENTSTATUS)
    Call<UpdateAppoimentResponse> updateappoimentstatus(@Header("Authorization") String token ,
                                              @Field(" appointment_id") String  appointment_id);
}
