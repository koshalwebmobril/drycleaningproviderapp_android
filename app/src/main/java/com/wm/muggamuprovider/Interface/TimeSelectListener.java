package com.wm.muggamuprovider.Interface;

public interface TimeSelectListener {
    void onSetAvailabilityStatus(int parentPosition, int childPosition, String availabilityStatus);
}
