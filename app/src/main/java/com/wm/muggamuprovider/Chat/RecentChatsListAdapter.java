package com.wm.muggamuprovider.Chat;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.UserLastSeenTime;

import java.util.ArrayList;

public class RecentChatsListAdapter extends RecyclerView.Adapter<RecentChatsListAdapter.MyViewHolder>
{
    ArrayList<RecentChatModel> chatslist;
    Context context;
    public RecentChatsListAdapter(Context context, ArrayList<RecentChatModel> chatslist)
    {
        this.context = context;
        this.chatslist = chatslist;
    }

    @Override
    public RecentChatsListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chats, parent, false);
        RecentChatsListAdapter.MyViewHolder vh = new RecentChatsListAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final RecentChatsListAdapter.MyViewHolder holder, final int position)
    {
        final RecentChatModel albumslistmodel = chatslist.get(position);
        String user_id= LoginPreferences.getActiveInstance(context).getUserId();
        Log.e("tag_use_id",user_id);
        if (albumslistmodel.getFrom().equals(user_id))
        {
            holder.name.setText(albumslistmodel.getReciever_name());
            holder.message.setText(albumslistmodel.getMessage());
            String image=albumslistmodel.getReciever_image();
            UserLastSeenTime userLastSeenTime = new UserLastSeenTime();
            holder.time.setText(userLastSeenTime.getTimeAgo(Long.parseLong(albumslistmodel.getTime()),context));

            if(image.equals(""))
            {
                Glide.with(context)
                        .load("")
                        .error(R.drawable.profile)
                        .into(holder.image_user);
            }
            else
            {
                Glide.with(context)
                        .load(image)
                        .error(R.drawable.profile)
                        .into(holder.image_user);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    String name= chatslist.get(position).getReciever_name();
                    String userid = chatslist.get(position).getReciever_id();
                    String token = chatslist.get(position).getDevice_token();
                    String image = chatslist.get(position).getReciever_image();
                    Intent intent = new Intent(context, ChatDetailsActivity.class);
                    intent.putExtra("name", name);
                    intent.putExtra("userid", userid);
                    intent.putExtra("user_token", token);
                    intent.putExtra("profileimages", image);
                    context.startActivity(intent);
                }
            });

        }
        else
            {
                UserLastSeenTime userLastSeenTime = new UserLastSeenTime();
            holder.name.setText(albumslistmodel.getSender_name());
            holder.message.setText(albumslistmodel.getMessage());
            holder.time.setText(userLastSeenTime.getTimeAgo(Long.parseLong(albumslistmodel.getTime()),context));
            String immmage=albumslistmodel.getSender_image();

            if(immmage.equals(""))
            {
                Glide.with(context)
                        .load("")
                        .error(R.drawable.profile)
                        .into(holder.image_user);
            }
            else
            {
                Glide.with(context)
                        .load(immmage)
                        .error(R.drawable.profile)
                        .into(holder.image_user);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = chatslist.get(position).getSender_name();
                    String id = chatslist.get(position).getFrom();
                    String token= chatslist.get(position).getFcm_token();
                    String image = chatslist.get(position).getSender_image();
                    Intent intent = new Intent(context, ChatDetailsActivity.class);
                    intent.putExtra("name", name);
                    intent.putExtra("userid", id);
                    intent.putExtra("user_token", token);
                    intent.putExtra("profileimages", image);
                    context.startActivity(intent);
                }
            });
            }
    }
    @Override
    public int getItemCount()
    {
        return chatslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name,time,message;
        ImageView image_user;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            image_user= itemView.findViewById(R.id.image_user);
            time= itemView.findViewById(R.id.time);
            name= itemView.findViewById(R.id.name);
            message= itemView.findViewById(R.id.message);
        }
    }


}