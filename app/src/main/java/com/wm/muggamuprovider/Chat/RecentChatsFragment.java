package com.wm.muggamuprovider.Chat;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.service.autofill.UserData;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.wm.muggamuprovider.Activities.MainActivity;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.config.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class RecentChatsFragment extends Fragment
{
    View view;
    RecentChatsListAdapter recentChatAdapter;
    private DatabaseReference recentDatabaseReference;
    UserData userData;
    private String userId="";
    Handler handler=new Handler();
    private ArrayList<RecentChatModel> recentMessageList = new ArrayList<>();
    private RecyclerView recycler_view_messeging;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_message, container, false);
        ((MainActivity)requireActivity()).toolbarHomeOther("Chats");
        ((MainActivity)requireActivity()).updateBottomBar(2);
        init();
        return view;
    }
    private void init()
    {
        recycler_view_messeging=view.findViewById(R.id.recycler_view_messeging);
        recentDatabaseReference = FirebaseDatabase.getInstance().getReference();

      //  fetchMessages();


    }

    private void fetchMessages()
    {
        final ArrayList<String> mKeys = new ArrayList<>();
        recentMessageList.clear();
        new Thread(() ->
        {
            ChildEventListener childEventListener=new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildName) {
                    if (dataSnapshot.exists()){
                        handler.post(() -> {
                            String key = dataSnapshot.getKey();
                            mKeys.add(key);
                            RecentChatModel message = dataSnapshot.getValue(RecentChatModel.class);

                            recentMessageList.add(message);
                            Collections.sort(recentMessageList, new FishNameComparator());
                            recycler_view_messeging.smoothScrollToPosition(recentChatAdapter.getItemCount() - 1);
                            recentChatAdapter.notifyDataSetChanged();
                        });
                    }
                }
                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildName) {
                    if (dataSnapshot.exists()) {
                        RecentChatModel message = dataSnapshot.getValue(RecentChatModel.class);
                        String key = dataSnapshot.getKey();
                        int index = mKeys.indexOf(key);
                        if (recentMessageList.size()>0&&index!=-1&&index<recentMessageList.size()) {
                            recentMessageList.set(index, message);
                        }
                        recentChatAdapter.notifyItemChanged(index);
                        // messageAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            };
            Query receiverQuery=recentDatabaseReference.child(Constant.RECENT_FIREBASE).orderByChild("reciever_id").equalTo(LoginPreferences.getActiveInstance(getActivity()).getUserId());
            Query senderQuery=recentDatabaseReference.child(Constant.RECENT_FIREBASE).orderByChild("from").equalTo(LoginPreferences.getActiveInstance(getActivity()).getUserId());
            receiverQuery.addChildEventListener(childEventListener);
            senderQuery.addChildEventListener(childEventListener);
        }
        ).start();
        if(recentMessageList!=null)
        {
           //txtnocent_chat.setVisibility(View.GONE);
            recycler_view_messeging.setVisibility(View.VISIBLE);
        }else
        {
           // txtnocent_chat.setVisibility(View.VISIBLE);
            recycler_view_messeging.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        fetchMessages();
        recentChatAdapter = new RecentChatsListAdapter(getActivity(),recentMessageList);
        recycler_view_messeging.setHasFixedSize(true);
        recycler_view_messeging.setItemViewCacheSize(25);
        recycler_view_messeging.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL,true));
        recycler_view_messeging.setAdapter(recentChatAdapter);
    }
    public class FishNameComparator implements Comparator<RecentChatModel>
    {
        public int compare(RecentChatModel left, RecentChatModel right) {
            return left.getTime().compareTo(right.getTime());
        }
    }
}