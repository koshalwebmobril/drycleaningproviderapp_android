package com.wm.muggamuprovider.Chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.config.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class ChatDetailsActivity extends AppCompatActivity
{
    UserChatDetailsAdapter userchatdetailsadapter;
    RecyclerView recycler_chat_details;
    ImageView back,user_img;
    TextView txtreciver_name,btn_chat;
    String my_notification_token;
    String my_user_id;
    String my_profile_img,my_user_name;
    EditText chat_edittext;
    private String reciverId, reciverdeviceToken,reciver_name,reciver_image;
    private DatabaseReference rootReference,recentDatabaseReference;
    private ArrayList<ChatModel> messageList=new ArrayList<>();
    Handler handler=new Handler();
    TextView nochat;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_chat_details);
        rootReference = FirebaseDatabase.getInstance().getReference();
        recentDatabaseReference = FirebaseDatabase.getInstance().getReference();
        recycler_chat_details=findViewById(R.id.recycler_chat);
        back=findViewById(R.id.back);
        user_img=findViewById(R.id.user_img);
        txtreciver_name =findViewById(R.id.user_name);
        btn_chat=findViewById(R.id.btn_chat);
        chat_edittext=findViewById(R.id.chat_edittext);
     //   nochat=findViewById(R.id.nochat);


        my_user_id=LoginPreferences.getActiveInstance(ChatDetailsActivity.this).getUserId();
        my_profile_img=LoginPreferences.getActiveInstance(ChatDetailsActivity.this).getUserProfile();
        my_user_name=LoginPreferences.getActiveInstance(ChatDetailsActivity.this).getUserName();

        if(getIntent().getExtras()!=null)
        {
            Glide.with(ChatDetailsActivity.this)
                    .load(getIntent().getStringExtra("profileimages"))
                    .error(R.drawable.profile)
                    .into(user_img);

            txtreciver_name.setText(getIntent().getStringExtra("name"));
            reciverId=getIntent().getStringExtra("userid");
            reciverdeviceToken =getIntent().getStringExtra("user_token");
            reciver_name=getIntent().getStringExtra("name");
            reciver_image=getIntent().getStringExtra("profileimages");

         //   Log.d("tag","onclick"+reciverId);
            Log.d("tag","onclick"+ reciverdeviceToken);
        }

        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_chat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                my_notification_token = FirebaseInstanceId.getInstance().getToken();
                Log.d("notification_token",my_notification_token);

                String strMessage="";
                strMessage=chat_edittext.getText().toString().trim();
                if (TextUtils.isEmpty(strMessage))
                {
                    Toast.makeText(ChatDetailsActivity.this, "Please enter message", Toast.LENGTH_SHORT).show();
                }
                else if(chat_edittext.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(ChatDetailsActivity.this, "No Spaces Allowed", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    sendMessage(strMessage,"Text");
                }
            }
        });

        fetchMessages();
/*
        if(messageList.size()==0)
        {
            recycler_chat_details.setVisibility(View.GONE);
            nochat.setVisibility(View.VISIBLE);
        }
        else
        {
            recycler_chat_details.setVisibility(View.VISIBLE);
            nochat.setVisibility(View.GONE);
        }*/
        userchatdetailsadapter = new UserChatDetailsAdapter(ChatDetailsActivity.this,messageList);
        recycler_chat_details.setHasFixedSize(true);
        recycler_chat_details.setItemViewCacheSize(25);
        recycler_chat_details.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        recycler_chat_details.setAdapter(userchatdetailsadapter);
    }

    private void sendMessage(String message,String type)
    {
        String push_id;
        if(Integer.parseInt(my_user_id) < Integer.parseInt(reciverId))
        {
            push_id = my_user_id + "-" + reciverId;
        }
        else
            {
            push_id = reciverId + "-" + my_user_id;
        }
        final String message_sender_reference = "messages/" + push_id;
        final String message_receiver_reference = "messages/" + push_id;
        final DatabaseReference user_message_key = rootReference.child("messages").child(push_id).push();
        final String forUid_status = reciverId + "_" + "unread";
        final String message_push_id = user_message_key.getKey();
        final Long time = System.currentTimeMillis() / 1000;
        final HashMap<String, Object> message_text_body = new HashMap<>();

        message_text_body.put("message", message);
        message_text_body.put("message_id", message_push_id);
        message_text_body.put("forUid_status", forUid_status);
        message_text_body.put("time", String.valueOf(time));
        message_text_body.put("reciever_id", reciverId);

        if (reciver_image != null && !reciver_image.isEmpty() && !reciver_image.equals("null"))
        {
            message_text_body.put("reciever_image", reciver_image);
        }
        else
        {
            message_text_body.put("reciever_image", "");
        }
        message_text_body.put("device_token",reciverdeviceToken);
        message_text_body.put("fcm_token",my_notification_token);
        message_text_body.put("reciever_name",reciver_name);
        message_text_body.put("type", type);
        message_text_body.put("sender_name", my_user_name);
        message_text_body.put("sender_image", my_profile_img);
        message_text_body.put("from", my_user_id);
        message_text_body.put("sender_id", my_user_id);

        final HashMap<String, Object> messageBodyDetails = new HashMap<>();
        messageBodyDetails.put(message_sender_reference + "/" + message_push_id, message_text_body);
        messageBodyDetails.put(message_receiver_reference + "/" + message_push_id, message_text_body);
        chat_edittext.setText("");

        rootReference.updateChildren(messageBodyDetails, (databaseError, databaseReference) ->
        {
            if (databaseError != null){
                Log.e("Sending message", databaseError.getMessage());
            }else {
                //  input_user_message.setText("");
                chat_edittext.setCursorVisible(true);
                chat_edittext.setEnabled(true);
                sendNotification(message,my_user_id,reciverId,my_user_name);
            }
        });
        recentDatabaseReference.child(Constant.RECENT_FIREBASE).child(push_id).setValue(message_text_body);
    }

    private void fetchMessages()
    {
        final String push_id;
        if(Integer.parseInt(my_user_id)<Integer.parseInt(reciverId))
        {
            push_id=my_user_id+"-"+reciverId;
        }
        else
            {
            push_id=reciverId+"-"+my_user_id;
        }
        final ArrayList<String> mKeys = new ArrayList<>();
        new Thread(() -> {
            rootReference.child("messages").child(push_id)
                    .addChildEventListener(new ChildEventListener()
                    {
                        @Override
                        public void onChildAdded(@NonNull final DataSnapshot dataSnapshot, String s)
                        {
                            if (dataSnapshot.exists())
                            {
                                handler.post(() -> {
                                    String key = dataSnapshot.getKey();
                                    mKeys.add(key);
                                    ChatModel message = dataSnapshot.getValue(ChatModel.class);
                                    messageList.addAll(Collections.singleton(message));
                                    recycler_chat_details.smoothScrollToPosition(userchatdetailsadapter.getItemCount() - 1);
                                    userchatdetailsadapter.notifyDataSetChanged();
                                });
                            }
                        }
                        @Override
                        public void onChildChanged(@NonNull final DataSnapshot dataSnapshot, String s) {
                            if (dataSnapshot.exists()) {
                                ChatModel message = dataSnapshot.getValue(ChatModel.class);
                                String key = dataSnapshot.getKey();
                                int index = mKeys.indexOf(key);
                                if (messageList.size() > 0 && index != -1) {
                                    messageList.set(index, message);
                                }
                                userchatdetailsadapter.notifyItemChanged(index);
                                // messageAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
        }).start();

    }

    private void sendNotification(String message, String messageSenderId, String messageReceiverID, String username) {
        //createNotificationChannel();

        Log.i("tag", "sendNotification: "+my_notification_token);
        Log.i("tag", "sendNotification: "+reciverdeviceToken);

        FcmNotificationBuilder.initialize()
                .title(username)
                .message(message)
                .username(username)
                .type("chat")
                .messageFrom("single")
                .uid(String.valueOf(messageSenderId))
                .recieverUid(String.valueOf(messageReceiverID))
                .firebaseToken(my_notification_token)//sender
                .receiverFirebaseToken(reciverdeviceToken)
                .send();
    }

    public class FishNameComparator implements Comparator<ChatModel>
    {
        public int compare(ChatModel left, ChatModel right) {
            return left.getTime().compareTo(right.getTime());
        }
    }

}