package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Models.forgotpasswordmodel.ForgotPasswordResponse;
import com.wm.muggamuprovider.Models.registermodel.ResponseAuthentication;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView back_arrow;
    EditText new_password,confirm_password;
    Button btn_resetpassword;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        init();

        back_arrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }
    public void init()
    {
        email=getIntent().getStringExtra("email");

        back_arrow=findViewById(R.id.back_arrow);
        new_password=findViewById(R.id.new_password);
        confirm_password=findViewById(R.id.confirm_password);
        btn_resetpassword=findViewById(R.id.btn_resetpassword);
        btn_resetpassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_resetpassword:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                    {
                        hitResetPasswordApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;
        }
    }


    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(new_password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter New Password", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (new_password.getText().toString().trim().length() < 6){
            Toast.makeText(this, "New Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (new_password.getText().toString().trim().length() > 16)
        {
            Toast.makeText(this, "New Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        }

        /*else if(new_password.getText().toString().trim().length()<8 || new_password.getText().toString().trim().length()>16 )
        {
            Toast.makeText(this, "New Password must be more than 7 upto 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }*/

        else if(TextUtils.isEmpty(confirm_password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter Confirm Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (confirm_password.getText().toString().trim().length() < 6){
            Toast.makeText(this, "Confirm Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (confirm_password.getText().toString().trim().length() > 16)
        {
            Toast.makeText(this, "Confirm Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        }


        else if(!confirm_password.getText().toString().trim().equals(new_password.getText().toString().trim()))
        {
            Toast.makeText(this, "New Password and Confirm Password Not Match!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }



    private void  hitResetPasswordApi()
    {
        final ProgressD progressDialog = ProgressD.show(ResetPasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<ResponseAuthentication> call = service.ResetPassword(email, "3",new_password.getText().toString(),confirm_password.getText().toString());
        call.enqueue(new Callback<ResponseAuthentication>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResponseAuthentication> call, retrofit2.Response<ResponseAuthentication> response)
            {
                progressDialog.dismiss();
                try
                {
                    ResponseAuthentication resultFile = response.body();
                    Toast.makeText(ResetPasswordActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if (!resultFile.isError())
                    {

                        ForgotPasswordResponse forgotPasswordResponse =resultFile.getForgotPasswordResponse();
                        Intent i=new Intent(ResetPasswordActivity.this,LoginActivity.class);
                        startActivity(i);
                    }
                    else
                    {
                       // Toast.makeText(ResetPasswordActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Reset Password Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseAuthentication> call, Throwable t)
            {
                Toast.makeText(ResetPasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }



}