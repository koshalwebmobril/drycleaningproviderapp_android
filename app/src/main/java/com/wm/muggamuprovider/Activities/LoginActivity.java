package com.wm.muggamuprovider.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Models.loginresponse.LoginModel;
import com.wm.muggamuprovider.Models.loginresponse.LoginResponse;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.GPSTracker;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.wm.muggamuprovider.Utils.CommonMethod.isValidEmaillId;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{
    Button btn_login;
    TextView signup,forgot_password;
    EditText email,password;
    String notification_token;
    String user_lat,user_lng;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        notification_token = FirebaseInstanceId.getInstance().getToken();
        signup.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                email.setText("");
                password.setText("");
                Intent i=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(i);
            }
        });

        forgot_password.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                email.setText("");
                password.setText("");
                Intent i=new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(i);
            }
        });
    }

    public void init()
    {
        btn_login=findViewById(R.id.btn_login);
        signup=findViewById(R.id.signup);
        forgot_password=findViewById(R.id.forgot_password);
        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_login:
                if (CommonMethod.isOnline(this))
                {
                    notification_token = FirebaseInstanceId.getInstance().getToken();
                    hideKeyboard((Button) v);
                    checkPermission();
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;
        }
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored)
        {
        }
    }


    private boolean validation()
    {
        if (TextUtils.isEmpty(email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter Email id", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (!isValidEmaillId(email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter valid Email id", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (TextUtils.isEmpty(password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (password.getText().toString().trim().length() < 6){
            Toast.makeText(this, "Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (password.getText().toString().trim().length() > 16)
        {
            Toast.makeText(this, "Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void hitLoginUserApi()
    {
        final ProgressD progressDialog = ProgressD.show(LoginActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<LoginResponse> call = service.LoginUser(email.getText().toString().trim(),password.getText().toString().trim() ,"3","2",notification_token,user_lat,user_lng);
        call.enqueue(new Callback<LoginResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    LoginResponse resultFile = response.body();
                    Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        LoginModel loginModellist =resultFile.getLoginModel();                       //get data

                        LoginPreferences.getActiveInstance(LoginActivity.this).setUserName(loginModellist.getName());
                        LoginPreferences.getActiveInstance(LoginActivity.this).setUserEmail(loginModellist.getEmail());
                        LoginPreferences.getActiveInstance(LoginActivity.this).setUserProfile((String) loginModellist.getProfileImagePath());
                        LoginPreferences.getActiveInstance(LoginActivity.this).setToken("Bearer "+ loginModellist.getToken());
                        LoginPreferences.getActiveInstance(LoginActivity.this).setUserId(String.valueOf(loginModellist.getId()));
                        LoginPreferences.getActiveInstance(LoginActivity.this).setCategory_id(loginModellist.getCategory_id());

                        email.setText("");
                        password.setText("");
                        Intent i=new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(i);
                    }
                    else if(resultFile.getCode() == 202)     //Please verify your account. We have send an otp to your email.
                    {
                        Intent i=new Intent(LoginActivity.this,OtpLoginActivity.class);
                        i.putExtra("email",email.getText().toString().trim());
                        startActivity(i);
                        email.setText("");
                        password.setText("");
                    }

                    else if(resultFile.getCode() == 400)     //Please verify your account. We have send an otp to your email.
                    {
                        Intent i=new Intent(LoginActivity.this,OtpLoginActivity.class);
                        i.putExtra("email",email.getText().toString().trim());
                        startActivity(i);
                        email.setText("");
                        password.setText("");
                    }
                    else
                    {
                       // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t)
            {
                Toast.makeText(LoginActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        finish();
    }


    @Override
    protected void onResume()
    {
        super.onResume();
       /* email.setText("");
        password.setText("");*/
    }


    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
            {
               requestPermissions();
            }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
                {
                   toastNeedPermissions();
               }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(LoginActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            if(CommonMethod.isOnline(this))
            {
                if(validation())
                {
                    gpsTracker=new GPSTracker(this);
                    user_lat=String.valueOf(gpsTracker.getLatitude());
                    user_lng=String.valueOf(gpsTracker.getLongitude());
                    hitLoginUserApi();
                }
            }
            else
            {
                CommonMethod.showAlert(getString(R.string.check_internet), this);
            }
        }
    }
}