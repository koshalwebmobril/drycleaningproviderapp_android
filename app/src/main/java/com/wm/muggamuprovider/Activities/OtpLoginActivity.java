package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Models.registermodel.ResponseAuthentication;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class OtpLoginActivity extends AppCompatActivity
{
    Button btn_confirm;
    private OtpTextView otpTextView;
    String email;
    ImageView imageview_back;
    String otpuser;
    TextView resend_otp;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        init();

        btn_confirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(CommonMethod.isOnline(OtpLoginActivity.this))
                {
                    otpuser=otpTextView.getOTP();
                    if(otpuser.length() < 4)
                    {
                        Toast.makeText(OtpLoginActivity.this, "Please enter OTP first", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else
                    {
                        OtpVerifyApi();
                    }
                    hideKeyboard((Button)v);
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), OtpLoginActivity.this);
                }
            }
        });

        otpTextView.setOtpListener(new OTPListener()
        {
            @Override
            public void onInteractionListener()
            {
                // fired when user types something in the Otpbox
            }
            @Override
            public void onOTPComplete(String otp)
            {
                otpuser =otp.toString().trim();
                if(otpuser.length()<4)
                { }
                else
                {
                    hideKeyboard(otpTextView);
                }
            }
        });

        imageview_back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        resend_otp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ResendPasswordApi();
            }
        });
    }
    public void init()
    {
        btn_confirm=findViewById(R.id.btn_confirm);
        otpTextView=findViewById(R.id.otp_view);
        imageview_back=findViewById(R.id.imageview_back);
        resend_otp=findViewById(R.id.resend_otp);
        email=getIntent().getStringExtra("email");
    }



    private void ResendPasswordApi()
    {
        final ProgressD progressDialog = ProgressD.show(OtpLoginActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<ResponseAuthentication> call = service.ResendOtp(email, "3");
        call.enqueue(new Callback<ResponseAuthentication>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResponseAuthentication> call, retrofit2.Response<ResponseAuthentication> response)
            {
                progressDialog.dismiss();
                try
                {
                    ResponseAuthentication resultFile = response.body();
                    if (resultFile.getCode() == 200)
                    {
                        Toast.makeText(OtpLoginActivity.this, getString(R.string.otpresendsuccess), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(OtpLoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseAuthentication> call, Throwable t)
            {
                Toast.makeText(OtpLoginActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void OtpVerifyApi()
    {
        final ProgressD progressDialog = ProgressD.show(OtpLoginActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<ResponseAuthentication> call = service.VerifyOtp(email,otpuser);
        call.enqueue(new Callback<ResponseAuthentication>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResponseAuthentication> call, retrofit2.Response<ResponseAuthentication> response)
            {
                progressDialog.dismiss();
                try
                {
                    ResponseAuthentication resultFile = response.body();
                    Toast.makeText(OtpLoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                    if(resultFile.getCode()==200)
                    {
                        LoginPreferences.getActiveInstance(OtpLoginActivity.this).setUserName(resultFile.getResultLogin().getName());
                        LoginPreferences.getActiveInstance(OtpLoginActivity.this).setUserEmail(resultFile.getResultLogin().getEmail());
                        LoginPreferences.getActiveInstance(OtpLoginActivity.this).setUserProfile( resultFile.getResultLogin().getProfileImagePath());
                        LoginPreferences.getActiveInstance(OtpLoginActivity.this).setToken("Bearer "+ resultFile.getResultLogin().getToken());
                        Intent i=new Intent(OtpLoginActivity.this,MainActivity.class);
                        startActivity(i);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Register Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseAuthentication> call, Throwable t)
            {
                Toast.makeText(OtpLoginActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }
}