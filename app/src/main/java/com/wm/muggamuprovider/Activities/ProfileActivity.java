package com.wm.muggamuprovider.Activities;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.developers.imagezipper.ImageZipper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.muggamuprovider.Adapter.UserSelectDaysAdapter;

import com.wm.muggamuprovider.Models.updateprofilemodel.DaysItemListModel;
import com.wm.muggamuprovider.Models.updateprofilemodel.GetProfileModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.updateprofilemodel.GetProfileResponse;

import com.wm.muggamuprovider.Models.updateprofilemodel.UpdateProfileResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.GPSTracker;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.content.ContentValues.TAG;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final int REQUEST_IMAGE =  999;
    TextView tvtitle;
    EditText company_name,email_address,mobile_number,address;
    TextView open_time_text,close_time_text;
    ImageView ivback;
    ImageView profile_image;
    File imageZipperFile;
    Dialog dialog;
    UserSelectDaysAdapter userSelectDaysAdapter;
    ArrayList<DaysItemListModel> daysitemmodellist;
    RecyclerView recyclerview_days;
    private String strCheckedItem = "";
    Button btn_changes;
    RelativeLayout relativeOpentime,relative_closetime;
    String profileimage;
    private int mHour, mMinute;
    String user_lat,user_lng;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private final static int PLACE_PICKER_REQUEST1 = 1000;
    private int requestCode;
    private int resultCode;
    private Intent data;
    LinearLayout linear_timing;
    String category_type;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
        if(CommonMethod.isOnline(this))
        {
            getProfileApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), this);
        }

        category_type=LoginPreferences.getActiveInstance(ProfileActivity.this).getCategory_id();
        if(category_type.equals(getString(R.string.categorystatus)))
        {
            linear_timing.setVisibility(View.VISIBLE);
        }
        else
        {
            linear_timing.setVisibility(View.GONE);
        }

        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        address.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(address.length()<=0)
                {
                    checkPermission();
                }
                else
                {
                    address.setFocusable(true);
                   // address.setSelection(address.getText().length());
                }
            }
        });



    }
    public void init()
    {
        daysitemmodellist = new ArrayList<DaysItemListModel>();
        tvtitle=findViewById(R.id.tvtitle);
        tvtitle.setText("Profile");
        company_name=findViewById(R.id.company_name);
        email_address=findViewById(R.id.email_address);
        mobile_number=findViewById(R.id.mobile_number);
        address=findViewById(R.id.address);
        ivback=findViewById(R.id.ivback);
        profile_image=findViewById(R.id.profile_image);
        recyclerview_days=findViewById(R.id.recycler1);
        btn_changes=findViewById(R.id.btn_changes);
        open_time_text=findViewById(R.id.open_time_text);
        close_time_text=findViewById(R.id.close_time_text);
        relativeOpentime=findViewById(R.id.relativeopen_time);
        relative_closetime=findViewById(R.id.relative_closetime);
        linear_timing=findViewById(R.id.linear_timing);


        btn_changes.setOnClickListener(this);
        profile_image.setOnClickListener(this);
        relativeOpentime.setOnClickListener(this);
        relative_closetime.setOnClickListener(this);
        linear_timing.setOnClickListener(this);
        email_address.setEnabled(false);
        mobile_number.setEnabled(false);
    }


    public void getProfileApi()
    {
        final ProgressD progressDialog = ProgressD.show(ProfileActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetProfileResponse> call = service.GetProfile(LoginPreferences.getActiveInstance(ProfileActivity.this).getToken());
        call.enqueue(new Callback<GetProfileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetProfileResponse> call, retrofit2.Response<GetProfileResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetProfileResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        GetProfileModel getProfileModel = resultFile.getGetProfileModel();
                        company_name.setText(getProfileModel.getName());
                        email_address.setText(getProfileModel.getEmail());
                        user_lat=getProfileModel.getLatitude().toString();
                        user_lng=getProfileModel.getLongitude().toString();

                        address.setText(getProfileModel.getAddress());
                        mobile_number.setText(getProfileModel.getMobile());
                        profileimage= (String) getProfileModel.getProfileImagePath();
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserEmail(getProfileModel.getEmail());
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserName(getProfileModel.getName());
                        Glide.with(ProfileActivity.this).load(getProfileModel.getProfileImagePath()).error(R.drawable.profile).placeholder(R.drawable.profile)
                                .into(profile_image);
                        open_time_text.setText(String.valueOf(getProfileModel.getOpenTime()));
                        close_time_text.setText(String.valueOf(getProfileModel.getCloseTime()));

                        if(category_type.equals(getString(R.string.categorystatus)))
                        {
                            daysitemmodellist = (ArrayList<DaysItemListModel>) getProfileModel.getDays();
                            LinearLayoutManager gridLayoutManager = new GridLayoutManager(ProfileActivity.this, 4);
                            recyclerview_days.setLayoutManager(gridLayoutManager);
                            userSelectDaysAdapter = new UserSelectDaysAdapter(ProfileActivity.this, daysitemmodellist);
                            recyclerview_days.setAdapter(userSelectDaysAdapter);
                        }
                        else
                        { }
                        userSelectDaysAdapter.setCheckedItemListner(new UserSelectDaysAdapter.ItemCheckListner() {
                            @Override
                            public void setCheckedList(String list) {
                                strCheckedItem = "";
                                Log.d("checked_list", strCheckedItem);
                            }
                            @Override
                            public void remove(DaysItemListModel daysselectitem) {
                                if (daysselectitem.isSelected()) {
                                    if (strCheckedItem.equals("")) {
                                        strCheckedItem = "" + daysselectitem.getDayname();
                                    } else {
                                        strCheckedItem = strCheckedItem + "," + daysselectitem.getDayname();

                                        Log.d("checked_list", strCheckedItem);
                                    }
                                }
                            }
                            @Override
                            public void onItemSelect(int position, int status) {
                                if (status == 1) {
                                    DaysItemListModel selectlangaugeItem = daysitemmodellist.get(position);
                                    selectlangaugeItem.setValue(1);
                                    daysitemmodellist.set(position, selectlangaugeItem);
                                } else {
                                    DaysItemListModel selectlangaugeItem = daysitemmodellist.get(position);
                                    selectlangaugeItem.setValue(0);
                                    daysitemmodellist.set(position, selectlangaugeItem);
                                }
                                userSelectDaysAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(ProfileActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetProfileResponse> call, Throwable t)
            {
                Toast.makeText(ProfileActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }




    private void loadProfile(String url) {
        Log.d("TAG", "Image cache path: " + url);

        Glide.with(this).load(url).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(profile_image);
        profile_image.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.profile_image:
                Dexter.withContext(this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    openDialogToUpdateProfilePIC();
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }
                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
                   break;


            case R.id.btn_changes:
                if(daysitemmodellist.size() == 1)
                {
                    if (daysitemmodellist.get(0).getValue() == 0)
                    {
                        strCheckedItem = String.valueOf(daysitemmodellist.get(0).getDayname());
                    }
                    else
                    {
                        strCheckedItem = "";
                    }
                }

                else if(daysitemmodellist.size() > 1)
                {
                    strCheckedItem = "";
                    for (DaysItemListModel userSelectLanguageModel1 : daysitemmodellist) {
                        if (userSelectLanguageModel1.getValue() == 1) {
                            if (TextUtils.isEmpty(strCheckedItem))
                            {
                                strCheckedItem = String.valueOf(userSelectLanguageModel1.getDayname());
                            }
                            else
                                {
                                strCheckedItem = strCheckedItem + "," + userSelectLanguageModel1.getDayname();
                               }
                        }
                    }
                }
                Log.i("TAG", "onClick1111: " + strCheckedItem);

                if(category_type.equals(getString(R.string.categorystatus)))
                {
                    if(!TextUtils.isEmpty(strCheckedItem))
                    {
                        hitUpdateProfileApi(company_name.getText().toString(),address.getText().toString(),open_time_text.getText().toString()
                                ,close_time_text.getText().toString(),strCheckedItem,imageZipperFile,user_lat,user_lng);
                    }
                    else
                    {
                        Toast.makeText(ProfileActivity.this, " Please select atleast one day", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    hitUpdateProfileApi(company_name.getText().toString(),address.getText().toString(),open_time_text.getText().toString()
                            ,close_time_text.getText().toString(),strCheckedItem,imageZipperFile,user_lat,user_lng);
                }
                break;


            case R.id.relativeopen_time:
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                if(minute<10)
                                {
                                    open_time_text.setText(hourOfDay + ":" +"0"+ minute);
                                }
                                else
                                {
                                    open_time_text.setText(hourOfDay + ":" + minute);
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;

            case R.id.relative_closetime:
                final Calendar c1 = Calendar.getInstance();
                mHour = c1.get(Calendar.HOUR_OF_DAY);
                mMinute = c1.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog1 = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                if(minute<10)
                                {
                                    close_time_text.setText(hourOfDay + ":" +"0"+ minute);
                                }
                                else
                                {
                                    close_time_text.setText(hourOfDay + ":" + minute);
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog1.show();
                break;
        }
    }


    private void launchCameraIntent() {
        Intent intent = new Intent(ProfileActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private final static int PLACE_PICKER_REQUEST = 1000;
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
        checkPermissionOnActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                Uri uri = data.getParcelableExtra("path");
                File file = new File(uri.getPath());
                loadProfile(uri.toString());
                try {
                    imageZipperFile = new ImageZipper(ProfileActivity.this)
                            .setQuality(50)
                            .setMaxWidth(300)
                            .setMaxHeight(300)
                            .compressToFile(file);

                } catch (IOException e) {
                    e.printStackTrace();
                } }
        }
        else if(resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case PLACE_PICKER_REQUEST:
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    Geocoder geocoder = new Geocoder(this);
                    try
                    {
                        List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude,place.getLatLng().longitude, 1);
                        String txtaddress = addresses.get(0).getAddressLine(0);

                        final LatLng latlng = place.getLatLng();
                        String s1= String.valueOf(latlng);
                        String[] s21 = s1.split(":");
                        String s23 = String.valueOf(s21[1]);
                        String s24 = s23.replaceAll("\\(", "");
                        String  address_lat = s24.replaceAll("\\)", "");
                        String[] latlong =  address_lat.split(",");
                        user_lat = latlong[0];
                        user_lng = latlong[1];
                        address.setText(txtaddress);
                        address.setFocusable(true);
                        address.setSelection(address.getText().length());
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
            }
        }
        else if (resultCode == AutocompleteActivity.RESULT_ERROR)
        {
            Status status = Autocomplete.getStatusFromIntent(data);
            Log.i(TAG, status.getStatusMessage());

        } else if (resultCode == RESULT_CANCELED)
        {

        }
    }
    public void openDialogToUpdateProfilePIC()
    {
         dialog = new Dialog(ProfileActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_curved_bg_inset);

        dialog.setContentView(R.layout.dialog_select);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        LinearLayout cameraLayout = dialog.findViewById(R.id.cameraLayout);
        LinearLayout galleryLayout = dialog.findViewById(R.id.galleryLayout);
        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCameraIntent();
                dialog.dismiss();
            }
        });
        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchGalleryIntent();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showSettingsDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
       /* builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });*/

        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    public void hitUpdateProfileApi(String company_name,String address,String open_time,String close_time,
                                    String business_days,File file,String user_lat,String user_lng)
    {
        final ProgressD progressDialog = ProgressD.show(ProfileActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        MultipartBody.Part documentBody = null;
        if (file != null)
        {
            RequestBody document1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            documentBody = MultipartBody.Part.createFormData("profile_image", file.getName(), document1);
        }
        RequestBody categoryname= RequestBody.create(MediaType.parse("multipart/form-data"), company_name);
        RequestBody serviceaddress = RequestBody.create(MediaType.parse("multipart/form-data"), address);
        RequestBody opentime = RequestBody.create(MediaType.parse("multipart/form-data"), open_time);
        RequestBody closetime = RequestBody.create(MediaType.parse("multipart/form-data"), close_time);
        RequestBody businessdays = RequestBody.create(MediaType.parse("multipart/form-data"), business_days);

        RequestBody userlat = RequestBody.create(MediaType.parse("multipart/form-data"), user_lat);
        RequestBody userlng = RequestBody.create(MediaType.parse("multipart/form-data"), user_lng);

        Call<UpdateProfileResponse> call = service.updateprofileresponse(LoginPreferences.getActiveInstance(ProfileActivity.this).getToken()
                ,categoryname,serviceaddress,opentime,closetime,businessdays,documentBody,userlat,userlng);
        call.enqueue(new Callback<UpdateProfileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, retrofit2.Response<UpdateProfileResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    UpdateProfileResponse resultFile = response.body();
                    Toast.makeText(ProfileActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        Intent i=new Intent(ProfileActivity.this,MainActivity.class);
                        startActivity(i);
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserName(resultFile.getprofilemodel().getName());
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserProfile(resultFile.getprofilemodel().getProfileImagePath().toString());
                    }

                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(ProfileActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t)
            {
                Toast.makeText(ProfileActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(ProfileActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
               /* gpsTracker=new GPSTracker(this);
                user_lat=String.valueOf(gpsTracker.getLatitude());
                user_lng=String.valueOf(gpsTracker.getLongitude());*/
            openPlacePicker1();
        }
    }

    public void openPlacePicker1()
    {
        if (!Places.isInitialized())
        {
            Places.initialize(getApplicationContext(),getResources().getString(R.string.googlekey));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
        startActivityForResult(intent, PLACE_PICKER_REQUEST1);
    }
    private void checkPermissionOnActivityResult(int requestCode, int resultCode, Intent data)
    {
    }

}