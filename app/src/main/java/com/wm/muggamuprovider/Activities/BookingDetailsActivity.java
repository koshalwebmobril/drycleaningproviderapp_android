package com.wm.muggamuprovider.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Adapter.BookingDetailsParentAdapter;
import com.wm.muggamuprovider.Chat.ChatDetailsActivity;
import com.wm.muggamuprovider.Models.bookingdetailsmodel.BookingDetailsModel;
import com.wm.muggamuprovider.Models.bookingdetailsmodel.BookingDetailsParentModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.acceptrejectmodel.AcceptRejectResponse;
import com.wm.muggamuprovider.Models.bookingdetailsmodel.BookingDetailsResponse;
import com.wm.muggamuprovider.Models.updatestatusmodel.UpdateStatusResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.view.View.GONE;

public class BookingDetailsActivity extends AppCompatActivity implements View.OnClickListener
{
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.Change_Password)
    TextView ChangePassword;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.user_image)
    ImageView userImage;
    @BindView(R.id.provider_name)
    TextView providerName;
    @BindView(R.id.contact_no)
    TextView contactNo;
    @BindView(R.id.created_date)
    TextView createdDate;
    @BindView(R.id.order_no)
    TextView orderNo;
    @BindView(R.id.view_more)
    ImageView viewMore;
    @BindView(R.id.view_more_hide)
    ImageView viewMoreHide;
    @BindView(R.id.linear_service_details)
    LinearLayout linearServiceDetails;
    @BindView(R.id.txt_pickup_location)
    TextView txtPickupLocation;
    @BindView(R.id.txt_pickuptime)
    TextView txtPickuptime;
    @BindView(R.id.txt_pickupaddress)
    TextView txtPickupaddress;
    @BindView(R.id.txtdelivery_date)
    TextView txtdeliveryDate;
    @BindView(R.id.txt_delivery_time)
    TextView txtDeliveryTime;
    @BindView(R.id.btn_reject)
    Button btnReject;
    @BindView(R.id.linear_btn_layout)
    LinearLayout linearBtnLayout;
    @BindView(R.id.btn_accept)
    Button btnAccept;
    @BindView(R.id.recyclerview_yourbooking)
    RecyclerView recyclerviewYourbooking;
    BookingDetailsParentAdapter bookingDetailsParentAdapter;
    @BindView(R.id.total_amount)
    TextView totalAmount;
    @BindView(R.id.relative_total_amount)
    RelativeLayout relativeTotalAmount;
    @BindView(R.id.txt_delivery_amount)
    TextView txtDeliveryAmount;
    @BindView(R.id.provider_status)
    TextView provider_status;

    @BindView(R.id.linear_caller)
    LinearLayout linear_caller;

    @BindView(R.id.chat_icon)
    ImageView chat_icon;

    @BindView(R.id.status)
    TextView status;

    int booking_status;
    BookingDetailsModel bookingdetails;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);
        ButterKnife.bind(this);
        getBookingDetailsApi();
    }

    @OnClick({R.id.view_more, R.id.view_more_hide, R.id.back, R.id.btn_reject, R.id.btn_accept,R.id.provider_status,
            R.id.linear_caller,R.id.chat_icon})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.view_more:
                linearServiceDetails.setVisibility(View.VISIBLE);
                viewMore.setVisibility(GONE);
                viewMoreHide.setVisibility(View.VISIBLE);
                break;

            case R.id.view_more_hide:
                linearServiceDetails.setVisibility(GONE);
                viewMoreHide.setVisibility(GONE);
                viewMore.setVisibility(View.VISIBLE);
                break;


            case R.id.back:
                finish();
                break;

            case R.id.chat_icon:
                Intent i=new Intent(BookingDetailsActivity.this, ChatDetailsActivity.class);
                i.putExtra("name",bookingdetails.getUserName());
                i.putExtra("userid",String.valueOf(bookingdetails.getUserId()));
                i.putExtra("user_token", bookingdetails.getUser_device_token());
                i.putExtra("profileimages", bookingdetails.getUserImage());
                startActivity(i);
                break;

            case R.id.btn_reject:
                hitAcceptRejectApi("0");
                break;

            case R.id.btn_accept:
                hitAcceptRejectApi("1");
                break;

            case R.id.linear_caller:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", contactNo.getText().toString().trim(), null));
                startActivity(intent);
                break;

            case R.id.provider_status:
                     PopupMenu popup = new PopupMenu(BookingDetailsActivity.this, view, Gravity.END);
                      if(booking_status==9)
                        {
                            popup.getMenu().add(1, R.id.Start, 1, "Start");
                        }
                        else if(booking_status==4)
                        {
                            popup.getMenu().add(2, R.id.InProgress, 2, "InProgress");
                        }
                        else if(booking_status==5)
                        {
                            popup.getMenu().add(3, R.id.completed, 3, "Completed");
                        }
                        else
                        {
                            provider_status.setVisibility(GONE);
                        }
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item)
                            {
                                switch (item.getItemId())
                                {
                                    case R.id.Start:
                                        hitupdateorderstatusapi("1");
                                        break;

                                    case R.id.InProgress:
                                        hitupdateorderstatusapi("2");
                                        break;

                                    case R.id.completed:
                                        hitupdateorderstatusapi("3");
                                        break;
                                }
                                return false;
                            }
                        });
                        popup.show();

        }
    }

    private void getBookingDetailsApi()
    {
        final ProgressD progressDialog = ProgressD.show(BookingDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<BookingDetailsResponse> call = service.bookingDetailsApi(LoginPreferences.getActiveInstance(this).getToken(), getIntent().getStringExtra("booking_id"));
        call.enqueue(new Callback<BookingDetailsResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<BookingDetailsResponse> call, Response<BookingDetailsResponse> response) {
                progressDialog.dismiss();
                try {
                    BookingDetailsResponse resultFile = response.body();
                    // Toast.makeText(BookingDetails.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                    if (resultFile.getCode() == 200) {
                         bookingdetails = resultFile.getBookingDetailsModel();
                        Glide.with(BookingDetailsActivity.this)
                                .load(bookingdetails.getUserImage()).apply(new RequestOptions().placeholder(R.drawable.user_profile_service).error(R.drawable.user_profile_service))
                                .into(userImage);
                        providerName.setText(bookingdetails.getUserName());
                        contactNo.setText(bookingdetails.getUserMobile());
                        createdDate.setText(bookingdetails.getCreatedAt());
                        orderNo.setText("Order No -: " + getString(R.string.slashicon)+ bookingdetails.getOrderId());
                        txtPickupLocation.setText(bookingdetails.getPickupDate());
                        txtPickuptime.setText(bookingdetails.getPickupTime());
                        txtPickupaddress.setText(bookingdetails.getPickupLocation());
                        txtdeliveryDate.setText(bookingdetails.getDeliveryDate());
                        txtDeliveryTime.setText(bookingdetails.getDeliveryTime());
                        booking_status=bookingdetails.getBooking_status();

                        String total_amount_string = String.format("%.2f", bookingdetails.getTotalAmount());
                        totalAmount.setText(getString(R.string.currency_icon)+" "+ total_amount_string);

                        String deliver_charge = String.format("%.2f", bookingdetails.getDeliveryFee());
                        txtDeliveryAmount.setText(getString(R.string.currency_icon)+" "+ deliver_charge);


                        List<BookingDetailsParentModel> parentlist = resultFile.getItemDetails();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(BookingDetailsActivity.this, RecyclerView.VERTICAL, false);
                        recyclerviewYourbooking.setLayoutManager(mLayoutManager);
                        bookingDetailsParentAdapter = new BookingDetailsParentAdapter(BookingDetailsActivity.this, parentlist);
                        recyclerviewYourbooking.setAdapter(bookingDetailsParentAdapter);

                        if(bookingdetails.getIsProviderAccepted() == 2)
                        {
                            linearBtnLayout.setVisibility(View.VISIBLE);
                            provider_status.setVisibility(GONE);
                            chat_icon.setVisibility(GONE);
                        }
                        else if(bookingdetails.getIsProviderAccepted()==0)
                        {
                            provider_status.setVisibility(GONE);
                        }
                        else
                            {
                              linearBtnLayout.setVisibility(GONE);
                              chat_icon.setVisibility(View.VISIBLE);
                            }
                        if(booking_status==9)
                        {
                            provider_status.setVisibility(View.VISIBLE);
                        }
                        else if(booking_status==4)
                        {
                            provider_status.setVisibility(View.VISIBLE);
                        }
                        else if(booking_status==5)
                        {
                            provider_status.setVisibility(View.VISIBLE);
                        }
                        else
                            {
                               provider_status.setVisibility(GONE);
                            }

                        if(booking_status==12)
                        {
                            status.setTextColor(Color.parseColor("#2CA86C"));
                            status.setText("Completed");
                        }
                        else if(booking_status==3)
                        {
                            status.setTextColor(Color.parseColor("#2CA86C"));
                            status.setText("Cancelled");
                        }
                        else
                        {
                            status.setTextColor(Color.parseColor("#EE3232"));
                            status.setText("Pending");
                        }
                    }
                    else
                        {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                } catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<BookingDetailsResponse> call, Throwable t) {
                Toast.makeText(BookingDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void hitAcceptRejectApi(String action)
    {
        final ProgressD progressDialog = ProgressD.show(BookingDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AcceptRejectResponse> call = service.acceptRejectApi(LoginPreferences.getActiveInstance(this).getToken(),getIntent().getStringExtra("booking_id"),action);
        call.enqueue(new Callback<AcceptRejectResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AcceptRejectResponse> call, Response<AcceptRejectResponse> response) {
                progressDialog.dismiss();
                try {
                    AcceptRejectResponse resultFile = response.body();
                     Toast.makeText(BookingDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                         // finish();
                        linearBtnLayout.setVisibility(GONE);
                        Intent i=new Intent(BookingDetailsActivity.this,MainActivity.class);
                        i.putExtra("page_status","2");
                        startActivity(i);
                    }
                    else
                        {

                        }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AcceptRejectResponse> call, Throwable t) {
                Toast.makeText(BookingDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void hitupdateorderstatusapi(String action)
    {
        final ProgressD progressDialog = ProgressD.show(BookingDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<UpdateStatusResponse> call = service.updatestatus(LoginPreferences.getActiveInstance(this).getToken(),getIntent().getStringExtra("booking_id"),action);
        call.enqueue(new Callback<UpdateStatusResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UpdateStatusResponse> call, Response<UpdateStatusResponse> response) {
                progressDialog.dismiss();
                try {
                    UpdateStatusResponse resultFile = response.body();
                    Toast.makeText(BookingDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        finish();
                    }
                    else
                    {

                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<UpdateStatusResponse> call, Throwable t) {
                Toast.makeText(BookingDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

}