package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Adapter.TimeListAdapter;
import com.wm.muggamuprovider.Interface.TimeSelectListener;
import com.wm.muggamuprovider.Models.availabilitymanagmentmodel.AvailabilityManagmentResponse;
import com.wm.muggamuprovider.Models.availabilitymanagmentmodel.TimeSlotsItem;
import com.wm.muggamuprovider.Models.AddAvailibityModel.PostAvailbiltyResponse;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;


import com.wm.muggamuprovider.databinding.ActivityAvailabilityManagmentBinding;
import com.wm.muggamuprovider.network.ApiInterface;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import noman.weekcalendar.listener.OnDateClickListener;
import noman.weekcalendar.listener.OnWeekChangeListener;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.wm.muggamuprovider.R.color.byweekgrey;
import static com.wm.muggamuprovider.R.color.login_bg_color;

public class AvailabilityManagmentActivity extends AppCompatActivity implements View.OnClickListener, TimeSelectListener {
    ActivityAvailabilityManagmentBinding binding;
    TimeSelectListener timeSelectListener;
    private long selectedDateInMilis;
    private long currentDateTimeInMillis;
    String selectedDate;
    String currentDate;
    DateTime currentSelectedDate;
    int count = 0;
    TimeListAdapter timeListAdapter;
    public List<TimeSlotsItem> timeList = new ArrayList<>();
    private List<String> timesIdsList;
    private String strCheckedItem = "";
    private String booking_status = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_availability_managment);
        binding.weekCalendar.reset();
        timeSelectListener = this;
        init();
        timesIdsList = new ArrayList<>();
        if(CommonMethod.isOnline(AvailabilityManagmentActivity.this))
        {
            getTimeSlotsApi(selectedDate);
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), AvailabilityManagmentActivity.this);
        }

        binding.back.setOnClickListener(this);
        binding.nextArrow.setOnClickListener(this);
        binding.previousArrow.setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);
        binding.byweekkey.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceType")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;

            case R.id.byweekkey:
                if (booking_status.equals("1"))
                {
                    booking_status = "2";
                    binding.byweekkey.setTextColor(getResources().getColor(login_bg_color));
                } else {
                    booking_status = "1";
                    binding.byweekkey.setTextColor(getResources().getColor(byweekgrey));
                }
                break;

            case R.id.nextArrow:
                binding.weekCalendar.moveToNext();
                currentSelectedDate = currentSelectedDate.plusDays(1);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                selectedDate = dateFormat.format(new Date(currentSelectedDate.getMillis()));
                Log.i("TAG", "onDateClick: " + selectedDate);
                selectedDateInMilis = currentSelectedDate.getMillis();

                timesIdsList = new ArrayList<>();
                getTimeSlotsApi(selectedDate);
                break;

            case R.id.previousArrow:
                binding.weekCalendar.moveToPrevious();
                currentSelectedDate = currentSelectedDate.plusDays(-1);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
                selectedDate = dateFormat1.format(new Date(currentSelectedDate.getMillis()));
                selectedDateInMilis = currentSelectedDate.getMillis();
                timesIdsList = new ArrayList<>();
                getTimeSlotsApi(selectedDate);
                break;

            case R.id.btn_save:
                Log.i("onDate", "onDateClickselected: " + timesIdsList);
                Log.i("onDate", "onDateClickselected: " + selectedDate);
                Log.i("onDate", "onDateClickselected: " + booking_status);
                strCheckedItem = "";
                timesIdsList = new ArrayList<>();
                for (int i = 0; i < timeList.size(); i++)
                {
                    for (int j = 0; j < timeList.get(i).getSlots().size(); j++) {
                        if (timeList.get(i).getSlots().get(j).getAvailability_status().equals("2")) {
                            timesIdsList.add(String.valueOf(timeList.get(i).getSlots().get(j).getId()));
                        }
                    }
                }
                String str = timesIdsList.stream().collect(Collectors.joining(","));
                Log.i("onDate1", "onDateClickselected111: " + str);

                hitsubmitTimeSlotsApi(selectedDate, str, String.valueOf(booking_status));
                break;
        }
    }

    private void init() {
        setCurrentDateTime();
        binding.weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                currentSelectedDate = dateTime;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                selectedDateInMilis = dateTime.getMillis();
                selectedDate = dateFormat.format(new Date(dateTime.getMillis()));
                Log.i("TAG", "SandeepSelected: " + selectedDateInMilis);

                timesIdsList = new ArrayList<>();
                getTimeSlotsApi(selectedDate);
            }
        });

        binding.weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(DateTime firstDayOfTheWeek, boolean forward) {
                if (count > 0) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM/dd/yyyy");
                    String myDate = dateFormat.format(new Date(firstDayOfTheWeek.getMillis()));
                    String weekLastDate = dateFormat.format(new Date(firstDayOfTheWeek.plusDays(7).getMillis()));

                    String currentMonth = myDate.substring(0, myDate.indexOf("/"));
                    String weekLastDayMonth = weekLastDate.substring(0, weekLastDate.indexOf("/"));
                    Log.i("TAG", "onWeekChange1: " + currentMonth);
                    Log.i("TAG", "onWeekChange12: " + weekLastDayMonth);

                    if (currentMonth.equals(weekLastDayMonth)) {
                        binding.monthTV.setText(currentMonth);
                    } else {
                        binding.monthTV.setText(currentMonth + "/" + weekLastDayMonth);
                    }
                } else {
                    count = 1;
                }

            }
        });
    }

    private void setCurrentDateTime() {
        DateTime dateTime = DateTime.now();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        selectedDateInMilis = dateTime.getMillis();
        selectedDate = dateFormat.format(new Date(dateTime.getMillis()));
        currentDate = selectedDate;
        // currentDateTimeInMillis = dateTime.getMillis();
        // selectedDateInMilis = dateTime.getMillis();
        currentSelectedDate = DateTime.now();
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("MMMM/dd/yyyy");
        String formattedDate = df.format(c);
        String currentMonth = formattedDate.substring(0, formattedDate.indexOf("/"));
        binding.monthTV.setText(currentMonth);
        Log.d("current_time", currentMonth);
        /*SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        String selectedDate = df1.format(c);
        Log.i("TAG", "setCurrentDateTime: " + selectedDate);
        currentDate = selectedDate;
        this.selectedDate = currentDate;
        currentSelectedDate = DateTime.now();*/

        Log.i("TAG", "setCurrentDateTime: " + currentSelectedDate);
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = (Date) formatter.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        currentDateTimeInMillis = date.getTime();
        selectedDateInMilis = currentDateTimeInMillis;
        Log.i("TAG", "SandeepCurrent: " + currentDateTimeInMillis);
    }


    private void hitsubmitTimeSlotsApi(String selectedDate, String strCheckedItem, String booking_status)
    {
        final ProgressD progressDialog = ProgressD.show(AvailabilityManagmentActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<PostAvailbiltyResponse> call = service.postAvailibity(LoginPreferences.getActiveInstance(this).getToken(), selectedDate, strCheckedItem, booking_status);
        call.enqueue(new Callback<PostAvailbiltyResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<PostAvailbiltyResponse> call, retrofit2.Response<PostAvailbiltyResponse> response) {
                progressDialog.dismiss();
                try {
                    PostAvailbiltyResponse resultFile = response.body();
                    Toast.makeText(AvailabilityManagmentActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if (resultFile.getCode() == 200) {
                        finish();
                    } else {
                    }
                } catch (Exception e) {
                    Toast.makeText(AvailabilityManagmentActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostAvailbiltyResponse> call, Throwable t) {
                Toast.makeText(AvailabilityManagmentActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void getTimeSlotsApi(String selectedDate) {
        final ProgressD progressDialog = ProgressD.show(AvailabilityManagmentActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AvailabilityManagmentResponse> call = service.getAvailability(LoginPreferences.getActiveInstance(this).getToken(), selectedDate);
        call.enqueue(new Callback<AvailabilityManagmentResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AvailabilityManagmentResponse> call, retrofit2.Response<AvailabilityManagmentResponse> response) {
                progressDialog.dismiss();
                try {
                    AvailabilityManagmentResponse resultFile = response.body();
                    if (resultFile.getCode() == 200) {
                        timeList = resultFile.getTimeSlots();

                        binding.timeRecyclerView.setLayoutManager(new LinearLayoutManager(AvailabilityManagmentActivity.this, LinearLayoutManager.VERTICAL, false));
                        timeListAdapter = new TimeListAdapter(AvailabilityManagmentActivity.this, timeList, timeSelectListener);
                        binding.timeRecyclerView.setAdapter(timeListAdapter);
                    } else {
                    }
                } catch (Exception e) {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AvailabilityManagmentResponse> call, Throwable t) {
                Toast.makeText(AvailabilityManagmentActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
    @Override
    public void onSetAvailabilityStatus(int parentPosition, int childPosition, String availabilityStatus)
    {
        timeList.get(parentPosition).getSlots().get(childPosition).setAvailability_status(availabilityStatus);
        timeListAdapter.notifyDataSetChanged();
    }
}
