package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Models.forgotpasswordmodel.ForgotPasswordResponse;
import com.wm.muggamuprovider.Models.registermodel.ResponseAuthentication;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.wm.muggamuprovider.Utils.CommonMethod.isValidEmaillId;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView back_arrow;
    Button btn_send;
    EditText email;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();

    }
    public void init()
    {
        back_arrow=findViewById(R.id.back_arrow);
        btn_send=findViewById(R.id.btn_send);
        email=findViewById(R.id.email);
        btn_send.setOnClickListener(this);
        back_arrow.setOnClickListener(this);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_send:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((Button) v);
                    if (validation())
                    {
                        ForgotPasswordApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;

            case R.id.back_arrow:
                finish();
                break;
        }
    }

    private void ForgotPasswordApi()
    {
        final ProgressD progressDialog = ProgressD.show(ForgotPasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<ResponseAuthentication> call = service.ForgotPassword(email.getText().toString().trim(), "3");
        call.enqueue(new Callback<ResponseAuthentication>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResponseAuthentication> call, retrofit2.Response<ResponseAuthentication> response)
            {
                progressDialog.dismiss();
                try
                {
                    ResponseAuthentication resultFile = response.body();
                    Toast.makeText(ForgotPasswordActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                    if (resultFile.getCode() == 200)
                    {
                        ForgotPasswordResponse forgotPasswordResponse =resultFile.getForgotPasswordResponse();
                        Intent i=new Intent(ForgotPasswordActivity.this, OtpRegisterActivity.class);
                        i.putExtra("email",email.getText().toString().trim());
                        startActivity(i);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseAuthentication> call, Throwable t)
            {
                Toast.makeText(ForgotPasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }


    private boolean validation()
    {
        if (TextUtils.isEmpty(email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter Email id", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (!isValidEmaillId(email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter valid Email id", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        email.setText("");
    }
}