package com.wm.muggamuprovider.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.wm.muggamuprovider.Fragments.BookingManagmentFragment;
import com.wm.muggamuprovider.Fragments.HomeAppoimentFragment;
import com.wm.muggamuprovider.Fragments.HomeDryCleaningFragment;
import com.wm.muggamuprovider.Chat.RecentChatsFragment;
import com.wm.muggamuprovider.Fragments.NotificationFragment;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.databinding.ActivityMainBinding;


import java.util.List;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    ActivityMainBinding binding;
    String company_name,company_mail,Profileimg;
    String category_id;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.layoutContent.toolbar.toolbarMain);
        init();

        if(getIntent().getExtras() != null)
        {
           if(getIntent().getStringExtra("page_status").equals("2"))
           {
               Fragment  currentFragment = new BookingManagmentFragment();
               loadFragmentOther(currentFragment);
           }
        }
        else                                                                        //appoiment module open
        {
            if(LoginPreferences.getActiveInstance(this).getCategory_id().equals(getString(R.string.categorystatus)))
            {
                Fragment homeFragment = new HomeDryCleaningFragment();
                loadHomeFragment(homeFragment);
            }
            else
            {
                Fragment homeappoimentFragment = new HomeAppoimentFragment();
                loadHomeFragment(homeappoimentFragment);
            }
        }
        if(LoginPreferences.getActiveInstance(this).getCategory_id().equals(getString(R.string.categorystatus)))
        {
            binding.drawerMenuItems.relativeavailibitymanagement.setVisibility(GONE);
        }
        else
        {
            binding.drawerMenuItems.relativemyservice.setVisibility(GONE);
        }


    }
    private void init()
    {
        company_name= LoginPreferences.getActiveInstance(MainActivity.this).getUserName();
        company_mail=LoginPreferences.getActiveInstance(MainActivity.this).getUserEmail();
        Profileimg=LoginPreferences.getActiveInstance(MainActivity.this).getUserProfile();
        category_id =LoginPreferences.getActiveInstance(MainActivity.this).getCategory_id();

        binding.drawerMenuItems.firstname.setText(company_name);
        binding.drawerMenuItems.gmail.setText(company_mail);
        Glide.with(MainActivity.this).load(Profileimg).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(binding.drawerMenuItems.providerimg);

        binding.layoutContent.toolbar.imgMenu.setOnClickListener(MainActivity.this);
        binding.drawerMenuItems.relativeProfile.setOnClickListener(this);
        binding.drawerMenuItems.relativechangepassword.setOnClickListener(this);
        binding.drawerMenuItems.relativemyservice.setOnClickListener(this);
        binding.drawerMenuItems.relativeavailibitymanagement.setOnClickListener(this);
        binding.drawerMenuItems.relativecontactus.setOnClickListener(this);
        binding.drawerMenuItems.relativeprivacypolicy.setOnClickListener(this);
        binding.drawerMenuItems.relativeprivacypolicy.setOnClickListener(this);
        binding.drawerMenuItems.relativetermcondition.setOnClickListener(this);
        binding.drawerMenuItems.relativelogout.setOnClickListener(this);
        binding.layoutContent.toolbar.imgBack.setOnClickListener(this);
        binding.drawerMenuItems.relativeDocuments.setOnClickListener(this);
        binding.drawerMenuItems.relativePayment.setOnClickListener(this);
        binding.drawerMenuItems.relativeReview.setOnClickListener(this);
      //  binding.drawerMenuItems.relativeNotification.setOnClickListener(this);
     //   binding.layoutContent.toolbar.imgnotification.setOnClickListener(this);


        binding.layoutContent.bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        binding.layoutContent.bottomNavigation.setItemIconTintList(null);
    }
    public void updateBottomBar(int i)
    {
        binding.layoutContent.bottomNavigation.getMenu().getItem(i).setChecked(true);
    }
    public void toolbarHome()
    {
        binding.layoutContent.toolbar.lnMain.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.logoMain.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.txtTitleother.setVisibility(View.INVISIBLE);
        binding.layoutContent.toolbar.lnOtherTool.setVisibility(GONE);
        binding.layoutContent.bottomNavigation.setVisibility(View.VISIBLE);
        binding.layoutContent.bottomNavigation.getMenu().getItem(0).setChecked(true);
    }


    public void toolbarHomeOther(String my_services)
    {
        binding.layoutContent.toolbar.lnMain.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.logoMain.setVisibility(View.INVISIBLE);
        binding.layoutContent.toolbar.txtTitleother.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.txtTitleother.setText(my_services);

        binding.layoutContent.toolbar.lnOtherTool.setVisibility(GONE);
        binding.layoutContent.bottomNavigation.setVisibility(View.VISIBLE);
        binding.layoutContent.bottomNavigation.getMenu().getItem(0).setChecked(true);
    }


    private void loadHomeFragment(Fragment fragment)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameMain, fragment);
        fragmentTransaction.commit();
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragmentss : fragments) {
                getSupportFragmentManager().beginTransaction().remove(fragmentss).commit();
            }
        }
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }


    private void loadFragmentOther(Fragment fragment)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameMain, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.imgMenu)
        {
            if (!binding.drawerLayout.isDrawerOpen(GravityCompat.START))
                binding.drawerLayout.openDrawer(GravityCompat.START);
            else binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        if(v.getId() == R.id.relative_profile)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,ProfileActivity.class);
            startActivity(i);
        }

        if(v.getId() == R.id.relativechangepassword)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,ChangePasswordActivity.class);
            startActivity(i);
        }

        if(v.getId() == R.id.relativemyservice)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this, AddServicesActivity.class);
            startActivity(i);
        }
        if(v.getId() == R.id.relativeavailibitymanagement)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,AvailabilityManagmentActivity.class);
            startActivity(i);
        }

        if(v.getId() == R.id.relativecontactus)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,ContactUsActivity.class);
            startActivity(i);
        }
        if(v.getId() == R.id.relativeprivacypolicy)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this, PrivacyPolicyActivity.class);
            i.putExtra("page_title", "Privacy Policy");
            startActivity(i);
        }

        if(v.getId() == R.id.relativetermcondition)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this, TermAndConditionActivity.class);
            i.putExtra("page_title", "Terms & Conditions");
            startActivity(i);
        }

        if(v.getId() == R.id.relativelogout)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.really_exit))
                    .setMessage(getString(R.string.are_sure_logout))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) ->
                    {
                        Intent i=new Intent(MainActivity.this,LoginActivity.class);
                        LoginPreferences.deleteAllPreference();
                        startActivity(i);
                    })
                    .setNegativeButton(getString(R.string.no), null).show();
        }

        if(v.getId() == R.id.imgBack)
        {
           onBackPressed();
        }
        if(v.getId() == R.id.relative_documents)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,DocumentsActivity.class);
            startActivity(i);
        }

        /*if(v.getId() == R.id.relative_notification)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this, NotificationFragment.class);
            startActivity(i);
        }*/
      /*  if(v.getId() == R.id.imgnotification)
        {
            Intent i=new Intent(MainActivity.this,NotificationActivity.class);
            startActivity(i);
        }*/
        if(v.getId() == R.id.relative_payment)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,PaymentActivity.class);
            startActivity(i);
        }

        if(v.getId() == R.id.relative_review)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,ReviewActivity.class);
            startActivity(i);
        }
    }

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item ->
    {
        Fragment f=getSupportFragmentManager().findFragmentById(R.id.frameMain);
        switch (item.getItemId())
        {
            case R.id.navHome:
                if(LoginPreferences.getActiveInstance(this).getCategory_id().equals(getString(R.string.categorystatus)))
                {
                    if(!(f instanceof HomeDryCleaningFragment))
                    {
                        Fragment homeFragment = new HomeDryCleaningFragment();
                        loadHomeFragment(homeFragment);
                    }
                }
                else
                {
                    if(!(f instanceof HomeAppoimentFragment))
                    {
                        Fragment homeFragment = new HomeAppoimentFragment();
                        loadHomeFragment(homeFragment);
                    }
                }
                return true;

            case R.id.navBookingManagment:
                if(!(f instanceof BookingManagmentFragment))
                {
                    Fragment  currentFragment = new BookingManagmentFragment();
                    loadFragmentOther(currentFragment);
                }
                return true;



            case R.id.navMessage:
                if(!(f instanceof RecentChatsFragment))
                {
                    Fragment  currentFragment = new RecentChatsFragment();
                    loadFragmentOther(currentFragment);
                }
                return true;

            case R.id.navNotification:
                if(!(f instanceof NotificationFragment))
                {
                    Fragment  currentFragment = new NotificationFragment();
                    loadFragmentOther(currentFragment);
                }
                return true;
        }
        return false;
    };
    @Override
    public void onBackPressed()
    {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        Fragment  currentFragment =getSupportFragmentManager().findFragmentById(R.id.frameMain);

        if(LoginPreferences.getActiveInstance(this).getCategory_id().equals(getString(R.string.categorystatus)))
        {
            if(currentFragment instanceof BookingManagmentFragment  || currentFragment instanceof RecentChatsFragment ||currentFragment instanceof NotificationFragment)
            {
                Fragment homeFragment = new HomeDryCleaningFragment();
                loadHomeFragment(homeFragment);
            }

            else if(getSupportFragmentManager().getBackStackEntryCount() > 0)
            {
                getSupportFragmentManager().popBackStack();
            }
            else if (getSupportFragmentManager().getBackStackEntryCount() == 0)
            {
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.really_exit))
                        .setMessage(getString(R.string.are_sure_exit))
                        .setPositiveButton(getString(R.string.yes), (dialog, which) -> MainActivity.this.finishAffinity())
                        .setNegativeButton(getString(R.string.no), null).show();
            }
        }
        else
        {
            if(currentFragment instanceof BookingManagmentFragment  || currentFragment instanceof RecentChatsFragment ||currentFragment instanceof NotificationFragment)
            {
                Fragment homeFragment = new HomeAppoimentFragment();
                loadHomeFragment(homeFragment);
            }

            else if(getSupportFragmentManager().getBackStackEntryCount() > 0)
            {
                getSupportFragmentManager().popBackStack();
            }
            else if (getSupportFragmentManager().getBackStackEntryCount() == 0)
            {
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.really_exit))
                        .setMessage(getString(R.string.are_sure_exit))
                        .setPositiveButton(getString(R.string.yes), (dialog, which) -> MainActivity.this.finishAffinity())
                        .setNegativeButton(getString(R.string.no), null).show();
            }
        }

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        binding.drawerMenuItems.firstname.setText(company_name);
        binding.drawerMenuItems.gmail.setText(company_mail);
    }
}