package com.wm.muggamuprovider.Activities;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Adapter.MyServiceAdapter;
import com.wm.muggamuprovider.Models.getservicecategorymodel.GetServiceCategoryModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.getservicecategorymodel.GetServiceCategoryResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class MyServiceActivity extends AppCompatActivity
{
    View view;
    RecyclerView recyclerView_my_service;
    MyServiceAdapter myServiceAdapter;
    EditText editsearchname;
    List<GetServiceCategoryModel> getServiceCategoriesModels;
    ProgressBar simpleProgressBar;
    TextView txtnoresult;
    ImageView btnsearch,back;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_service);
        init();
        if(CommonMethod.isOnline(this))
        {
            getserviceCategoryItemApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), this);
        }
    }
    private void init()
    {
      
        recyclerView_my_service=findViewById(R.id.recyclerview_myservice);
        editsearchname=findViewById(R.id.editsearchname);
        simpleProgressBar=findViewById(R.id.simpleProgressBar);
        txtnoresult=findViewById(R.id.txtnoresult);
        btnsearch=findViewById(R.id.btnsearch);
        back=findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });


        btnsearch.setOnClickListener(new View.OnClickListener() 
        {
            @Override
            public void onClick(View view)
            {
                if(editsearchname.getText().toString().trim().equals(""))
                {
                    Toast.makeText(MyServiceActivity.this, "Please enter category to search", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    hitSearchApi(String.valueOf(editsearchname.getText().toString()));
                    hideKeyboard(view);
                }
            }
        });

        editsearchname.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    hitSearchApi(String.valueOf(editsearchname.getText().toString()));
                    hideKeyboard(view);
                    return true;
                }
                return false;
            }
        });

       /* editsearchname.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getServiceCategoriesModels.clear();
                hitSearchApi(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });*/


        editsearchname.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

              Log.d("tag",s.toString());

              if(String.valueOf(s).equals(""))
               {
                   getserviceCategoryItemApi();
               }
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

    }


    private void getserviceCategoryItemApi()
    {
        final ProgressD progressDialog = ProgressD.show(MyServiceActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetServiceCategoryResponse> call = service.getservicecategoryapi();
        call.enqueue(new Callback<GetServiceCategoryResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetServiceCategoryResponse> call, retrofit2.Response<GetServiceCategoryResponse> response)
            {
                progressDialog.dismiss();
                txtnoresult.setVisibility(View.GONE);
                recyclerView_my_service.setVisibility(View.VISIBLE);
                try
                {
                    GetServiceCategoryResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        getServiceCategoriesModels= resultFile.getCategories();
                        recyclerView_my_service.setLayoutManager(new LinearLayoutManager(MyServiceActivity.this, LinearLayoutManager.VERTICAL, false));
                        myServiceAdapter = new MyServiceAdapter(MyServiceActivity.this,getServiceCategoriesModels);
                        recyclerView_my_service.setAdapter(myServiceAdapter);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetServiceCategoryResponse> call, Throwable t)
            {
                Toast.makeText(MyServiceActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void hitSearchApi(String keyword)
    {
       // simpleProgressBar.setVisibility(View.VISIBLE);
        final ProgressD progressDialog = ProgressD.show(MyServiceActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetServiceCategoryResponse> call = service.searchcategory(LoginPreferences.getActiveInstance(MyServiceActivity.this).getToken(),keyword);
        call.enqueue(new Callback<GetServiceCategoryResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetServiceCategoryResponse> call, retrofit2.Response<GetServiceCategoryResponse> response)
            {
               // simpleProgressBar.setVisibility(View.GONE);
                progressDialog.dismiss();
                try
                {
                    GetServiceCategoryResponse resultFile = response.body();
                   // Toast.makeText(getActivity(), resultFile.g, Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        recyclerView_my_service.setVisibility(View.VISIBLE);
                        txtnoresult.setVisibility(View.GONE);
                        getServiceCategoriesModels.clear();

                        getServiceCategoriesModels= resultFile.getCategories();
                        recyclerView_my_service.setLayoutManager(new LinearLayoutManager(MyServiceActivity.this, LinearLayoutManager.VERTICAL, false));
                        myServiceAdapter = new MyServiceAdapter(MyServiceActivity.this,getServiceCategoriesModels);
                        recyclerView_my_service.setAdapter(myServiceAdapter);
                      //  myServiceAdapter.notifyDataSetChanged();
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        getServiceCategoriesModels.clear();
                        txtnoresult.setVisibility(View.VISIBLE);
                        recyclerView_my_service.setVisibility(View.GONE);
                        txtnoresult.setText(resultFile.getMessage());
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetServiceCategoryResponse> call, Throwable t)
            {
                Toast.makeText(MyServiceActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                simpleProgressBar.setVisibility(View.GONE);
            }
        });
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) MyServiceActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }








}