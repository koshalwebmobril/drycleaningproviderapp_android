package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Chat.ChatDetailsActivity;
import com.wm.muggamuprovider.Models.acceptrejectmodel.AcceptRejectResponse;
import com.wm.muggamuprovider.Models.updateappoimentmodel.UpdateAppoimentResponse;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.view.View.GONE;

public class AppoinmentDetailsActivity extends AppCompatActivity
{
    ImageView service_image,back;
    TextView service_name,ordernumber,txt_date_time,txtaddress;
    Button btn_accept,btn_reject,btn_completed;
    LinearLayout linear_btn_layout;
    ImageView chat_icon;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appoinment_details);
        service_image=findViewById(R.id.service_image);
        service_name=findViewById(R.id.service_name);
        ordernumber=findViewById(R.id.ordernumber);
        txt_date_time=findViewById(R.id.txt_date_time);
        back=findViewById(R.id.back);
        btn_accept=findViewById(R.id.btn_accept);
        btn_reject=findViewById(R.id.btn_reject);
        txtaddress=findViewById(R.id.txtaddress);
        linear_btn_layout=findViewById(R.id.linear_btn_layout);
        chat_icon=findViewById(R.id.chat_icon);
        btn_completed=findViewById(R.id.btn_completed);
        user_id=getIntent().getStringExtra("user_id");

        Glide.with(this)
                .load(getIntent().getStringExtra("profile_image")).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile))
                .into(service_image);
        service_name.setText(getIntent().getStringExtra("user_name"));
        ordernumber.setText(getIntent().getStringExtra("order_no"));
        txt_date_time.setText(getIntent().getStringExtra("dateandtime"));
        txtaddress.setText(getIntent().getStringExtra("address"));

        if(getIntent().getStringExtra("appoiment_status").equals("2"))
        {
            btn_completed.setVisibility(View.VISIBLE);
        }

        if(getIntent().getStringExtra("isprovideraccepted").equals("0"))
        {
            chat_icon.setVisibility(GONE);
        }
        else
        {
            chat_icon.setVisibility(View.VISIBLE);
        }




        if(getIntent().getStringExtra("isprovideraccepted").equals("2"))
        {
            if(getIntent().getStringExtra("page_open_status").equals("1"))
            {
                linear_btn_layout.setVisibility(GONE);
            }
            else
            {
                linear_btn_layout.setVisibility(View.VISIBLE);
            }
        }
        else
        {
            linear_btn_layout.setVisibility(View.GONE);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        btn_accept.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hitAcceptRejectApi("1");
            }
        });
        btn_reject.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hitAcceptRejectApi("0");
            }
        });

        chat_icon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), ChatDetailsActivity.class);
                intent.putExtra("name", getIntent().getStringExtra("user_name"));
                intent.putExtra("userid", user_id);
                intent.putExtra("user_token", getIntent().getStringExtra("user_token"));
                intent.putExtra("profileimages", getIntent().getStringExtra("profile_image"));
                startActivity(intent);
            }
        });


        btn_completed.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hitappoimentcomplete();
            }
        });
    }

    private void hitAcceptRejectApi(String action)
    {
        final ProgressD progressDialog = ProgressD.show(AppoinmentDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AcceptRejectResponse> call = service.acceptRejectappointmentApi(LoginPreferences.getActiveInstance(this).getToken(),getIntent().getStringExtra("booking_id"),action);
        call.enqueue(new Callback<AcceptRejectResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AcceptRejectResponse> call, Response<AcceptRejectResponse> response) {
                progressDialog.dismiss();
                try {
                    AcceptRejectResponse resultFile = response.body();
                    Toast.makeText(AppoinmentDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        linear_btn_layout.setVisibility(GONE);
                        Intent i=new Intent(AppoinmentDetailsActivity.this,MainActivity.class);
                        i.putExtra("page_status","2");
                        startActivity(i);
                    }
                    else
                    {

                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AcceptRejectResponse> call, Throwable t) {
                Toast.makeText(AppoinmentDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void hitappoimentcomplete()
    {
        final ProgressD progressDialog = ProgressD.show(AppoinmentDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<UpdateAppoimentResponse> call = service.updateappoimentstatus(LoginPreferences.getActiveInstance(this).getToken(),
                getIntent().getStringExtra("appimentid"));
        call.enqueue(new Callback<UpdateAppoimentResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UpdateAppoimentResponse> call, Response<UpdateAppoimentResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    UpdateAppoimentResponse resultFile = response.body();
                    Toast.makeText(AppoinmentDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<UpdateAppoimentResponse> call, Throwable t) {
                Toast.makeText(AppoinmentDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


}