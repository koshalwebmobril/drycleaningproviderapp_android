package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Adapter.ReviewsAdapter;
import com.wm.muggamuprovider.Models.getreviewmodel.ReviewListItem;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.getreviewmodel.GetReviewResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ReviewActivity extends AppCompatActivity {

    RecyclerView recycler_review;
    ReviewsAdapter reviewsAdapter;
    ImageView back;
    TextView txt_review;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        recycler_review=findViewById(R.id.recycler_review);
        back=findViewById(R.id.back);
        txt_review=findViewById(R.id.txt_review);
        if(CommonMethod.isOnline(ReviewActivity.this))
        {
            getReview();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), ReviewActivity.this);
        }

        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void getReview()
    {
        ProgressD progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetReviewResponse> call = service.getProviderReview(LoginPreferences.getActiveInstance(this).getToken());
        call.enqueue(new Callback<GetReviewResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetReviewResponse> call, retrofit2.Response<GetReviewResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetReviewResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        List<ReviewListItem> reviewlistitem=resultFile.getReviewList();
                        txt_review.setVisibility(View.GONE);
                        recycler_review.setVisibility(View.VISIBLE);

                        recycler_review.setLayoutManager(new LinearLayoutManager(ReviewActivity.this, LinearLayoutManager.VERTICAL, false));
                        reviewsAdapter = new ReviewsAdapter(ReviewActivity.this,reviewlistitem);
                        recycler_review.setAdapter(reviewsAdapter);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        txt_review.setVisibility(View.VISIBLE);
                        recycler_review.setVisibility(View.GONE);
                        txt_review.setText(resultFile.getMessage());
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetReviewResponse> call, Throwable t)
            {
                Toast.makeText(ReviewActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}