package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Adapter.SubItemAdapter;
import com.wm.muggamuprovider.Interface.ChooseClothListener;
import com.wm.muggamuprovider.Models.chooseclothesmodel.ProviderSubitemPriceModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.AddSubItemProviderPriceResponse.AddSubItemProviderPriceResponse;
import com.wm.muggamuprovider.Models.getprovidersubitemsmodel.GetProviderSubItemResponse;
import com.wm.muggamuprovider.Models.getprovidersubitemsmodel.SubitemDetailsModel;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ChooseClothesActivity extends AppCompatActivity
{
    RelativeLayout relative_men_topwear;
    ImageView back;
    public List<SubitemDetailsModel> Subitemdetailslist = new ArrayList<>();
    List<ProviderSubitemPriceModel> pricelist = new ArrayList<>();
    RecyclerView recycle_item_list;
    SubItemAdapter subItemAdapter;
    ChooseClothListener chooseClothListener;
    public static ChooseClothesActivity chooseClothesActivity;
    Button btn_login;
    private String strPrice="";
    private String strId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_clothes);
        chooseClothesActivity=this;

        btn_login=findViewById(R.id.btn_login);
        relative_men_topwear = findViewById(R.id.relative_men_topwear);
        back = findViewById(R.id.back);
        recycle_item_list = findViewById(R.id.recycle_item_list);
        if(CommonMethod.isOnline(ChooseClothesActivity.this))
        {
            getSubItemApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), ChooseClothesActivity.this);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                strPrice="";
                strId="";
                for(int i=0 ; i<Subitemdetailslist.size(); i++)
                 {
                     for(int j=0 ; j<Subitemdetailslist.get(i).getItems().size();j++)
                     {
                         if(Subitemdetailslist.get(i).getItems().get(j).getProviderSubitem()!=null && Subitemdetailslist.get(i).getItems().get(j).getProviderSubitem().size()>0)
                          {
                               if(TextUtils.isEmpty(strPrice))
                              {
                                  strPrice= ""+Subitemdetailslist.get(i).getItems().get(j).getProviderSubitem().get(0).getPrice();
                                  strId = ""+Subitemdetailslist.get(i).getItems().get(j).getId();
                              }
                              else
                              {
                                  strPrice=strPrice+","+Subitemdetailslist.get(i).getItems().get(j).getProviderSubitem().get(0).getPrice();
                                  strId=strId + ","+ Subitemdetailslist.get(i).getItems().get(j).getId();
                              }
                          }
                          else
                          { }
                     }
                 }
                Log.i("TAG", "onClickSize: "+strId);
                if (CommonMethod.isOnline(ChooseClothesActivity.this))
                {
                    hideKeyboard((Button) v);
                    hitSubItemSaveApi(strPrice,strId);
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), ChooseClothesActivity.this);
                }
            }
        });
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    public static ChooseClothesActivity getInstance()
    {
         return chooseClothesActivity;
    }

    private void getSubItemApi()
    {
        final ProgressD progressDialog = ProgressD.show(ChooseClothesActivity.this, getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<GetProviderSubItemResponse> call = service.GetProviderSubitems(LoginPreferences.getActiveInstance(this).getToken(), LoginPreferences.getActiveInstance(this).getServiceId());
        call.enqueue(new Callback<GetProviderSubItemResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetProviderSubItemResponse> call, retrofit2.Response<GetProviderSubItemResponse> response) {
                progressDialog.dismiss();
                try {
                    GetProviderSubItemResponse resultFile = response.body();
                    if (resultFile.getCode() == 200)
                    {
                        Subitemdetailslist = resultFile.getSubitemDetails();
                        recycle_item_list.setLayoutManager(new LinearLayoutManager(ChooseClothesActivity.this, LinearLayoutManager.VERTICAL, false));
                        subItemAdapter = new SubItemAdapter(ChooseClothesActivity.this, Subitemdetailslist);
                        recycle_item_list.setAdapter(subItemAdapter);
                    }
                    /*else if(resultFile.getCode() == 401)
                    {
                        Toast.makeText(ChooseClothesActivity.this, "Unauthenticated", Toast.LENGTH_SHORT).show();
                    }*/

                    else {
                    }
                } catch (Exception e) {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetProviderSubItemResponse> call, Throwable t) {
                Toast.makeText(ChooseClothesActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void hitSubItemSaveApi(String strPrice,String strId)
    {
         ProgressD progressDialog = ProgressD.show(ChooseClothesActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<AddSubItemProviderPriceResponse> call = service.addSubItemProvider(LoginPreferences.getActiveInstance(this).getToken(),LoginPreferences.getActiveInstance(this).getServiceId(),strId,strPrice);
        call.enqueue(new Callback<AddSubItemProviderPriceResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AddSubItemProviderPriceResponse> call, retrofit2.Response<AddSubItemProviderPriceResponse> response) {
               if(progressDialog!=null)
               {
                   progressDialog.dismiss();
               }
               try {
                    AddSubItemProviderPriceResponse resultFile = response.body();
                    Toast.makeText(ChooseClothesActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        finish();
                     //   Intent i=new Intent(ChooseClothesActivity.this,)
                    }
                    else if(resultFile.getCode() == 401)
                    {
                        Toast.makeText(ChooseClothesActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else
                        {

                        }
                } catch (Exception e) {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AddSubItemProviderPriceResponse> call, Throwable t) {
                Toast.makeText(ChooseClothesActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

}