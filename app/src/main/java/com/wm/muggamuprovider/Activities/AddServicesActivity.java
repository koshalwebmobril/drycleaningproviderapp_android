package com.wm.muggamuprovider.Activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.developers.imagezipper.ImageZipper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nikhilpanju.recyclerviewenhanced.OnActivityTouchListener;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;
import com.wm.muggamuprovider.Models.getservicemodel.ProviderServicesItem;
import com.wm.muggamuprovider.Models.getserviceslistmodel.ProviderServicesItemModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.addservicemodel.AddServiceResponse;
import com.wm.muggamuprovider.Models.getserviceslistmodel.GetServiceListResponse;
import com.wm.muggamuprovider.Models.getservicemodel.GetServiceResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.ToastUtil;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
public class AddServicesActivity extends AppCompatActivity implements RecyclerTouchListener.RecyclerTouchListenerHelper{
    RecyclerView recyclerview_dry_lundary;
    Button plus_icon;
    String category_id;
    TextView no_service;
    Dialog dialog_alert;
    RelativeLayout relative_add_image;
    File imageZipperFile;
    private static final int REQUEST_IMAGE =  999;
    ImageView img_service;
    ProgressD progressDialog;
    ImageView  img_when_update,image_drylaundry;
    TextView title_name;
    ImageView back;
    private OnActivityTouchListener touchListener;
    private RecyclerTouchListener onTouchListener;
    MainAdapter mAdapter;
    List<ProviderServicesItemModel> getserviceresponselist;
    String heading_name;
    private ArrayAdapter<String> timeSlotAdapter;
    ArrayList<String> servicesnamelist = new ArrayList<>();
    ArrayList<String> servicesidlist = new ArrayList<>();

    String service_id,service_name;
    private List<ProviderServicesItem> servicelist;
    String service_save_id,provider_service_id;
    AutoCompleteTextView autoCompleteState;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dry_laundary);
        recyclerview_dry_lundary=findViewById(R.id.recyclerview_dry_lundary);
        plus_icon=findViewById(R.id.plus_icon);
        no_service=findViewById(R.id.no_service);
        image_drylaundry=findViewById(R.id.image_drylaundry);
        title_name=findViewById(R.id.title_name);
        back=findViewById(R.id.back);

    //    category_id=getIntent().getStringExtra("category_id");
    //    heading_name=getIntent().getStringExtra("heading_name");
     //   title_name.setText(heading_name);
        Glide.with(AddServicesActivity.this).load(getIntent().getStringExtra("image")).error(R.drawable.drylaundary).placeholder(R.drawable.drylaundary)
                .into(image_drylaundry);
        if(CommonMethod.isOnline(this))
        {
            getserviceApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), this);
        }
        plus_icon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getServiceTitle();
                openAlertDailog();
            }
        });


        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        onTouchListener = new RecyclerTouchListener(AddServicesActivity.this, recyclerview_dry_lundary);
        onTouchListener.setClickable(new RecyclerTouchListener.OnRowClickListener()
        {
            @Override
                    public void onRowClicked(int position)
                    {
                        ProviderServicesItemModel serviceitemmodel = getserviceresponselist.get(position);
                        Intent i=new Intent(AddServicesActivity.this,ChooseClothesActivity.class);
                        startActivity(i);
                        LoginPreferences.getActiveInstance(AddServicesActivity.this).setserviceid(String.valueOf(serviceitemmodel.getServiceId()));
                        Log.d("service_id",String.valueOf(serviceitemmodel.getServiceId()));
                    }
                    @Override
                    public void onIndependentViewClicked(int independentViewID, int position)
                    {
                        ToastUtil.makeToast(getApplicationContext(), "Button in row " + (position + 1) + " clicked!");
                    }
                })
                .setSwipeOptionViews(R.id.edit, R.id.delete)
                .setSwipeable(R.id.rowFG, R.id.rowBG, new RecyclerTouchListener.OnSwipeOptionsClickListener() {
                    @Override
                    public void onSwipeOptionClicked(int viewID, int position)
                    {
                        if (viewID == R.id.edit)
                        {
                             ProviderServicesItemModel serviceitemmodel = getserviceresponselist.get(position);
                             service_save_id=String.valueOf(serviceitemmodel.getId());
                           //  provider_service_id=String.valueOf(serviceitemmodel.getId());
                             String image=serviceitemmodel.getThumbnailPath();
                             String service_name=serviceitemmodel.getName();

                            // getServiceTitle();
                            openAlertDailogEdit(image,service_name);
                        }

                        else if(viewID == R.id.delete)
                        {
                            new android.app.AlertDialog.Builder(AddServicesActivity.this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle(getString(R.string.really_delete))
                                    .setMessage(getString(R.string.are_sure_delete_item))
                                    .setPositiveButton(getString(R.string.yes), (dialog, which) ->
                                    {
                                        ProviderServicesItemModel serviceitemmodel = getserviceresponselist.get(position);
                                        int service_id=serviceitemmodel.getId();
                                        deleteserviceApi(String.valueOf(service_id));
                                    })
                                    .setNegativeButton(getString(R.string.no), null).show();
                        }
                    }
                });
    }
    @Override
    protected void onResume() {
        super.onResume();
        recyclerview_dry_lundary.addOnItemTouchListener(onTouchListener); }

    @Override
    protected void onPause() {
        super.onPause();
        recyclerview_dry_lundary.removeOnItemTouchListener(onTouchListener);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev)
    {
        if(touchListener != null)
            touchListener.getTouchCoordinates(ev);
             return super.dispatchTouchEvent(ev);
    }

    @Override
    public void setOnActivityTouchListener(OnActivityTouchListener listener)
    {
        this.touchListener = listener;
    }

    private void getserviceApi()
    {
        progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetServiceResponse> call = service.getservice(LoginPreferences.getActiveInstance(this).getToken());
        call.enqueue(new Callback<GetServiceResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetServiceResponse> call, retrofit2.Response<GetServiceResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetServiceResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        no_service.setVisibility(View.GONE);
                        recyclerview_dry_lundary.setVisibility(View.VISIBLE);

                        getserviceresponselist= resultFile.getProviderServices();
                        mAdapter = new MainAdapter(this, getserviceresponselist);
                        recyclerview_dry_lundary.setAdapter(mAdapter);
                        recyclerview_dry_lundary.setLayoutManager(new LinearLayoutManager(AddServicesActivity.this));
                    }
                    else if(resultFile.getCode() == 404)
                    {
                            no_service.setVisibility(View.VISIBLE);
                            recyclerview_dry_lundary.setVisibility(View.GONE);
                            no_service.setText("No Service Available");
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetServiceResponse> call, Throwable t)
            {
                Toast.makeText(AddServicesActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void deleteserviceApi(String service_id)
    {
        progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AddServiceResponse> call = service.deleteserviceapi(LoginPreferences.getActiveInstance(this).getToken(),String.valueOf(service_id));
        call.enqueue(new Callback<AddServiceResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AddServiceResponse> call, retrofit2.Response<AddServiceResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    AddServiceResponse resultFile = response.body();
                    Toast.makeText(AddServicesActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        if(CommonMethod.isOnline(AddServicesActivity.this))
                        {
                            getserviceApi();
                        }
                        else
                        {
                            CommonMethod.showAlert(getString(R.string.check_internet), AddServicesActivity.this);
                        }

                    }
                    else
                    {
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AddServiceResponse> call, Throwable t)
            {
                Toast.makeText(AddServicesActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void openAlertDailog()
    {
        dialog_alert = new Dialog(AddServicesActivity.this);
        dialog_alert.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_alert.setContentView(R.layout.item_alert_dailog_add_service);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog_alert.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog_alert.getWindow().setAttributes(lp);

        Button ok = dialog_alert.findViewById(R.id.ok);
        Button cancel = dialog_alert.findViewById(R.id.cancel);
        relative_add_image=dialog_alert.findViewById(R.id. relative_add_image);
        relative_add_image=dialog_alert.findViewById(R.id.relative_add_image);
         autoCompleteState=dialog_alert.findViewById(R.id.add_service_textview);
        img_service=dialog_alert.findViewById(R.id.img_service);
        img_when_update = dialog_alert.findViewById(R.id.img_when_update);


        autoCompleteState.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                 timeSlotAdapter = new ArrayAdapter<String>(AddServicesActivity.this, android.R.layout.simple_spinner_dropdown_item, servicesnamelist);
                autoCompleteState.setAdapter(timeSlotAdapter);
                autoCompleteState.requestFocus();
                autoCompleteState.showDropDown();
                autoCompleteState.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        autoCompleteState.setText(servicesnamelist.get(position));

                        service_name=servicesnamelist.get(position);
                        service_id=servicesidlist.get(position);
                    }
                });
            }
        });

        ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(autoCompleteState.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(AddServicesActivity.this, "Please select service name", Toast.LENGTH_SHORT).show();
                }
                else if(imageZipperFile==null)
                {
                    Toast.makeText(AddServicesActivity.this, "Please Add Service Image", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    hitaddserviceapi(service_id,"",imageZipperFile);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog_alert.dismiss();
            }
        });

        relative_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Dexter.withContext(AddServicesActivity.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted())
                                {
                                    openDialogToUpdateProfilePIC();
                                }
                                if (report.isAnyPermissionPermanentlyDenied())
                                {
                                    showSettingsDialog();
                                }
                            }
                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                                token.continuePermissionRequest();
                            }
                        }).check();
            }
        });
        dialog_alert.show();
        dialog_alert.setCanceledOnTouchOutside(false);
    }

    private void openAlertDailogEdit(String image,String service_name)
    {
        dialog_alert = new Dialog(AddServicesActivity.this);
        dialog_alert.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_alert.setContentView(R.layout.item_alert_dailog_update_service);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog_alert.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog_alert.getWindow().setAttributes(lp);

        Button ok = dialog_alert.findViewById(R.id.ok);
        Button cancel = dialog_alert.findViewById(R.id.cancel);
        relative_add_image=dialog_alert.findViewById(R.id. relative_add_image);
        relative_add_image=dialog_alert.findViewById(R.id.relative_add_image);
        AutoCompleteTextView autoCompleteState=dialog_alert.findViewById(R.id.add_service_textview);
        img_service=dialog_alert.findViewById(R.id.img_service);
        img_when_update = dialog_alert.findViewById(R.id.img_when_update);

        img_service.setVisibility(View.GONE);
        img_when_update.setVisibility(View.VISIBLE);
        autoCompleteState.setText(service_name);
        Glide.with(this).load(image).into(img_when_update);

        ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hitupdateServiceapi(service_save_id,imageZipperFile);
            }
        });



        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog_alert.dismiss();
            }
        });

        relative_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Dexter.withContext(AddServicesActivity.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted())
                                {
                                    openDialogToUpdateProfilePIC();
                                }
                                if (report.isAnyPermissionPermanentlyDenied())
                                {
                                    showSettingsDialog();
                                }
                            }
                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                                token.continuePermissionRequest();
                            }
                        }).check();
            }
        });
        dialog_alert.show();
        dialog_alert.setCanceledOnTouchOutside(false);
    }

    private void hitaddserviceapi(String service_id, String category_id, File file)
    {
        progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        MultipartBody.Part documentBody = null;
        if (file != null)
        {
            RequestBody document1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            documentBody = MultipartBody.Part.createFormData("service_image", file.getName(), document1);
        }
        RequestBody categoryid = RequestBody.create(MediaType.parse("multipart/form-data"), category_id);
        RequestBody serviceid = RequestBody.create(MediaType.parse("multipart/form-data"), service_id);
        Call <AddServiceResponse> call = service.addservice(LoginPreferences.getActiveInstance(this).getToken(),categoryid,serviceid,documentBody);
        call.enqueue(new Callback<AddServiceResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AddServiceResponse> call, retrofit2.Response<AddServiceResponse> response)
            {
                 progressDialog.dismiss();
                try
                {
                     AddServiceResponse resultFile = response.body();
                     Toast.makeText(AddServicesActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        dialog_alert.dismiss();
                        if(CommonMethod.isOnline(AddServicesActivity.this))
                        {
                            getserviceApi();
                        }
                        else
                        {
                            CommonMethod.showAlert(getString(R.string.check_internet),AddServicesActivity.this);
                        }
                    }
                    else if(resultFile.getCode() == 422)
                    {
                        dialog_alert.dismiss();
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<AddServiceResponse> call, Throwable t)
            {
                progressDialog.dismiss();
            }
        });
    }



    private void hitupdateServiceapi(String service_save_id, File file)
    {
        progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        MultipartBody.Part documentBody = null;
        if (file != null)
        {
            RequestBody document1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            documentBody = MultipartBody.Part.createFormData("service_image", file.getName(), document1);
        }
        RequestBody serviceid = RequestBody.create(MediaType.parse("multipart/form-data"), service_save_id);
     //   RequestBody providerserviceid = RequestBody.create(MediaType.parse("multipart/form-data"), provider_service_id);
        Call <AddServiceResponse> call = service.UPDATESERVICE(LoginPreferences.getActiveInstance(this).getToken(),serviceid,documentBody);
        call.enqueue(new Callback<AddServiceResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AddServiceResponse> call, retrofit2.Response<AddServiceResponse> response)
            {
                try
                {
                    AddServiceResponse resultFile = response.body();
                    Toast.makeText(AddServicesActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        progressDialog.dismiss();
                        dialog_alert.dismiss();
                        if(CommonMethod.isOnline(AddServicesActivity.this))
                        {
                            getserviceApi();
                        }
                        else
                        {
                            CommonMethod.showAlert(getString(R.string.check_internet), AddServicesActivity.this);
                        }
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<AddServiceResponse> call, Throwable t)
            {
                progressDialog.dismiss();
            }
        });
    }

    public void openDialogToUpdateProfilePIC()
    {
        Dialog dialog = new Dialog(AddServicesActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_curved_bg_inset);

        dialog.setContentView(R.layout.dialog_select);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        LinearLayout cameraLayout = dialog.findViewById(R.id.cameraLayout);
        LinearLayout galleryLayout = dialog.findViewById(R.id.galleryLayout);
        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCameraIntent();
                dialog.dismiss();
            }
        });
        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchGalleryIntent();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(AddServicesActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        startActivityForResult(intent, REQUEST_IMAGE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                Log.i("TAG", "onActivityResult: " + uri);
                File file = new File(uri.getPath());
                loadProfile(uri.toString());


                try {
                    imageZipperFile = new ImageZipper(AddServicesActivity.this)
                            .setQuality(50)
                            .setMaxWidth(300)
                            .setMaxHeight(300)
                            .compressToFile(file);

                } catch (IOException e) {
                    e.printStackTrace();
                } }
        }
    }

    private void loadProfile(String url)
    {
        img_service.setVisibility(View.GONE);
        img_when_update.setVisibility(View.VISIBLE);

           Glide.with(this).load(url).error(R.drawable.profile).placeholder(R.drawable.profile)
                    .into(img_when_update);
        img_when_update.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
    }

    private void showSettingsDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
       /* builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });*/

        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }



    private class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder>
    {
        LayoutInflater inflater;
        List<ProviderServicesItemModel> modelList;
        public MainAdapter(Callback<GetServiceResponse> context, List<ProviderServicesItemModel> list)
        {
            inflater = LayoutInflater.from(AddServicesActivity.this);
            modelList = new ArrayList<>(list);
        }

        @Override
        public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.drylaundry_item_view, parent, false);
            return new MainViewHolder(view);
        }
        @Override
        public void onBindViewHolder(MainViewHolder holder, int position)
        {
            holder.bindData(modelList.get(position));
        }

        @Override
        public int getItemCount() {
            return modelList.size();
        }

        class MainViewHolder extends RecyclerView.ViewHolder {

            TextView name, subText;
            ImageView service_img;

            public MainViewHolder(View itemView) {
                super(itemView);
                name = itemView.findViewById(R.id.service_name);
                service_img=itemView.findViewById(R.id.service_img);
            }

            public void bindData(ProviderServicesItemModel rowModel)
            {
                String upperString = rowModel.getName().substring(0, 1).toUpperCase() + rowModel.getName().substring(1).toLowerCase();
                name.setText(upperString);
                Glide.with(AddServicesActivity.this).load(rowModel.getThumbnailPath()).into(service_img);
            }
        }
    }

    public void getServiceTitle()
    {
        final ProgressD progressDialog = ProgressD.show(AddServicesActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetServiceListResponse> call = service.GetServiceList(LoginPreferences.getActiveInstance(AddServicesActivity.this).getToken());
        call.enqueue(new Callback<GetServiceListResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetServiceListResponse> call, Response<GetServiceListResponse> response) {
                progressDialog.dismiss();
                try {
                    GetServiceListResponse resultFile = response.body();
                    if (resultFile.getCode() == 200)
                    {
                        servicelist = resultFile.getProviderServices();
                        servicesnamelist = new ArrayList<>();
                        servicesidlist = new ArrayList<>();

                        for(int i = 0; i < servicelist.size(); i++)
                        {
                            servicesnamelist.add(servicelist.get(i).getServiceName());
                            servicesidlist.add(String.valueOf(servicelist.get(i).getId()));
                        }
                    }
                    else if (resultFile.getCode() == 401)
                    {
                        Toast.makeText(AddServicesActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetServiceListResponse> call, Throwable t) {
                Toast.makeText(AddServicesActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

}