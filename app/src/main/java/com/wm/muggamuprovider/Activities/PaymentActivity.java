package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Adapter.PaymentsAdapter;
import com.wm.muggamuprovider.Models.paymentlistmodel.PaymentListItem;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.paymentlistmodel.PaymentListResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class PaymentActivity extends AppCompatActivity {

    ImageView back;
    RecyclerView recycle_payment;
    PaymentsAdapter paymentsAdapter;
    TextView no_payment_found,amount_textview;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        back=findViewById(R.id.back);
        recycle_payment=findViewById(R.id.recycle_payment);
        no_payment_found=findViewById(R.id.no_payment_found);
        amount_textview=findViewById(R.id.amount_textview);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getpaymentlistApi();
    }

    private void  getpaymentlistApi()
    {
        ProgressD progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<PaymentListResponse> call = service.getPaymentList(LoginPreferences.getActiveInstance(this).getToken());
        call.enqueue(new Callback<PaymentListResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<PaymentListResponse> call, retrofit2.Response<PaymentListResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    PaymentListResponse resultFile = response.body();
                  //  PaymentListResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        no_payment_found.setVisibility(View.GONE);
                        recycle_payment.setVisibility(View.VISIBLE);
                        amount_textview.setText(String.valueOf(resultFile.getTotalAmount()));
                        List<PaymentListItem> paymentlist=resultFile.getPaymentList();
                        recycle_payment.setLayoutManager(new LinearLayoutManager(PaymentActivity.this, LinearLayoutManager.VERTICAL, false));
                        paymentsAdapter = new PaymentsAdapter(PaymentActivity.this,paymentlist);
                        recycle_payment.setAdapter(paymentsAdapter);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        no_payment_found.setVisibility(View.VISIBLE);
                        recycle_payment.setVisibility(View.GONE);
                        no_payment_found.setText(resultFile.getMessage());
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<PaymentListResponse> call, Throwable t)
            {
                Toast.makeText(PaymentActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}