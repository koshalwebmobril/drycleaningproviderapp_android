package com.wm.muggamuprovider.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.wm.muggamuprovider.Adapter.AddressAdapter;
import com.wm.muggamuprovider.R;

public class AddressActivity extends AppCompatActivity
{
    AddressAdapter AddressAdapter;
    RecyclerView recycler_address;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        recycler_address=findViewById(R.id.recycler_address);
        back=findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recycler_address.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        AddressAdapter = new AddressAdapter(this);
        recycler_address.setAdapter(AddressAdapter);



    }
}