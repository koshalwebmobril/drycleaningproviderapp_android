package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.muggamuprovider.Adapter.DocumentsAdapter;
import com.wm.muggamuprovider.Interface.OnClickDelete;
import com.wm.muggamuprovider.Models.getdocumentmodel.DocumentsItemModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.documentdeletemodel.DocumentDeleteResponse;
import com.wm.muggamuprovider.Models.getdocumentmodel.GetDocumentResponse;
import com.wm.muggamuprovider.Models.uploaddocumentmodel.UploadDocumentResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DocumentsActivity extends AppCompatActivity implements OnClickDelete {

    ImageView back;
    RecyclerView recyclerview_doc;
    DocumentsAdapter documentsadapter;
    TextView no_documents;
    OnClickDelete onClickDelete;
    Button plus_icon;
    List<DocumentsItemModel> documentlist;
    private int REQUEST_CODE_DOC = 2;
    private  int  clickImage=1;
    private int GALLERY = 1;
    byte[] mData1;
    String extention;
    File file;
    private String selectedFilePath = "";
    private MultipartBody.Part attachementFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documents);
        recyclerview_doc=findViewById(R.id.recyclerview_doc);
        back=findViewById(R.id.back);
        no_documents=findViewById(R.id.no_documents);
        plus_icon= findViewById(R.id.plus_icon);
        onClickDelete=this;
        if(CommonMethod.isOnline(DocumentsActivity.this))
        {
            GetAllDocuments();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), DocumentsActivity.this);
        }
        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        plus_icon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Dexter.withContext(DocumentsActivity.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()
                                )
                                {
                                    openPdfFilesFormDevice();
                                }

                                if (report.isAnyPermissionPermanentlyDenied())
                                {
                                    showSettingsDialog();
                                }
                            }
                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                                token.continuePermissionRequest();
                            }
                        }).check();

              //  openPdfFilesFormDevice();
                //  filename.setText("");
            }
        });
    }


    private void showSettingsDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
       /* builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });*/

        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void GetAllDocuments()
    {
        final ProgressD progressDialog = ProgressD.show(DocumentsActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetDocumentResponse> call = service.GetDocument(LoginPreferences.getActiveInstance(this).getToken());
        call.enqueue(new Callback<GetDocumentResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetDocumentResponse> call, retrofit2.Response<GetDocumentResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetDocumentResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        recyclerview_doc.setVisibility(View.VISIBLE);
                        no_documents.setVisibility(View.GONE);
                         documentlist=resultFile.getProviderDocuments();

                        recyclerview_doc.setLayoutManager(new LinearLayoutManager(DocumentsActivity.this, LinearLayoutManager.VERTICAL, false));
                        documentsadapter = new DocumentsAdapter(DocumentsActivity.this, (ArrayList) documentlist,onClickDelete);
                        recyclerview_doc.setAdapter(documentsadapter);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        recyclerview_doc.setVisibility(View.GONE);
                        no_documents.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetDocumentResponse> call, Throwable t)
            {
                Toast.makeText(DocumentsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public void delete(int documentid,int position)
    {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.really_delete))
                .setMessage(getString(R.string.are_sure_delete))
                .setPositiveButton(getString(R.string.yes), (dialog, which) ->
                {
                   hitDeleteDocumentApi(documentid,position);
                })
                .setNegativeButton(getString(R.string.no), null).show();
    }


    public void hitDeleteDocumentApi(int documentid,int position)
    {
           final ProgressD progressDialog = ProgressD.show(DocumentsActivity.this,getResources().getString(R.string.logging_in), true, false, null);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(UrlApi.BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
            ApiInterface service = retrofit.create(ApiInterface.class);
            Call<DocumentDeleteResponse> call = service.documentdelete(LoginPreferences.getActiveInstance(this).getToken(),documentid);
            call.enqueue(new Callback<DocumentDeleteResponse>()
            {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<DocumentDeleteResponse> call, retrofit2.Response<DocumentDeleteResponse> response)
                {
                    progressDialog.dismiss();
                    try
                    {
                        DocumentDeleteResponse resultFile = response.body();
                        Toast.makeText(DocumentsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                        if(resultFile.getCode() == 200)
                        {
                           // GetAllDocuments();
                            documentlist.remove(position);
                            documentsadapter.notifyDataSetChanged();
                            documentsadapter.notify();
                            if(documentlist.size()==0)
                            {
                                recyclerview_doc.setVisibility(View.GONE);
                                no_documents.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                recyclerview_doc.setVisibility(View.VISIBLE);
                                no_documents.setVisibility(View.GONE);
                            }
                        }
                        else if(resultFile.getCode() == 404)
                        {

                        }
                        else
                        {
                            // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("Login Faild", e.toString());
                    }
                }

                @Override
                public void onFailure(Call<DocumentDeleteResponse> call, Throwable t)
                {
                    Toast.makeText(DocumentsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        }

    private void showFileChooser()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        //   String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword","application/pdf","image/*"};
        String[] mimetypes = {"application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(intent, REQUEST_CODE_DOC);
    }

  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == DocumentsActivity.RESULT_CANCELED)
        {
            return;
        }
        switch (clickImage)
        {
            case 1:
                if (requestCode == GALLERY)
                {
                    if (data != null)
                    {
                        Uri selectedImageUri = data.getData( );
                        Bitmap bitmap = null;
                        ByteArrayOutputStream bytes = null;
                        try
                        {
                            bitmap = MediaStore.Images.Media.getBitmap(DocumentsActivity.this.getContentResolver(), selectedImageUri);
                            bytes = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                            mData1 = bytes.toByteArray();

                            String picturePath = getPath(DocumentsActivity.this, selectedImageUri);
                            String[] bits = picturePath.split("/");
                            String lastOne = bits[bits.length-1];
                           // filename.setText(lastOne);
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
                else if(requestCode==REQUEST_CODE_DOC && data!=null)
                {
                    Uri uri=data.getData();
                    Uri uri1 = data.getParcelableExtra("path");
                    String path=  FileUtils.getPath(this,uri);
                    file = new File(path);
                    hitUploadDocumentFile(file);
                    InputStream iStream = null;
                    try
                    {
                        Cursor mCursor = DocumentsActivity.this.getApplicationContext().getContentResolver().query(uri, null, null, null, null);
                        int indexedname = mCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        mCursor.moveToFirst();
                        String filenamestring = mCursor.getString(indexedname);
                        mCursor.close();
                        iStream = DocumentsActivity.this.getContentResolver().openInputStream(uri);


                        extention=filenamestring.substring(filenamestring.lastIndexOf("."));
                        if(extention.equals(".png") || extention.equals(".jpg")  || extention.equals(".jpeg")
                                || extention.equals(".doc")|| extention.equals(".PDF") || extention.equals(".pdf") || extention.equals(".docx"))
                        {
                            mData1 = getBytes(iStream);
                        }
                        else
                        {
                            Toast.makeText(DocumentsActivity.this, "Invalid file format", Toast.LENGTH_SHORT).show();
                        }
                        Log.i(TAG, "onActivityResult11: "+extention);
                    }
                    catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
    }*/

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String getPath(Context context, Uri uri )
    {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }


    public void hitUploadDocumentFile()
    {
        final ProgressD progressDialog = ProgressD.show(DocumentsActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        MultipartBody.Part documentBody = null;
        if (file != null)
        {
           /* RequestBody document1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            documentBody = MultipartBody.Part.createFormData("document", file.getName(), document1);*/
        }
        Call<UploadDocumentResponse> call = service.uploaddocment(LoginPreferences.getActiveInstance(this).getToken(),attachementFile);
        call.enqueue(new Callback<UploadDocumentResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UploadDocumentResponse> call, retrofit2.Response<UploadDocumentResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    UploadDocumentResponse resultFile = response.body();
                    Toast.makeText(DocumentsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                         GetAllDocuments();
                    }
                    else if(resultFile.getCode() == 404)
                    {

                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<UploadDocumentResponse> call, Throwable t)
            {
                Toast.makeText(DocumentsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }



    public void openPdfFilesFormDevice()
    {
        //Currently showing all document types options
        try {
            String[] fileSupport = {"pdf"};
            FilePickerBuilder.getInstance()
                    .setMaxCount(1) //for 1 pdf doc at one time
                    .setActivityTheme(R.style.LibAppTheme) //optional
                    .showFolderView(true)
                    .enableDocSupport(true)
                    //.addFileSupport("PDF FILES", fileSupport, R.drawable.ic_pdf)
                    .pickFile(this);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "\"Error occurred\\ntry again later\"", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    try {

                        ArrayList<String> list = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);
                        File myFile = null;
                        if (list != null && list.size() > 0) {
                            selectedFilePath = list.get(0);
                            myFile = new File(selectedFilePath);
                            Log.e("gopal", "file path " + selectedFilePath);


                            if (myFile != null)
                            {
                              //  tvAttachements.setText("Selected successfully");
                                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), myFile);
                                attachementFile = MultipartBody.Part.createFormData("document", myFile.getName(), requestFile);
                                hitUploadDocumentFile();
                            }

                        } else
                            {
                            selectedFilePath = "";
                          //  tvAttachements.setText("Error");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                     //   toastShort("Error occured");
                    }
                }
                break;
        }
    }
}