package com.wm.muggamuprovider.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Utils.ProgressD;

public class ViewDocumentsActivity extends AppCompatActivity {

    TextView txtdocumentname;
    String strdocument_name;
    ImageView back;
    WebView webview1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_documents);
        txtdocumentname=findViewById(R.id.txtdocumentname);
        back=findViewById(R.id.back);

        webview1=findViewById(R.id.webview);
        final ProgressD progressDialog = ProgressD.show(ViewDocumentsActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        txtdocumentname.setText(getIntent().getStringExtra("document_name"));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        webview1.getSettings().setJavaScriptEnabled(true);
        webview1.loadUrl("http://docs.google.com/gview?embedded=true&url=" + getIntent().getStringExtra("document_path"));
        webview1.setWebViewClient(new WebViewClient()
        {
            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                progressDialog.dismiss();
            }
        });

    }
}