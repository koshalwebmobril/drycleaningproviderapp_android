package com.wm.muggamuprovider.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.wm.muggamuprovider.Adapter.AddServicesAdapter;
import com.wm.muggamuprovider.R;

public class AddServiceActivity extends AppCompatActivity
{
    RecyclerView recyclerview_services;
    AddServicesAdapter addServicesAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        recyclerview_services=findViewById(R.id.recyclerview_services);
        recyclerview_services.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        addServicesAdapter = new AddServicesAdapter(this);
        recyclerview_services.setAdapter(addServicesAdapter);
    }
}