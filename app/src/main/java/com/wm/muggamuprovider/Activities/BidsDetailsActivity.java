package com.wm.muggamuprovider.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.wm.muggamuprovider.Adapter.AddPostDetailsAdapter;
import com.wm.muggamuprovider.Adapter.PostwithbidinfoAdapter;
import com.wm.muggamuprovider.Models.bidacceptmodel.PostDetailsItem;
import com.wm.muggamuprovider.Models.postwithbidinfomodel.PostWithBidInfoModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.postonbidmodel.PostOnBidResponse;
import com.wm.muggamuprovider.Models.postwithbidinfomodel.PostWithBidInfoResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class BidsDetailsActivity extends AppCompatActivity
{
    ImageView back,service_img;
    TextView service_name,service_day_time,service_address,service_remarks,bid_price;
    RecyclerView recyclerview_bids_info;
    String post_id,bid_status;
    TextView post_button,username;
    PostwithbidinfoAdapter postwithbidinfoAdapter;
    RelativeLayout relative_bid;
    EditText bid_amount;
    ImageView profile_image1,bid_profile,profile_image;
    TextView username1,bid_price_info,bid_time;
    LinearLayout linear_bid_details;
    LinearLayout linear_not_bid_yet;
    String page_status;
    TextView created_at,service_time;
    List<PostDetailsItem> postdetailslist;

    RecyclerView recyclerview_postdetails;
    AddPostDetailsAdapter addPostDetailsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bids_details);
        created_at=findViewById(R.id.created_at);
        service_time=findViewById(R.id.service_time);

        relative_bid=findViewById(R.id.relative_bid);
        back=findViewById(R.id.back);
        service_img=findViewById(R.id.service_img);
        service_name=findViewById(R.id.service_name);
        service_day_time=findViewById(R.id.service_day_time);
        service_address=findViewById(R.id.service_address);
        service_remarks=findViewById(R.id.service_remarks);
        recyclerview_postdetails=findViewById(R.id.recyclerview_postdetails);

        post_button =findViewById(R.id.post);
        username=findViewById(R.id.username);
        bid_amount=findViewById(R.id.bid_amount);
        profile_image1=findViewById(R.id.profile_image1);
        username1=findViewById(R.id.username1);
        bid_price_info=findViewById(R.id.bid_price_info);
        bid_time=findViewById(R.id.bid_time);
        linear_bid_details=findViewById(R.id.linear_bid_details);
         linear_not_bid_yet=findViewById(R.id.linear_not_bid_yet);
        bid_profile=findViewById(R.id.bid_profile);
        profile_image=findViewById(R.id.profile_image);


        service_name.setText(getIntent().getStringExtra("service_name"));
        service_day_time.setText(getIntent().getStringExtra("service_day_time"));
        service_address.setText(getIntent().getStringExtra("service_address"));
        service_remarks.setText(getIntent().getStringExtra("service_remarks"));
      //  created_at.setText(getIntent().getStringExtra("created_at"));
        service_time.setText("at"+" " + getIntent().getStringExtra("service_time"));

        postdetailslist= (List<PostDetailsItem>) getIntent().getSerializableExtra("PostDetailsItem");
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(BidsDetailsActivity.this, RecyclerView.VERTICAL, false);
        recyclerview_postdetails.setLayoutManager(mLayoutManager);
        addPostDetailsAdapter = new AddPostDetailsAdapter(BidsDetailsActivity.this, postdetailslist);
        recyclerview_postdetails.setAdapter(addPostDetailsAdapter);


        Picasso.get().load(getIntent().getStringExtra("service_image")).into(service_img);
        if(getIntent().getStringExtra("profile_image") !=null)
        {
            Picasso.get().load(getIntent().getStringExtra("profile_image")).into(profile_image);
        }
        else
        { }
        post_id=getIntent().getStringExtra("post_id");
        username.setText(getIntent().getStringExtra("username"));
        bid_status=getIntent().getStringExtra("bid_status");
        page_status=getIntent().getStringExtra("page_status");

        if(page_status.equals("AllBids"))
        {
            if(bid_status.equals("true"))
            {
                hitbiddetailsApi(post_id);
            }
            else
            {
                relative_bid.setVisibility(View.VISIBLE);
                linear_not_bid_yet.setVisibility(View.VISIBLE);
                linear_bid_details.setVisibility(View.VISIBLE);
                if (LoginPreferences.getActiveInstance(BidsDetailsActivity.this).getUserProfile().isEmpty())
                {
                    bid_profile.setImageResource(R.drawable.profile);
                }
                else
                    {
                         Picasso.get().load(LoginPreferences.getActiveInstance(this).getUserProfile()).into(bid_profile);
                    }
            }
        }
        else
            {
                relative_bid.setVisibility(View.GONE);
                linear_not_bid_yet.setVisibility(View.GONE);
                linear_bid_details.setVisibility(View.VISIBLE);

                if (LoginPreferences.getActiveInstance(BidsDetailsActivity.this).getUserProfile().isEmpty())
                {
                    profile_image1.setImageResource(R.drawable.profile);
                } else
                {
                    Picasso.get().load(LoginPreferences.getActiveInstance(this).getUserProfile()).into(profile_image1);
                }
                //Picasso.get().load(LoginPreferences.getActiveInstance(BidsDetailsActivity.this).getUserProfile()).into(profile_image1);
                username1.setText(LoginPreferences.getActiveInstance(BidsDetailsActivity.this).getUserName());
                bid_price_info.setText(getString(R.string.currency_icon) +" "+ getIntent().getStringExtra("bid_amount"));
                bid_time.setText("Time : "+ getIntent().getStringExtra("bid_time"));

            }

        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        post_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (CommonMethod.isOnline(BidsDetailsActivity.this))
                {
                    hideKeyboard((TextView) v);
                    if(validation())
                    {
                        hitbidApi(bid_amount.getText().toString().trim());
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), BidsDetailsActivity.this);
                }
            }
        });
    }
    private void hitbiddetailsApi(String post_id)
    {
        ProgressD progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<PostWithBidInfoResponse> call = service.postwithbidinfo(LoginPreferences.getActiveInstance(this).getToken(),post_id);
        call.enqueue(new Callback<PostWithBidInfoResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<PostWithBidInfoResponse> call, retrofit2.Response<PostWithBidInfoResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    PostWithBidInfoResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        relative_bid.setVisibility(View.GONE);
                        linear_not_bid_yet.setVisibility(View.GONE);
                        linear_bid_details.setVisibility(View.VISIBLE);

                        PostWithBidInfoModel model=resultFile.getPostWithBidInfoModel();
                        String bid_amount_response= String.valueOf(model.getBidAmount());
                        if (LoginPreferences.getActiveInstance(BidsDetailsActivity.this).getUserProfile().isEmpty())
                        {
                            profile_image1.setImageResource(R.drawable.profile);
                        }
                        else
                        {
                            Picasso.get().load(LoginPreferences.getActiveInstance(BidsDetailsActivity.this).getUserProfile()).into(profile_image1);
                        }

                       // Picasso.get().load(LoginPreferences.getActiveInstance(BidsDetailsActivity.this).getUserProfile()).into(profile_image1);
                        username1.setText(LoginPreferences.getActiveInstance(BidsDetailsActivity.this).getUserName());
                        bid_price_info.setText(getString(R.string.currency_icon) +" "+ bid_amount_response);
                        bid_time.setText("Time : "+ model.getCreatedAt());
                    }
                  /*  else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recylerbidsitems.setVisibility(View.GONE);
                    }*/
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<PostWithBidInfoResponse> call, Throwable t)
            {
                Toast.makeText(BidsDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }



    private void hitbidApi(String bid_amount)
    {
        ProgressD progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<PostOnBidResponse> call = service.postonbid(LoginPreferences.getActiveInstance(this).getToken(),post_id,bid_amount);
        call.enqueue(new Callback<PostOnBidResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<PostOnBidResponse> call, retrofit2.Response<PostOnBidResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    PostOnBidResponse resultFile = response.body();
                    Toast.makeText(BidsDetailsActivity.this,resultFile.getMessage() , Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        hitbiddetailsApi(post_id);
                    }
                    /*else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recylerbidsitems.setVisibility(View.GONE);
                    }*/
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<PostOnBidResponse> call, Throwable t)
            {
                Toast.makeText(BidsDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private boolean validation()
    {
        if (TextUtils.isEmpty(bid_amount.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("0"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("00"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("000"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("0000"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("00000"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("000000"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("0000000"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("00000000"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("000000000"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (bid_amount.getText().toString().trim().equals("0000000000"))
        {
            Toast.makeText(this, "Please enter valid bid amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}