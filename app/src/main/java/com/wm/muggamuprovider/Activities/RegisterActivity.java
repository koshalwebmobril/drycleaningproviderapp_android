package com.wm.muggamuprovider.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.ApiClient.RetrofitConnection;
import com.wm.muggamuprovider.Models.getCategorymodel.CategoriesItem;
import com.wm.muggamuprovider.Models.getCategorymodel.GetCategoryResponse;
import com.wm.muggamuprovider.Models.registermodel.ResponseAuthentication;
import com.wm.muggamuprovider.Models.registermodel.RegisterResponse;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.GPSTracker;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.content.ContentValues.TAG;
import static com.wm.muggamuprovider.Utils.CommonMethod.isValidEmaillId;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView back_arrow;
    Button btn_signup;
    EditText company_name,email_address,mobile_number,password,company_address,service_details;
    TextView sign_in;
    String user_lat,user_lng;
    String notification_token;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    private int requestCode;
    private int resultCode;
    private Intent data;

    private final static int PLACE_PICKER_REQUEST1 = 999;
    private String address_lat;
    String latitude,longitude;
    String city;
    TextInputLayout textinputaddress;
    private String txtaddress;
    AutoCompleteTextView autoCompleteState;
    private ArrayAdapter<String> timeSlotAdapter;
    List<CategoriesItem> categoryitemlist;

    ArrayList<String> servicesnamelist = new ArrayList<>();
    ArrayList<String> servicesidlist = new ArrayList<>();

    String service_name,service_id;
    RelativeLayout relative_select_service;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        if(CommonMethod.isOnline(RegisterActivity.this))
        {
            getCategoryApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), RegisterActivity.this);
        }
        relative_select_service.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                initilizeStateSpinner();
            }
        });


        back_arrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }
    public void init()
    {
        autoCompleteState=findViewById(R.id.autoCompleteState);
        relative_select_service=findViewById(R.id.relative_select_service);

        back_arrow=findViewById(R.id.back_arrow);
        btn_signup=findViewById(R.id.btn_signup);
        sign_in=findViewById(R.id.sign_in);
        company_name=findViewById(R.id.company_name);
        email_address=findViewById(R.id.email_address);
        mobile_number=findViewById(R.id.mobile_number);
        password=findViewById(R.id.password);
        company_address=findViewById(R.id.company_address);
        textinputaddress=findViewById(R.id.textinputaddress);

        btn_signup.setOnClickListener(this);
        sign_in.setOnClickListener(this);
        company_address.setOnClickListener(this);
        autoCompleteState.setOnClickListener(this);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_signup:
                if (CommonMethod.isOnline(this))
                {
                    if(validation())
                    {
                        notification_token = FirebaseInstanceId.getInstance().getToken();
                        hideKeyboard((Button) v);
                        RegisterUserApi();
                    }

                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;

            case R.id.sign_in:
                Intent i=new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(i);
                break;


            case R.id.company_address:
                checkPermission();
                break;

            case R.id.autoCompleteState:
                initilizeStateSpinner();
                break;
        }
    }
    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(company_name.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Company Name", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (TextUtils.isEmpty(email_address.getText().toString().trim())) {
            Toast.makeText(this, "Please Enter Email Id", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (!isValidEmaillId(email_address.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Valid Email id", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(mobile_number.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (mobile_number.getText().toString().trim().length() < 7)
        {
            Toast.makeText(this, "Mobile Number should be min 7 digit", Toast.LENGTH_LONG).show();
            return false;
        }

        else if (mobile_number.getText().toString().trim().length() > 15)
        {
            Toast.makeText(this, "Mobile number should be max 15 digit", Toast.LENGTH_LONG).show();
            return false;
        }

        else if (TextUtils.isEmpty(password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Password", Toast.LENGTH_SHORT).show();
            return false;
        }
       /* else if (password.getText().toString().trim().length() < 8 || password.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "Password must be 8 to 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }*/

        else if (password.getText().toString().trim().length() < 6){
            Toast.makeText(this, "Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (password.getText().toString().trim().length() > 16)
        {
            Toast.makeText(this, "Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(TextUtils.isEmpty(company_address.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Company Address", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(TextUtils.isEmpty(company_address.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Company Address", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
      /*  company_name.setText("");
        email_address.setText("");
        mobile_number.setText("");
        password.setText("");*/


        //  company_address.setText("");

    }

    private void RegisterUserApi()
    {
        final ProgressD progressDialog = ProgressD.show(RegisterActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<ResponseAuthentication> call = service.RegisterUser(company_name.getText().toString().trim(),email_address.getText().toString().trim(),
                mobile_number.getText().toString().trim(),"3",password.getText().toString().trim(),
                company_address.getText().toString().trim(),"2",notification_token,user_lat,user_lng,
                service_id
        );

        call.enqueue(new Callback<ResponseAuthentication>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResponseAuthentication> call, retrofit2.Response<ResponseAuthentication> response)
            {
                progressDialog.dismiss();
                try
                {
                    ResponseAuthentication resultFile = response.body();
                    Toast.makeText(RegisterActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                    if (resultFile.getCode() == 200)
                    {
                        RegisterResponse registerResponse =resultFile.getRegisterResponse();
                        LoginPreferences.getActiveInstance(RegisterActivity.this).setUserName(registerResponse.getName());
                        LoginPreferences.getActiveInstance(RegisterActivity.this).setUserEmail(registerResponse.getEmail());
                       // LoginPreferences.getActiveInstance(RegisterActivity.this).setCategory_id(registerResponse.getEmail());

                        company_name.setText("");
                        email_address.setText("");
                        mobile_number.setText("");
                        password.setText("");

                        Intent i=new Intent(RegisterActivity.this, RegisterOtpVerifyActivity.class);
                        i.putExtra("email",registerResponse.getEmail());
                        startActivity(i);
                    }
                    else if(resultFile.getCode() == 401)
                    {

                    }
                    else
                    {
                      //  Toast.makeText(RegisterActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Register Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseAuthentication> call, Throwable t)
            {
                Toast.makeText(RegisterActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }



    private void getCategoryApi()
    {
        final ProgressD progressDialog = ProgressD.show(RegisterActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<GetCategoryResponse> call = service.GetCategory();

        call.enqueue(new Callback<GetCategoryResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetCategoryResponse> call, retrofit2.Response<GetCategoryResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetCategoryResponse resultFile = response.body();
                   // Toast.makeText(RegisterActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        categoryitemlist=resultFile.getCategories();
                        for(int i = 0; i < categoryitemlist.size() ; i++)
                        {
                            servicesnamelist.add(categoryitemlist.get(i).getTitle());
                            servicesidlist.add(String.valueOf(categoryitemlist.get(i).getId()));
                        }
                    }
                    else if(resultFile.getCode() == 401)
                    {

                    }
                    else
                    {
                        //  Toast.makeText(RegisterActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Register Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetCategoryResponse> call, Throwable t)
            {
                Toast.makeText(RegisterActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS)
        {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(RegisterActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            openPlacePicker1();
        }
    }

    private final static int PLACE_PICKER_REQUEST = 999;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
        checkPermissionOnActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case PLACE_PICKER_REQUEST:
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    Geocoder geocoder = new Geocoder(this);
                    try
                    {
                        List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude,place.getLatLng().longitude, 1);
                        txtaddress = addresses.get(0).getAddressLine(0);

                        final LatLng latlng = place.getLatLng();
                        String s1= String.valueOf(latlng);
                        String[] s21 = s1.split(":");
                        String s23 = String.valueOf(s21[1]);
                        String s24 = s23.replaceAll("\\(", "");
                        address_lat = s24.replaceAll("\\)", "");
                        String[] latlong =  address_lat.split(",");
                        user_lat = latlong[0];
                        user_lng = latlong[1];
                        company_address.setText(txtaddress);
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
            }
        }
        else if (resultCode == AutocompleteActivity.RESULT_ERROR)
        {
            Status status = Autocomplete.getStatusFromIntent(data);
            Log.i(TAG, status.getStatusMessage());

        } else if (resultCode == RESULT_CANCELED)
        {

        }
    }

    private void checkPermissionOnActivityResult(int requestCode, int resultCode, Intent data)
    {
    }

    public void openPlacePicker1()
    {
        if (!Places.isInitialized())
        {
            Places.initialize(getApplicationContext(),getResources().getString(R.string.googlekey));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
        startActivityForResult(intent, PLACE_PICKER_REQUEST1);
    }

    private void initilizeStateSpinner()
    {
        timeSlotAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,servicesnamelist );
        autoCompleteState.setAdapter(timeSlotAdapter);
        autoCompleteState.requestFocus();
        autoCompleteState.showDropDown();
        autoCompleteState.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                 autoCompleteState.setText(servicesnamelist.get(position));

                 service_name=servicesnamelist.get(position);
                 service_id=servicesidlist.get(position);
            }
        });
    }

}