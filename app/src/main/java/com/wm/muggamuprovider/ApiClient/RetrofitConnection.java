package com.wm.muggamuprovider.ApiClient;






import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConnection {

    private static com.wm.muggamuprovider.ApiClient.RetrofitConnection connect;
   // private ApiInterface clientService;
    //private static final String BASE_URL = "";


    public static synchronized com.wm.muggamuprovider.ApiClient.RetrofitConnection getInstance() {
        if (connect == null) {
            connect = new com.wm.muggamuprovider.ApiClient.RetrofitConnection();
        }
        return connect;
    }

    public ApiInterface createService() {
        Retrofit retrofit;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();

        retrofit = new Retrofit
                .Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit.create(ApiInterface.class);
    }
}
