package com.wm.muggamuprovider.Utils;

import android.annotation.SuppressLint;
import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@SuppressLint("Registered")
public class UserLastSeenTime {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
  //  private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
   // private SimpleDateFormat dataFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());


    public String getTimeAgo(long time,Context context) {
        long tim = time*1000;
        Date date = new Date(tim);
        String currentTime=sdf.format(date);

        //String dateWise=dataFormat.format(date);

      //  String current=sdf.format(date);
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }


        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return currentTime;
          //  return context.getResources().getString(R.string.active_just_now);
            //return null;
        }
        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
           // return this.getResources().getString(R.string.active_few_second);
            return currentTime;
        } else if (diff < 2 * MINUTE_MILLIS) {
           // return this.getResources().getString(R.string.active_minute_ago);
            return currentTime;
        } else if (diff < 50 * MINUTE_MILLIS) {
            //return "Active " + diff / MINUTE_MILLIS + " minutes ago";
            return currentTime;
        } else if (diff < 90 * MINUTE_MILLIS) {
            return currentTime;
           // return "Active an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
           // return "Active " + diff / HOUR_MILLIS + " hours ago";
            return currentTime;
        } else if (diff < 48 * HOUR_MILLIS) {
            return currentTime;
           // return context.getResources().getString(R.string.active_yesterday);
        } else {
            return currentTime;
          //  return context.getResources().getString(R.string.active_on)+" "+dateWise;
          //  return "Active " + diff / DAY_MILLIS + " days ago";
        }
    }


}
