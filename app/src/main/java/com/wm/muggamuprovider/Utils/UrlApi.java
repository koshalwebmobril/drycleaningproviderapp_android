package com.wm.muggamuprovider.Utils;

public class UrlApi
{
   // public static String BASE_URL = "https://webmobril.org/dev/dryCleaning/api/provider/";    //webmobril server
   
    public static String BASE_URL = "http://52.54.1.108/drycleaning/api/provider/";       // Aws Server
    public static String BASE_URL1 = "http://52.54.1.108/drycleaning/api/v1/";           //  Aws contact us
    public static String BASE_URL2 = "http://52.54.1.108/drycleaning/provider/";       //   Aws privacy policy

    public static final String REGISTER="register";
    public static final String LOGIN="login";
    public static final String FORGOTPASSWORD="forgot-password";
    public static final String VERIFYOTP="verify-otp";
    public static final String RESETPASSWORD="reset-password";
    public static final String GETPROFILE="get-profile";
    public static final String CHANGEPASSWORDAPI="change-password";
    public static final String GETSERVICECATEGORYAPI="get-categories";
    public static final String GETSERVICE="get-service";
    public static final String ADDSERVICE="add-service";
    public static final String UPDATEPROFILE="update-profile";
    public static final String CONTACTUS="contact-us";
    public static final String UPDATESERVICEAPI="update-service";
    public static final String DeleteServiceApi="remove-service";
    public static final String ALLPOSTAPI="all-post";
    public static final String PRIVACY ="privacy";
    public static final String TERMS="terms";

    public static final String BIDACCEPTEDAPI="accepted-bid";
    public static final String BIDREJECTEDAPI="rejected-bid";

    public static final String POSTWITHBIDINFO="post-with-bid-info";
    public static final String PostBID="bid-on-post";
    public static final String GETPROVIDERSUBITEM="get-provider-subitems";
    public static final String GETDOCUMENTS="get-documents";
    public static final String DELETEDOCUMENT="delete-document";
    public static final String UPLOADDOCUMENT="upload-document";
    public static final String ADDSUBITEMAPI="add-subitem-to-provider";
    public static final String SEARCHCATEGORYAPI="search-category";
    public static final String GETREVIEWAPI="get-provider-reviews";
    public static final String GETALLBOOKINGS="get-all-bookings";
    public static final String GETALLBOOKINGSServices="get-all-booking-services";
    public static final String GETCURRENTSERVICES="get-current-booking-services";
    public static final String GETCOMPLETEDSERVICES="get-completed-booking-services";
    public static final String GETINPROGRESSSERVICES="get-in-progress-booking-services";
    public static final String BOOKINGDETAILS="booking-details";
    public static final String AcceptRejectApi="accept-reject-booking";
    public static final String GETNOTIFICATION="notifications";
    public static final String UPDATEBOOKINGSTATUS="update-service-status";
    public static final String ALLSERVICE="all-services";
    public static final String PAYMENTTRANSACTION="transactions";
    public static final String UPDATETOKEN="update-device-token";
    public static final String GETTIMESLOTS="get-time-slots";
    public static final String GETCATEGORY="get-categories";
    public static final String ADDAVAILIBITY ="add-availability";
    public static final String GETALLAPPOIMENT ="get-all-appointments";
    public static final String ACCEPTREJECTAPPOINTMENT ="accept-reject-appointment";
    public static final String UPDATEAPPOIMENTSTATUS ="update-appointment-status";
    public static final String GETACCEPTEDAPPOIMENTS ="get-all-accepted-appointments";
    public static final String RESENDOTP ="resend-otp";



}
