package com.wm.muggamuprovider.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.google.android.material.tabs.TabLayout;
import com.wm.muggamuprovider.Activities.MainActivity;
import com.wm.muggamuprovider.Adapter.ServiceBidsAdapter;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.databinding.FragmentHomeBinding;


import java.util.ArrayList;

public class HomeDryCleaningFragment extends Fragment implements View.OnClickListener
{
    private FragmentHomeBinding binding;
    private FragmentTransaction ft;
    private Fragment currentFragment;

    ViewPager simpleViewPager;
    TabLayout tabLayout;
    private ArrayList<Fragment> fragmentArrayList;
    private ArrayList<String> titleList;
    View view;
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        view = binding.getRoot();
        ((MainActivity)requireActivity()).toolbarHome();
        init();


        fragmentArrayList=new ArrayList<>();
        titleList=new ArrayList<>();

        fragmentArrayList.add(new ServicesFragment());
        titleList.add("Services");

        fragmentArrayList.add(new BidsFragment());
        titleList.add("Bids");

        tabLayout.setupWithViewPager(simpleViewPager);
        simpleViewPager.setOffscreenPageLimit(1);

        ServiceBidsAdapter adapter = new ServiceBidsAdapter(getChildFragmentManager(), fragmentArrayList,titleList);
        simpleViewPager.setAdapter(adapter);
        simpleViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        simpleViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                try
                {

                }
                catch (Exception e){e.printStackTrace();}
            }

            @Override
            public void onPageSelected(int position) {
                try{
                    final InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(simpleViewPager.getWindowToken(), 0);
                }
                catch (Exception e){e.printStackTrace();}
            }
            @Override
            public void onPageScrollStateChanged(int state)
            {
                try{ }
                catch (Exception e){e.printStackTrace();}
            }
        });
        return view;
    }

    private void init()
    {
        simpleViewPager = (ViewPager) view.findViewById(R.id.simpleViewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.simpleTabLayout);
        fragmentArrayList=new ArrayList<>();
        titleList=new ArrayList<>();
    }
    @Override
    public void onClick(View v)
    { }


}