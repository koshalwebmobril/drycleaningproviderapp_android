package com.wm.muggamuprovider.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Adapter.AllBookingAdapter;
import com.wm.muggamuprovider.Adapter.ServiceItemsListAdapter;
import com.wm.muggamuprovider.ApiClient.RetrofitConnection;
import com.wm.muggamuprovider.Models.ServiceItemModel;
import com.wm.muggamuprovider.Models.getallbookingmodel.GetAllBookingsModel;

import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.changepasswordmodel.ChangePasswordResponse;
import com.wm.muggamuprovider.Models.getallbookingmodel.GetAllBookingResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.RecyclerTouchListener;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ServicesFragment extends Fragment implements View.OnClickListener
{
    View view;
    RecyclerView recyler_service,recyclerview_allservices_item,recyleritems;
    ServiceItemsListAdapter serviceitemlistadapter;
    AllBookingAdapter allservice_itemadapter;
    List<GetAllBookingsModel> allservicemodellist;
    ArrayList<ServiceItemModel> serviceitemmodellist;
    int position=-1;
    TextView no_item_message;
    String notification_token;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_service_view_pager, container, false);
        init();
        GetData();
        notification_token = FirebaseInstanceId.getInstance().getToken();
        recyler_service.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        serviceitemlistadapter = new ServiceItemsListAdapter(getActivity(),serviceitemmodellist);
        recyler_service.setAdapter(serviceitemlistadapter);
        if(CommonMethod.isOnline(getActivity()))
        {
            hitgetAllServicesApi();
            Updatedevicetoken();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
        }
        recyler_service.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyler_service, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position)
            {
                if(position==0)
                 {
                     if(CommonMethod.isOnline(getActivity()))
                     {
                         hitgetAllServicesApi();
                     }
                     else
                     {
                         CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                     }
                 }
                else if(position==1)
                {
                    if(CommonMethod.isOnline(getActivity()))
                    {
                        hitgetAllCureentServicesApi();
                    }
                    else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                    }
                }
                else if(position==2)
                {
                    if(CommonMethod.isOnline(getActivity()))
                    {
                        hitgetInProgressServicesApi();
                    }
                    else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                    }
                }
                else if(position==3)
                {
                    if(CommonMethod.isOnline(getActivity()))
                    {
                        hitgetCompletedServicesApi();
                    }
                    else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        recyler_service=view.findViewById(R.id.recyler_service);
        recyclerview_allservices_item=view.findViewById(R.id.recyleritems);
        no_item_message=view.findViewById(R.id.no_item_message);
        recyleritems=view.findViewById(R.id.recyleritems);
        serviceitemmodellist=new ArrayList<ServiceItemModel>();
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
          /*  case R.id.imgBack:
                backpressed();
                break;*/
        }
    }

    public void GetData()
    {
        ServiceItemModel restaurentModel1 = new ServiceItemModel("All Services");
        serviceitemmodellist.add(restaurentModel1);

        ServiceItemModel restaurentModel2 = new ServiceItemModel("Current Services");
        serviceitemmodellist.add(restaurentModel2);

        ServiceItemModel restaurentModel3 = new ServiceItemModel("InProgress Services");
        serviceitemmodellist.add(restaurentModel3);

        ServiceItemModel restaurentModel4 = new ServiceItemModel("Completed Services");
        serviceitemmodellist.add(restaurentModel4);
    }


    private void hitgetAllServicesApi()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetAllBookingResponse> call = service.getallbookingServicesapi(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<GetAllBookingResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllBookingResponse> call, retrofit2.Response<GetAllBookingResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetAllBookingResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        no_item_message.setVisibility(View.GONE);
                        recyleritems.setVisibility(View.VISIBLE);
                       // serviceitemmodellist.clear();

                        allservicemodellist=  resultFile.getBookings();

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recyclerview_allservices_item.setLayoutManager(mLayoutManager);
                        allservice_itemadapter = new AllBookingAdapter(getActivity(),allservicemodellist);
                        recyclerview_allservices_item.setAdapter(allservice_itemadapter);
                    }

                    else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recyleritems.setVisibility(View.GONE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllBookingResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void hitgetAllCureentServicesApi()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetAllBookingResponse> call = service.getcurrentServicesapi(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<GetAllBookingResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllBookingResponse> call, retrofit2.Response<GetAllBookingResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetAllBookingResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                       // serviceitemmodellist.clear();
                        no_item_message.setVisibility(View.GONE);
                        recyleritems.setVisibility(View.VISIBLE);

                        allservicemodellist= (ArrayList<GetAllBookingsModel>) resultFile.getBookings();

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recyclerview_allservices_item.setLayoutManager(mLayoutManager);
                        allservice_itemadapter = new AllBookingAdapter(getActivity(),allservicemodellist);
                        recyclerview_allservices_item.setAdapter(allservice_itemadapter);
                    }

                    else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recyleritems.setVisibility(View.GONE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllBookingResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void hitgetCompletedServicesApi()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetAllBookingResponse> call = service.getCompletedServicesapi(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<GetAllBookingResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllBookingResponse> call, retrofit2.Response<GetAllBookingResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetAllBookingResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                       // serviceitemmodellist.clear();
                        no_item_message.setVisibility(View.GONE);
                        recyleritems.setVisibility(View.VISIBLE);

                        allservicemodellist= (ArrayList<GetAllBookingsModel>) resultFile.getBookings();

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recyclerview_allservices_item.setLayoutManager(mLayoutManager);
                        allservice_itemadapter = new AllBookingAdapter(getActivity(),allservicemodellist);
                        recyclerview_allservices_item.setAdapter(allservice_itemadapter);
                    }

                    else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recyleritems.setVisibility(View.GONE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllBookingResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void hitgetInProgressServicesApi()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetAllBookingResponse> call = service.getInProgressServicesapi(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<GetAllBookingResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllBookingResponse> call, retrofit2.Response<GetAllBookingResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetAllBookingResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                       // serviceitemmodellist.clear();
                        no_item_message.setVisibility(View.GONE);
                        recyleritems.setVisibility(View.VISIBLE);

                        allservicemodellist= (ArrayList<GetAllBookingsModel>) resultFile.getBookings();

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recyclerview_allservices_item.setLayoutManager(mLayoutManager);
                        allservice_itemadapter = new AllBookingAdapter(getActivity(),allservicemodellist);
                        recyclerview_allservices_item.setAdapter(allservice_itemadapter);
                    }

                    else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recyleritems.setVisibility(View.GONE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllBookingResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void Updatedevicetoken()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<ChangePasswordResponse> call = service.updatetoken(LoginPreferences.getActiveInstance(getActivity()).getToken(),"2",notification_token);
        call.enqueue(new Callback<ChangePasswordResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, retrofit2.Response<ChangePasswordResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ChangePasswordResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                       //     Toast.makeText(getActivity(), "sucess", Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode() == 404)
                    {

                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}