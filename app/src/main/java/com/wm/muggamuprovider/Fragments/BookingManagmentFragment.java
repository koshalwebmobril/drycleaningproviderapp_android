package com.wm.muggamuprovider.Fragments;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Activities.MainActivity;
import com.wm.muggamuprovider.Adapter.NewAppoinmentManagementAdapter;
import com.wm.muggamuprovider.Adapter.NewBookingManagementAdapter;
import com.wm.muggamuprovider.Adapter.PriviousAppointmentManagementAdapter;
import com.wm.muggamuprovider.Adapter.PriviousBookingManagementAdapter;
import com.wm.muggamuprovider.ApiClient.RetrofitConnection;
import com.wm.muggamuprovider.Models.GetAllAppoiments.AppointmentsItem;
import com.wm.muggamuprovider.Models.GetAllAppoiments.GetAllAppoimentResponse;
import com.wm.muggamuprovider.Models.getbookingmodel.BookingsItemModel;
import com.wm.muggamuprovider.Models.getbookingmodel.GetBookingResponse;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;


import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class BookingManagmentFragment extends Fragment
{
    RecyclerView recycler_new_booking,recycler_privious_booking;
    List<BookingsItemModel> bookingmanagmentlist;                                     //booking module
    NewBookingManagementAdapter bookingmanagmentadapter;
    List<BookingsItemModel> newbookinglist=new ArrayList<>();
    PriviousBookingManagementAdapter priviousBookingManagementAdapter;
    List<BookingsItemModel> proviousbookinglist=new ArrayList<>();


    List<AppointmentsItem> appointmentsItemlist;                                          //appointment module
    NewAppoinmentManagementAdapter newAppoinmentManagementAdapter;
    List<AppointmentsItem> newappoinmentlist=new ArrayList<>();
    PriviousAppointmentManagementAdapter priviousAppointmentManagementAdapter;
    List<AppointmentsItem> priviousappoimentlist =new ArrayList<>();

    View view;
    TextView no_data;
    LinearLayout linear_newpriviousbooking,linear_newbooking,linear_priviousbooking;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_booking_managment, container, false);
        ((MainActivity)requireActivity()).toolbarHomeOther(getString(R.string.bookingmanagement));
        ((MainActivity)requireActivity()).updateBottomBar(1);
        init();
        if(LoginPreferences.getActiveInstance(getActivity()).getCategory_id().equals(getString(R.string.categorystatus)))
       {
           if(CommonMethod.isOnline(getActivity()))
           {
               getBookingManagment();
           }
           else
           {
               CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
           }
       }
       else
       {
           if(CommonMethod.isOnline(getActivity()))
           {
               getAppoimentManagment();
           }
           else
           {
               CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
           }
       }
       return view;
    }
    private void init()
    {
        recycler_new_booking=view.findViewById(R.id.recycler_new_booking);
        recycler_privious_booking=view.findViewById(R.id.recycler_PreviousBooking);
        no_data=view.findViewById(R.id.no_data);
        linear_newpriviousbooking=view.findViewById(R.id.linear_newpriviousbooking);
        linear_newbooking=view.findViewById(R.id.linear_newbooking);
        linear_priviousbooking=view.findViewById(R.id.linear_priviousbooking);
    }

    public void getBookingManagment()
    {
           ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(UrlApi.BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
            ApiInterface service = retrofit.create(ApiInterface.class);
            Call<GetBookingResponse> call = service.getBookings(LoginPreferences.getActiveInstance(getActivity()).getToken());
            call.enqueue(new Callback<GetBookingResponse>()
            {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<GetBookingResponse> call, retrofit2.Response<GetBookingResponse> response)
                {
                    progressDialog.dismiss();
                    try
                    {
                        GetBookingResponse resultFile = response.body();
                        if(resultFile.getCode() == 200)
                        {
                            linear_newpriviousbooking.setVisibility(View.VISIBLE);
                            no_data.setVisibility(View.GONE);
                            bookingmanagmentlist=resultFile.getBookings();
                            for(int i=0;i<bookingmanagmentlist.size();i++)
                            {
                               if(bookingmanagmentlist.get(i).getIsProviderAccepted()==1 || bookingmanagmentlist.get(i).getIsProviderAccepted()==0)
                               {
                                   proviousbookinglist.add(bookingmanagmentlist.get(i));
                               }
                               else
                               {
                                   newbookinglist.add(bookingmanagmentlist.get(i));
                               }
                            }

                            if(proviousbookinglist.size()==0)
                            {
                                linear_priviousbooking.setVisibility(View.GONE);
                            }
                            else if(newbookinglist.size()==0)
                            {
                                linear_newbooking.setVisibility(View.GONE);
                            }
                            recycler_new_booking.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                            bookingmanagmentadapter = new NewBookingManagementAdapter(getActivity(),newbookinglist);
                            recycler_new_booking.setAdapter(bookingmanagmentadapter);

                            recycler_privious_booking.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                            priviousBookingManagementAdapter = new PriviousBookingManagementAdapter(getActivity(),proviousbookinglist);
                            recycler_privious_booking.setAdapter(priviousBookingManagementAdapter);
                        }
                        else if(resultFile.getCode() == 404)
                        {
                            linear_newpriviousbooking.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                            no_data.setText(resultFile.getMessage());
                        }
                        else
                        {
                             Toast.makeText(getActivity(), resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("Login Faild", e.toString());
                    }
                }

                @Override
                public void onFailure(Call<GetBookingResponse> call, Throwable t)
                {
                    Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        }



    private void getAppoimentManagment()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<GetAllAppoimentResponse> call = service.getallappoimentapi(LoginPreferences.getActiveInstance(getActivity()).getToken());

        call.enqueue(new Callback<GetAllAppoimentResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllAppoimentResponse> call, retrofit2.Response<GetAllAppoimentResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetAllAppoimentResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        linear_newpriviousbooking.setVisibility(View.VISIBLE);
                        no_data.setVisibility(View.GONE);
                        appointmentsItemlist=resultFile.getAppointments();

                        for(int i=0;i<appointmentsItemlist.size();i++)
                        {
                            if(appointmentsItemlist.get(i).getIsProviderAccepted()==1 || appointmentsItemlist.get(i).getIsProviderAccepted()==0)
                            {
                                priviousappoimentlist.add(appointmentsItemlist.get(i));
                            }
                            else
                            {
                                newappoinmentlist.add(appointmentsItemlist.get(i));
                            }
                        }

                        if(priviousappoimentlist.size()==0)
                        {
                            linear_priviousbooking.setVisibility(View.GONE);
                        }

                        else if(newappoinmentlist.size()==0)
                        {
                            linear_newbooking.setVisibility(View.GONE);
                        }

                        recycler_new_booking.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        newAppoinmentManagementAdapter = new NewAppoinmentManagementAdapter(getActivity(),newappoinmentlist);
                        recycler_new_booking.setAdapter(newAppoinmentManagementAdapter);

                        recycler_privious_booking.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        priviousAppointmentManagementAdapter = new PriviousAppointmentManagementAdapter(getActivity(),priviousappoimentlist);
                        recycler_privious_booking.setAdapter(priviousAppointmentManagementAdapter);
                    }

                    else if(resultFile.getCode() == 404)
                    {
                        linear_newpriviousbooking.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        no_data.setText(resultFile.getMessage());
                    }
                    else
                    {
                        Toast.makeText(getActivity(), resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllAppoimentResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

    }
}