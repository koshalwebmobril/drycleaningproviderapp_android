package com.wm.muggamuprovider.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamuprovider.Adapter.AcceptedBidsAdapter;
import com.wm.muggamuprovider.Adapter.AllBidsItemsAdapter;
import com.wm.muggamuprovider.Adapter.AllBidsListAdapter;
import com.wm.muggamuprovider.Adapter.RejectsBidsAdapter;
import com.wm.muggamuprovider.ApiClient.RetrofitConnection;
import com.wm.muggamuprovider.Models.BidsItemModel;
import com.wm.muggamuprovider.Models.bidacceptmodel.AllPostItemsModel;


import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.bidacceptmodel.BidAcceptedResponse;
import com.wm.muggamuprovider.Models.getallpostmodel.GetAllPostResponse;
import com.wm.muggamuprovider.Models.bidrejectedmodel.BidRejectedResponse;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;
import com.wm.muggamuprovider.Utils.RecyclerTouchListener;
import com.wm.muggamuprovider.Utils.UrlApi;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class BidsFragment extends Fragment implements View.OnClickListener
{
    RecyclerView recyler_all_bids,recylerbidsitems;
    ArrayList<BidsItemModel> allbidsmodellist;
    AllBidsListAdapter allbidslistadapter;

    AllBidsItemsAdapter allbidsitemadapter;
    AcceptedBidsAdapter acceptedBidsAdapter;
    RejectsBidsAdapter rejectedBidAdapter;
    TextView no_item_message;
    View view;
    List<AllPostItemsModel> allPostItemModels;
    List<AllPostItemsModel> acceptedbidsitemmodellist;
   // List<RejectedBidsModel> rejectedbidsmodellist;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_bids, container, false);
        init();
        GetData();                    //bids items
        recyler_all_bids.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        allbidslistadapter = new AllBidsListAdapter(getActivity(), (ArrayList) allbidsmodellist);
        recyler_all_bids.setAdapter(allbidslistadapter);

        getAllPostApi();

        recyler_all_bids.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyler_all_bids, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position)
            {
                if(position==0)
                {
                    if(CommonMethod.isOnline(getActivity()))
                    {
                        getAllPostApi();
                    }
                    else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                    }
                }
                else if(position==1)
                {
                    if(CommonMethod.isOnline(getActivity()))
                    {
                        bidAcceptedApi();
                    }
                    else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                    }
                }
                else if(position==2)
                {
                    if(CommonMethod.isOnline(getActivity()))
                    {
                        bidrejectedApi();
                    }
                    else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                    }
                }
            }
            @Override
            public void onLongClick(View view, int position) { }
        }));
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        recyler_all_bids=view.findViewById(R.id.recyler_all_bids);
        recylerbidsitems=view.findViewById(R.id.recylerbidsitems);
        no_item_message=view.findViewById(R.id.no_item_message);
        allbidsmodellist=new ArrayList<BidsItemModel>();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
          /*  case R.id.imgBack:
                backpressed();
                break;*/
        }
    }

    public void GetData()
    {
        BidsItemModel restaurentModel1 = new BidsItemModel("All New Bids");
        allbidsmodellist.add(restaurentModel1);

        BidsItemModel restaurentModel2 = new BidsItemModel("Bids Accepted");
        allbidsmodellist.add(restaurentModel2);

        BidsItemModel restaurentModel3 = new BidsItemModel("Bids Rejected");
        allbidsmodellist.add(restaurentModel3);
    }

    private void getAllPostApi()
    {
       ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetAllPostResponse> call = service.getallpostapi(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<GetAllPostResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllPostResponse> call, retrofit2.Response<GetAllPostResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetAllPostResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        no_item_message.setVisibility(View.GONE);
                        recylerbidsitems.setVisibility(View.VISIBLE);

                        allPostItemModels =resultFile.getPostResult();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recylerbidsitems.setLayoutManager(mLayoutManager);
                        allbidsitemadapter = new AllBidsItemsAdapter(getActivity(), (ArrayList) allPostItemModels);
                        recylerbidsitems.setAdapter(allbidsitemadapter);
                    }

                    else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recylerbidsitems.setVisibility(View.GONE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllPostResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
    private void bidAcceptedApi()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<BidAcceptedResponse> call = service.BidAcceptedApi(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<BidAcceptedResponse>()
        {
            @Override
            public void onResponse(Call<BidAcceptedResponse> call, retrofit2.Response<BidAcceptedResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    BidAcceptedResponse resultFile = response.body();
                  //  Toast.makeText(getActivity(), resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                            no_item_message.setVisibility(View.GONE);
                            recylerbidsitems.setVisibility(View.VISIBLE);
                            acceptedbidsitemmodellist =resultFile.getAcceptedBids();

                             LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                             recylerbidsitems.setLayoutManager(mLayoutManager);
                             acceptedBidsAdapter = new AcceptedBidsAdapter(getActivity(), (ArrayList) acceptedbidsitemmodellist);
                             recylerbidsitems.setAdapter(acceptedBidsAdapter);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recylerbidsitems.setVisibility(View.GONE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<BidAcceptedResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void bidrejectedApi()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<BidRejectedResponse> call = service.BidRejectedApi(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<BidRejectedResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<BidRejectedResponse> call, retrofit2.Response<BidRejectedResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    BidRejectedResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                                 no_item_message.setVisibility(View.GONE);
                                 recylerbidsitems.setVisibility(View.VISIBLE);
                                 List<AllPostItemsModel> rejectedbidmodellist=resultFile.getRejectedBids();

                                 LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                                recylerbidsitems.setLayoutManager(mLayoutManager);
                                rejectedBidAdapter = new RejectsBidsAdapter(getActivity(),rejectedbidmodellist);
                                recylerbidsitems.setAdapter(rejectedBidAdapter);
                    }

                    else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        recylerbidsitems.setVisibility(View.GONE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<BidRejectedResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}