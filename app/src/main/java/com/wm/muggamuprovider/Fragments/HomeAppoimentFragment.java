package com.wm.muggamuprovider.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamuprovider.Activities.MainActivity;
import com.wm.muggamuprovider.Adapter.AllAppoimentsAdapter;
import com.wm.muggamuprovider.ApiClient.RetrofitConnection;
import com.wm.muggamuprovider.Models.GetAllAppoiments.AppointmentsItem;
import com.wm.muggamuprovider.Models.GetAllAppoiments.GetAllAppoimentResponse;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Sharedpreference.LoginPreferences;
import com.wm.muggamuprovider.Utils.CommonMethod;
import com.wm.muggamuprovider.Utils.ProgressD;

import com.wm.muggamuprovider.databinding.FragmentHomeAppoimentBinding;
import com.wm.muggamuprovider.network.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class HomeAppoimentFragment extends Fragment implements View.OnClickListener
{
    private FragmentHomeAppoimentBinding binding;
    private FragmentTransaction ft;
    View view;
    List<AppointmentsItem> allappointmentlist;
    AllAppoimentsAdapter allAppoimentsAdapter;

    @Override
    public void onResume()
    {
        super.onResume();
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_appoiment, container, false);
        view = binding.getRoot();
        ((MainActivity)requireActivity()).toolbarHome();

        if(CommonMethod.isOnline(getActivity()))
        {
            hitgetAllAppoimentApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
        }
        return view;
    }
    @Override
    public void onClick(View v)
    { }

    private void hitgetAllAppoimentApi()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<GetAllAppoimentResponse> call = service.getallappoimenthomeapi(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<GetAllAppoimentResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllAppoimentResponse> call, retrofit2.Response<GetAllAppoimentResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetAllAppoimentResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        binding.noItemMessage.setVisibility(View.GONE);
                        binding.recycler.setVisibility(View.VISIBLE);
                        allappointmentlist=  resultFile.getAppointments();

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL,false);
                        binding.recycler.setLayoutManager(mLayoutManager);
                        allAppoimentsAdapter = new AllAppoimentsAdapter(getActivity(),allappointmentlist);
                        binding.recycler.setAdapter(allAppoimentsAdapter);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        binding.noItemMessage.setVisibility(View.VISIBLE);
                        binding.recycler.setVisibility(View.GONE);
                        binding.noItemMessage.setText(resultFile.getMessage());
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllAppoimentResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }



}