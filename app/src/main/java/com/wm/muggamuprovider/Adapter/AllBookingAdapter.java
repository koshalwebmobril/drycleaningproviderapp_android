package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamuprovider.Activities.BookingDetailsActivity;
import com.wm.muggamuprovider.Models.getallbookingmodel.GetAllBookingsModel;
import com.wm.muggamuprovider.R;

import java.util.List;

public class AllBookingAdapter extends RecyclerView.Adapter<AllBookingAdapter.MyViewHolder>
{
    List<GetAllBookingsModel> allserviceitemlist;
    Context context;

    public AllBookingAdapter(Context context, List allserviceitemlist)
    {
        this.context = context;
        this.allserviceitemlist = allserviceitemlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_allservice, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final GetAllBookingsModel getallbookingmodel = allserviceitemlist.get(position);

        Glide.with(context)
                .load(getallbookingmodel.getUserProfile()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile))
                .into(holder.user_image);
        holder.user_name.setText(getallbookingmodel.getUserName());
        holder.time.setText(getallbookingmodel.getBookedAt());
        holder.user_mobile_number.setText(getallbookingmodel.getUserMobile());
        holder.register_no.setText(context.getString(R.string.slashicon)+getallbookingmodel.getOrderId());
        holder.service_name.setText(getallbookingmodel.getServiceName());
        holder.date_and_time.setText(getallbookingmodel.getDeliveryDate());
        holder.service_time.setText(getallbookingmodel.getDeliveryTime());
        holder.address.setText(getallbookingmodel.getPickupLocation());
        holder.Price.setText(context.getString(R.string.currency_icon)+" "+ String.format("%.2f", getallbookingmodel.getTotalAmount()));

        if(getallbookingmodel.getBookingStatus().equals("12"))
        {
            holder.status.setTextColor(Color.parseColor("#2CA86C"));
            holder.status.setText("Completed");
        }
        else if(getallbookingmodel.getBookingStatus().equals("3"))
        {
            holder.status.setTextColor(Color.parseColor("#2CA86C"));
            holder.status.setText("Cancelled");
        }
        else
        {
            holder.status.setTextColor(Color.parseColor("#EE3232"));
            holder.status.setText("Pending");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, BookingDetailsActivity.class);
                i.putExtra("booking_id",String.valueOf(getallbookingmodel.getId()));
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return allserviceitemlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView status,service_item,user_name,time,user_mobile_number,register_no,service_name,Price,date_and_time,service_time,address;
        ImageView user_image;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_item= itemView.findViewById(R.id.service_item);
            user_image=itemView.findViewById(R.id.user_image);
            user_name=itemView.findViewById(R.id.user_name);
            time=itemView.findViewById(R.id.time);
            user_mobile_number=itemView.findViewById(R.id.user_mobile_number);
            register_no=itemView.findViewById(R.id.register_no);
            service_name=itemView.findViewById(R.id.service_name);
            Price=itemView.findViewById(R.id.Price);
            date_and_time=itemView.findViewById(R.id.date_and_time);
            service_time=itemView.findViewById(R.id.service_time);
            address=itemView.findViewById(R.id.address);
            status=itemView.findViewById(R.id.status);
        }
    }
}