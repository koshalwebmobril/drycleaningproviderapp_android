package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamuprovider.Interface.TimeSelectListener;
import com.wm.muggamuprovider.Models.availabilitymanagmentmodel.TimeSlotsItem;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Utils.SpacesItemDecoration;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
public class TimeListAdapter extends RecyclerView.Adapter<com.wm.muggamuprovider.Adapter.TimeListAdapter.TimeListViewHolder> {
    Context context;
    List<TimeSlotsItem> timelist;
    TimeSelectListener timeSelectListene;
    int dateStatus;

    public TimeListAdapter(Context context, List<TimeSlotsItem> timelist,TimeSelectListener timeSelectListene)
    {
        this.context = context;
        this.timelist = timelist;
        this.timeSelectListene = timeSelectListene;
    }
    @NonNull
    @Override
    public TimeListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.time_item, parent, false);
        return new TimeListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeListViewHolder holder, int position)
    {
        TimeSlotsItem subitems = timelist.get(position);
        holder.titleTimeTv.setText(subitems.getTitle());
        GridTimeListAdapter gridTimeListAdapter = new GridTimeListAdapter(context,position,timelist.get(position).getSlots(),timeSelectListene,dateStatus);
        holder.timeGridRecyclerView.setAdapter(gridTimeListAdapter);
    }

    @Override
    public int getItemCount() {
        return timelist.size();
    }

    public class TimeListViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.titleTimeTv)
        TextView titleTimeTv;
        @BindView(R.id.timeGridRecyclerView)
        RecyclerView timeGridRecyclerView;

        public TimeListViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setGridRecyclerView();
        }
        public void setGridRecyclerView()
        {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 4);
            timeGridRecyclerView.setLayoutManager(gridLayoutManager);
            int spacingInPixels = context.getResources().getDimensionPixelSize(R.dimen.spacing);
            timeGridRecyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        }
    }
}
