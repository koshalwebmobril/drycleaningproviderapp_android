package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamuprovider.Interface.ChooseClothListener;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.chooseclothesmodel.ItemsItem;
import com.wm.muggamuprovider.Models.getprovidersubitemsmodel.SubitemDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class SubItemAdapter extends RecyclerView.Adapter<SubItemAdapter.MyViewHolder> implements SubItemChildAdapter.SubItemChildAdapterListener {
    List<SubitemDetailsModel> list;
    Context context;
    ChooseClothListener chooseClothListener;
    public SubItemAdapter(Context context, List<SubitemDetailsModel> list)
    {
        this.context = context;
        this.list = list;
    //    this.chooseClothListener=chooseClothListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.work_log_itemlist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         SubitemDetailsModel subitems = list.get(position);

         holder.category_name.setText(subitems.getCategory());
         holder.arrow_up.setOnClickListener(new View.OnClickListener()
         {
            @Override
            public void onClick(View v)
            {
                holder.arrow_up.setVisibility(View.GONE);
                holder.arrow_down.setVisibility(View.VISIBLE);
                holder.recycle_child.setVisibility(View.GONE);
            }
        });

        holder.arrow_down.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                holder.recycle_child.setVisibility(View.VISIBLE);
                holder.arrow_down.setVisibility(View.GONE);
                holder.arrow_up.setVisibility(View.VISIBLE);
            }
        });
        holder.recycle_child.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        SubItemChildAdapter subitemchildadapter = new SubItemChildAdapter(context,position, (ArrayList) subitems.getItems(),this,chooseClothListener);
        holder.recycle_child.setAdapter(subitemchildadapter);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    @Override
    public void onSaveValue(String trim, ItemsItem itemsItem)
    {
         Log.d("save",trim);
         String getid=String.valueOf(itemsItem.getId());
    }

   /* @Override
    public void updateClothingList(int parentPosition, int childPosition, String price) {
        if (!TextUtils.isEmpty(price)) {
            ProviderSubitemPriceModel providerSubitemPriceModel=new ProviderSubitemPriceModel(Integer.parseInt(price));
            if(list.get(parentPosition).getItems().get(childPosition).getProviderSubitem()==null){
                list.get(parentPosition).getItems().get(childPosition).getProviderSubitem().add(providerSubitemPriceModel);

            }else {
                list.get(parentPosition).getItems().get(childPosition).getProviderSubitem().set(0,providerSubitemPriceModel);

            }
            // Subitemdetailslist.get(parentPosition).getItems().get(childPosition).getProviderSubitem().get(0).setPrice(Integer.parseInt(price));
            //subItemAdapter.notifyDataSetChanged();
        }
    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView category_name;
        ImageView arrow_up,arrow_down;
        RecyclerView recycle_child;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            category_name=itemView.findViewById(R.id.category_name);
            recycle_child =itemView.findViewById(R.id.recycle_child);
            arrow_down=itemView.findViewById(R.id.arrow_down);
            arrow_up=itemView.findViewById(R.id.arrow_up);
        }
    }
}