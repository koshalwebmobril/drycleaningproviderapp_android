package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamuprovider.Activities.BidsDetailsActivity;
import com.wm.muggamuprovider.Models.bidacceptmodel.AllPostItemsModel;
import com.wm.muggamuprovider.R;

import java.io.Serializable;
import java.util.List;
public class AcceptedBidsAdapter extends RecyclerView.Adapter<AcceptedBidsAdapter.MyViewHolder>
{
    List<AllPostItemsModel> acceptedbiditemmodellist;
    Context context;
    public AcceptedBidsAdapter(Context context, List acceptedbiditemmodellist)
    {
        this.context = context;
        this.acceptedbiditemmodellist = acceptedbiditemmodellist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_bids, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        AllPostItemsModel acceptedrejectedbidsitemsmodel = acceptedbiditemmodellist.get(position);
        Glide.with(context).load(acceptedrejectedbidsitemsmodel.getPostAttachment()).apply(new RequestOptions().placeholder(R.drawable.clothes_img).error(R.drawable.clothes_img)).into(holder.service_img);
        Glide.with(context).load(acceptedrejectedbidsitemsmodel.getProfile_image()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile)).into(holder.profile_image);

        holder.service_name.setText(acceptedrejectedbidsitemsmodel.getName());
        holder.created_at.setText(acceptedrejectedbidsitemsmodel.getCreated_at());
        holder.service_date.setText(acceptedrejectedbidsitemsmodel.getServiceDate());
        holder.service_time.setText(acceptedrejectedbidsitemsmodel.getServiceTime());
        holder.username.setText(acceptedrejectedbidsitemsmodel.getUser_name());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, BidsDetailsActivity.class);
                i.putExtra("PostDetailsItem", (Serializable) acceptedrejectedbidsitemsmodel.getPostDetails());
                i.putExtra("service_name",String.valueOf(acceptedrejectedbidsitemsmodel.getName()));
                i.putExtra("service_day_time", acceptedrejectedbidsitemsmodel.getServiceDate());
                i.putExtra("service_address", acceptedrejectedbidsitemsmodel.getServiceAddress());
                i.putExtra("service_remarks", acceptedrejectedbidsitemsmodel.getRemarks());
                i.putExtra("service_image", acceptedrejectedbidsitemsmodel.getPostAttachment());
                i.putExtra("bid_status", acceptedrejectedbidsitemsmodel.getIs_bid_done());
                i.putExtra("page_status", "AcceptReject");
              //  i.putExtra("post_id",String.valueOf(acceptedrejectedbidsitemsmodel.getp));
                i.putExtra("bid_amount",String.valueOf(acceptedrejectedbidsitemsmodel.getBidAmount()));
                i.putExtra("bid_time",String.valueOf(acceptedrejectedbidsitemsmodel.getServiceTime()));
                i.putExtra("username",acceptedrejectedbidsitemsmodel.getUser_name());
                i.putExtra("created_at",String.valueOf(acceptedrejectedbidsitemsmodel.getCreated_at()));
                i.putExtra("service_time",String.valueOf(acceptedrejectedbidsitemsmodel.getServiceTime()));
                context.startActivity(i);
            }
        });

    }
    @Override
    public int getItemCount()
    {
      //  return allserviceitemlist.size();
        return acceptedbiditemmodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_item;
        ImageView service_img,profile_image;
        TextView service_name,service_time,service_date,created_at,username;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_item= itemView.findViewById(R.id.service_item);
            service_img=itemView.findViewById(R.id.service_img);
            service_name=itemView.findViewById(R.id.service_name);
            service_time=itemView.findViewById(R.id.service_time);
            service_date=itemView.findViewById(R.id.service_date);
            created_at=itemView.findViewById(R.id.created_at);
            username=itemView.findViewById(R.id.username);
            profile_image=itemView.findViewById(R.id.profile_image);
        }
    }
}