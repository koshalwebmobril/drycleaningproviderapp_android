package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamuprovider.R;

public class AddServicesAdapter extends RecyclerView.Adapter<AddServicesAdapter.MyViewHolder>
{
   /* ArrayList<ChatsModel> chatslist;*/
    Context context;

    public AddServicesAdapter(Context context)
    {
        this.context = context;
    }

    @Override
    public AddServicesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_get_service, parent, false);
        AddServicesAdapter.MyViewHolder vh = new AddServicesAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final AddServicesAdapter.MyViewHolder holder, final int position)
    {
       /* final ChatsModel albumslistmodel = chatslist.get(position);*/
       // final String name= albumslistmodel.getName();
      //  holder.name.setText(name);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*Intent i=new Intent(context, BookingDetailsActivity.class);
                context.startActivity(i);*/
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            name= itemView.findViewById(R.id.name);
        }
    }
}