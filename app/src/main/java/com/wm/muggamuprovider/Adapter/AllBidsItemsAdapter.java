package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamuprovider.Activities.BidsDetailsActivity;
import com.wm.muggamuprovider.Models.bidacceptmodel.AllPostItemsModel;
import com.wm.muggamuprovider.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AllBidsItemsAdapter extends RecyclerView.Adapter<AllBidsItemsAdapter.MyViewHolder>
{
    List<AllPostItemsModel> allpostlist;
    Context context;
    ImageView menu_icon;

    public AllBidsItemsAdapter(Context context, ArrayList allpostlist)
    {
        this.context = context;
        this.allpostlist = allpostlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_bids, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final AllPostItemsModel allPostItemsModel = allpostlist.get(position);
        Glide.with(context).load(allPostItemsModel.getPostAttachment()).apply(new RequestOptions().placeholder(R.drawable.clothes_img).error(R.drawable.clothes_img)).into(holder.service_img);
        Glide.with(context).load(allPostItemsModel.getProfile_image()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile)).into(holder.profile_image);

        holder.service_name.setText(allPostItemsModel.getName());
        holder.username.setText(allPostItemsModel.getUser_name());
        holder.created_at.setText(allPostItemsModel.getCreated_at());
        holder.service_date.setText(allPostItemsModel.getServiceDate());
        holder.service_time.setText(allPostItemsModel.getServiceTime());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, BidsDetailsActivity.class);
                i.putExtra("PostDetailsItem", (Serializable) allPostItemsModel.getPostDetails());
                i.putExtra("service_name",allPostItemsModel.getName());
                i.putExtra("service_day_time",allPostItemsModel.getServiceDate());
                i.putExtra("service_address",allPostItemsModel.getServiceAddress());
                i.putExtra("service_remarks",allPostItemsModel.getRemarks());
                i.putExtra("service_image",allPostItemsModel.getPostAttachment());
                i.putExtra("post_id",String.valueOf(allPostItemsModel.getId()));
                i.putExtra("username",allPostItemsModel.getUser_name());
                i.putExtra("bid_status",allPostItemsModel.getIs_bid_done());
                i.putExtra("profile_image",allPostItemsModel.getProfile_image());
                i.putExtra("service_image",allPostItemsModel.getPostAttachment());
                i.putExtra("created_at",String.valueOf(allPostItemsModel.getCreated_at()));
                i.putExtra("service_time",String.valueOf(allPostItemsModel.getServiceTime()));
                i.putExtra("page_status", "AllBids");
                context.startActivity(i);
            }
        });

       /* menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                    PopupMenu popup = new PopupMenu(context, v, Gravity.END);
                    popup.inflate(R.menu.poupup_menu);

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                    {
                        @Override
                        public boolean onMenuItemClick(MenuItem item)
                        {
                            switch (item.getItemId())
                            {
                                case R.id.accept:
                                    //handle menu1 click
                                    break;
                                case R.id.reject:
                                    //handle menu2 click
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                }
        });*/
    }
    @Override
    public int getItemCount()
    {
      //  return allserviceitemlist.size();
        return allpostlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_item,text,sd;
        ImageView service_img,profile_image;
        TextView service_name,service_time,service_date,created_at,username;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_item= itemView.findViewById(R.id.service_item);
          //  menu_icon=itemView.findViewById(R.id.menu_icon);
            service_img=itemView.findViewById(R.id.service_img);
            service_name=itemView.findViewById(R.id.service_name);
            service_time=itemView.findViewById(R.id.service_time);
            service_date=itemView.findViewById(R.id.service_date);
            created_at=itemView.findViewById(R.id.created_at);
            username=itemView.findViewById(R.id.username);
            profile_image=itemView.findViewById(R.id.profile_image);
        }
    }
}