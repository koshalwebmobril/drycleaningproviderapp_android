package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamuprovider.Models.BidsItemModel;
import com.wm.muggamuprovider.R;

import java.util.ArrayList;

public class AllBidsListAdapter extends RecyclerView.Adapter<AllBidsListAdapter.MyViewHolder>
{
    ArrayList<BidsItemModel> bidsitemlist;
    Context context;
    int checkedPosition=-1;

    public AllBidsListAdapter(Context context, ArrayList bidsitemlist)
    {
        this.context = context;
        this.bidsitemlist = bidsitemlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_service, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final BidsItemModel albumslistmodel = bidsitemlist.get(position);
        final String name= albumslistmodel.getName();
        holder.service_item.setText(name);

        if (checkedPosition == -1)
        {
            if (position == 0)
            {
                holder.service_item.setTextColor(context.getResources().getColor(R.color.login_bg_color));
            }
            else
            {
                holder.service_item.setTextColor(context.getResources().getColor(R.color.grey_service_item));
            }
        }

        else
            {
            if (checkedPosition == position)
            {
                holder.service_item.setTextColor(context.getResources().getColor(R.color.login_bg_color));
            }
            else {
                holder.service_item.setTextColor(context.getResources().getColor(R.color.grey_service_item));
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if(checkedPosition!=position)
                {
                    checkedPosition=position;
                    notifyItemChanged(checkedPosition);
                    notifyDataSetChanged();
                }
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return bidsitemlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_item;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_item= itemView.findViewById(R.id.service_item);
        }
    }
}