package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamuprovider.Activities.BookingDetailsActivity;
import com.wm.muggamuprovider.Models.paymentlistmodel.PaymentListItem;
import com.wm.muggamuprovider.R;

import java.util.List;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.MyViewHolder>
{
    List<PaymentListItem> paymentlist;
    Context context;

    public PaymentsAdapter(Context context,List paymentlist)
    {
        this.context = context;
        this.paymentlist = paymentlist;
    }

    @Override
    public PaymentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payments, parent, false);
        PaymentsAdapter.MyViewHolder vh = new PaymentsAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final PaymentsAdapter.MyViewHolder holder, final int position)
    {
        final PaymentListItem albumslistmodel = paymentlist.get(position);
        holder.name.setText(albumslistmodel.getUserName());
        holder.dateandtime.setText(albumslistmodel.getDeliveryDate() +","+ albumslistmodel.getDeliveryTime());
        holder.amount.setText(context.getString(R.string.currency_icon)+" "+ String.format("%.2f", albumslistmodel.getTotalAmount()));
        Glide.with(context).load(albumslistmodel.getUserProfileImage()).apply(new RequestOptions().
                placeholder(R.drawable.profile).error(R.drawable.profile)).into(holder.user_image);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, BookingDetailsActivity.class);
                i.putExtra("booking_id",String.valueOf(albumslistmodel.getId()));
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return paymentlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name,dateandtime,amount;
        ImageView user_image;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            name= itemView.findViewById(R.id.name);
            dateandtime= itemView.findViewById(R.id.dateandtime);
            amount= itemView.findViewById(R.id.amount);
            user_image= itemView.findViewById(R.id.user_image);
        }
    }
}