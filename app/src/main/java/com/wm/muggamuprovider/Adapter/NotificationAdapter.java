package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamuprovider.Models.notificationmodel.NotificationModel;
import com.wm.muggamuprovider.R;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>
{
    Context context;
    List<NotificationModel> notificationModelList;
    public NotificationAdapter(Context context, List notificationModelList)
    {
        this.context = context;
        this.notificationModelList=notificationModelList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         NotificationModel notificationmodel = notificationModelList.get(position);
         holder.title.setText(notificationmodel.getTitle());
         holder.title_text.setText(notificationmodel.getDescription());
         String[] splited = notificationmodel.getCreatedAt().split("\\s+");
         holder.timing.setText(splited[0]);
         Log.d("notification",splited[0]);

         holder.itemView.setOnClickListener(view ->
        {
            /*Intent i=new Intent (context, ParticularServiceProvider.class);
            context.startActivity(i);*/
        });
    }
    @Override
    public int getItemCount()
    {
        return notificationModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView title,title_text,timing;
        ImageView notification_icon;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            title_text=itemView.findViewById(R.id.title_text);
            timing=itemView.findViewById(R.id.timing);
            notification_icon=itemView.findViewById(R.id.notification_icon);
        }
    }
}