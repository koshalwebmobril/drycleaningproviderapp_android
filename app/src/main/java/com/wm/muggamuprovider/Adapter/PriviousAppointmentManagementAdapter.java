package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wm.muggamuprovider.Activities.AppoinmentDetailsActivity;
import com.wm.muggamuprovider.Models.GetAllAppoiments.AppointmentsItem;
import com.wm.muggamuprovider.R;

import java.util.List;

public class PriviousAppointmentManagementAdapter extends RecyclerView.Adapter<PriviousAppointmentManagementAdapter.MyViewHolder>
{
    List<AppointmentsItem> bookingmanagmentlist;
    Context context;
    public PriviousAppointmentManagementAdapter(Context context, List bookingmanagmentlist)
    {
        this.context = context;
        this.bookingmanagmentlist = bookingmanagmentlist;
    }

    @Override
    public PriviousAppointmentManagementAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_managment, parent, false);
        PriviousAppointmentManagementAdapter.MyViewHolder vh = new PriviousAppointmentManagementAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final PriviousAppointmentManagementAdapter.MyViewHolder holder, final int position)
    {
        final AppointmentsItem bookingmanagmentmodel = bookingmanagmentlist.get(position);
        Glide.with(context).load(bookingmanagmentmodel.getUserProfileImage()).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(holder.user_image);

        holder.order_no.setText(bookingmanagmentmodel.getAppointmentId());
        holder.dateandtime.setText(bookingmanagmentmodel.getDate()+","+bookingmanagmentmodel.getTime());

        if(TextUtils.isEmpty(bookingmanagmentmodel.getUserName()))
        {
            holder.username.setText(R.string.user);
        }
        else
        {
            holder.username.setText(String.valueOf(bookingmanagmentmodel.getUserName()));
        }


        if(bookingmanagmentmodel.getIsProviderAccepted()==1)
        {
            holder.booking_status_reject.setVisibility(View.GONE);
            holder.booking_status_accept.setVisibility(View.VISIBLE);
        }

        else if(bookingmanagmentmodel.getIsProviderAccepted()==0)
        {
            holder.booking_status_reject.setVisibility(View.VISIBLE);
            holder.booking_status_accept.setVisibility(View.GONE);
        }
        else
        {
            holder.booking_status_reject.setVisibility(View.GONE);
            holder.booking_status_accept.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, AppoinmentDetailsActivity.class);
                i.putExtra("booking_id",String.valueOf(bookingmanagmentmodel.getId()));
                i.putExtra("order_no",String.valueOf(bookingmanagmentmodel.getAppointmentId()));
                i.putExtra("user_name",holder.username.getText().toString().trim());
                i.putExtra("profile_image",String.valueOf(bookingmanagmentmodel.getUserProfileImage()));
                i.putExtra("address",bookingmanagmentmodel.getAddress());
                i.putExtra("dateandtime",holder.dateandtime.getText().toString().trim());
                i.putExtra("isprovideraccepted",String.valueOf(bookingmanagmentmodel.getIsProviderAccepted()));
                i.putExtra("page_open_status","2");
                i.putExtra("user_id",String.valueOf(bookingmanagmentmodel.getUserId()));
                i.putExtra("user_token",String.valueOf(bookingmanagmentmodel.getUser_device_token()));
                i.putExtra("appimentid",String.valueOf(bookingmanagmentmodel.getId()));
                i.putExtra("appoiment_status",String.valueOf(bookingmanagmentmodel.getAppointment_status()));
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return bookingmanagmentlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView username,order_no,dateandtime,booking_status_accept,booking_status_reject;
        ImageView user_image;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            user_image=itemView.findViewById(R.id.user_image);
            username=itemView.findViewById(R.id.username);
            order_no=itemView.findViewById(R.id.order_no);
            dateandtime=itemView.findViewById(R.id.dateandtime);
            booking_status_accept=itemView.findViewById(R.id.booking_status_accept);
            booking_status_reject=itemView.findViewById(R.id.booking_status_reject);
        }
    }
}