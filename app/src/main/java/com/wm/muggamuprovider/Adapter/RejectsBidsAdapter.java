package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamuprovider.Activities.BidsDetailsActivity;
import com.wm.muggamuprovider.Models.bidacceptmodel.AllPostItemsModel;
import com.wm.muggamuprovider.R;

import java.io.Serializable;
import java.util.List;

public class RejectsBidsAdapter extends RecyclerView.Adapter<RejectsBidsAdapter.MyViewHolder>
{
    List<AllPostItemsModel> rejectedbiditemmodellist;
    Context context;
    public RejectsBidsAdapter(Context context, List rejectedbiditemmodellist)
    {
        this.context = context;
        this.rejectedbiditemmodellist = rejectedbiditemmodellist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_bids, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final AllPostItemsModel rejectedbidmodel = rejectedbiditemmodellist.get(position);
        Glide.with(context).load(rejectedbidmodel.getPostAttachment()).apply(new RequestOptions().placeholder(R.drawable.clothes_img).error(R.drawable.clothes_img)).into(holder.service_img);
        Glide.with(context).load(rejectedbidmodel.getProfile_image()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile)).into(holder.profile_image);

        holder.service_name.setText(String.valueOf(rejectedbidmodel.getName()));
        holder.created_at.setText(rejectedbidmodel.getCreated_at());
        holder.service_date.setText(rejectedbidmodel.getServiceDate());
        holder.service_time.setText(rejectedbidmodel.getServiceTime());
        holder.username.setText(rejectedbidmodel.getUser_name());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, BidsDetailsActivity.class);
                i.putExtra("PostDetailsItem", (Serializable) rejectedbidmodel.getPostDetails());
                i.putExtra("service_name",String.valueOf(rejectedbidmodel.getName()));
                i.putExtra("service_day_time", rejectedbidmodel.getServiceDate());
                i.putExtra("bid_time",String.valueOf(rejectedbidmodel.getServiceTime()));
                i.putExtra("service_address", rejectedbidmodel.getServiceAddress());
                i.putExtra("service_remarks", rejectedbidmodel.getRemarks());
                i.putExtra("service_image", rejectedbidmodel.getPostAttachment());
                i.putExtra("bid_status", rejectedbidmodel.getIs_bid_done());
                i.putExtra("page_status", "AcceptReject");
              //  i.putExtra("post_id",String.valueOf(rejectedbidmodel.getPostId()));
                i.putExtra("bid_amount",String.valueOf(rejectedbidmodel.getBidAmount()));
                i.putExtra("username",rejectedbidmodel.getUser_name());
                i.putExtra("created_at",String.valueOf(rejectedbidmodel.getCreated_at()));
                i.putExtra("service_time",String.valueOf(rejectedbidmodel.getServiceTime()));
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return rejectedbiditemmodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_item;
        ImageView service_img,profile_image;
        TextView service_name,service_time,service_date,created_at,username;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_item= itemView.findViewById(R.id.service_item);
            service_img=itemView.findViewById(R.id.service_img);
            service_name=itemView.findViewById(R.id.service_name);
            service_time=itemView.findViewById(R.id.service_time);
            service_date=itemView.findViewById(R.id.service_date);
            created_at=itemView.findViewById(R.id.created_at);
            username=itemView.findViewById(R.id.username);
            profile_image=itemView.findViewById(R.id.profile_image);
        }
    }
}