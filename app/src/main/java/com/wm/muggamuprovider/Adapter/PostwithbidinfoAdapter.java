package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.wm.muggamuprovider.Activities.BidsDetailsActivity;
import com.wm.muggamuprovider.Models.bidacceptmodel.AllPostItemsModel;
import com.wm.muggamuprovider.R;

import java.util.ArrayList;
import java.util.List;

public class PostwithbidinfoAdapter extends RecyclerView.Adapter<PostwithbidinfoAdapter.MyViewHolder>
{
    List<AllPostItemsModel> allpostlist;
    Context context;
    ImageView menu_icon;
    public PostwithbidinfoAdapter(Context context, ArrayList allpostlist)
    {
        this.context = context;
        this.allpostlist = allpostlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final AllPostItemsModel allPostItemsModel = allpostlist.get(position);
        Picasso.get().load(allPostItemsModel.getPostAttachment()).into(holder.service_img);
        holder.service_name.setText(allPostItemsModel.getName());
        holder.service_time1.setText(allPostItemsModel.getServiceTime());
        holder.service_date.setText(allPostItemsModel.getServiceDate());
       // holder.service_time.setText(allPostItemsModel.getServiceTime());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, BidsDetailsActivity.class);
                i.putExtra("service_name",allPostItemsModel.getName());
                i.putExtra("service_day_time",allPostItemsModel.getServiceDate());
                i.putExtra("service_address",allPostItemsModel.getServiceAddress());
                i.putExtra("service_remarks",allPostItemsModel.getRemarks());
                i.putExtra("service_image",allPostItemsModel.getPostAttachment());
                //  i.putExtra("bid_price",allPostItemsModel.get)
                context.startActivity(i);
            }
        });

        menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                    PopupMenu popup = new PopupMenu(context, v, Gravity.END);
                    popup.inflate(R.menu.poupup_menu);

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                    {
                        @Override
                        public boolean onMenuItemClick(MenuItem item)
                        {
                            switch (item.getItemId())
                            {
                                case R.id.accept:
                                    //handle menu1 click
                                    break;
                                case R.id.reject:
                                    //handle menu2 click
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                }
        });
    }
    @Override
    public int getItemCount()
    {
      //  return allserviceitemlist.size();
        return allpostlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_item,text,sd;
        ImageView service_img;
        TextView service_name,service_time,service_date,service_time1;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_item= itemView.findViewById(R.id.service_item);
          //  menu_icon=itemView.findViewById(R.id.menu_icon);
            service_img=itemView.findViewById(R.id.service_img);
            service_name=itemView.findViewById(R.id.service_name);
            service_time=itemView.findViewById(R.id.service_time);
            service_date=itemView.findViewById(R.id.service_date);
            service_time1=itemView.findViewById(R.id.created_at);
        }
    }
}