package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamuprovider.Activities.ChooseClothesActivity;
import com.wm.muggamuprovider.Interface.ChooseClothListener;
import com.wm.muggamuprovider.Models.chooseclothesmodel.ProviderSubitemPriceModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Models.chooseclothesmodel.ItemsItem;


import java.util.List;

public class SubItemChildAdapter extends RecyclerView.Adapter<SubItemChildAdapter.MyViewHolder> {
    List<ItemsItem> itemslist;
    Context context;
    private SubItemChildAdapterListener subItemChildAdapterListener;
    ChooseClothListener chooseClothListener;
    int parentPosition;

    public SubItemChildAdapter(Context context, int parentPosition, List<ItemsItem> itemslist, SubItemChildAdapterListener subItemChildAdapterListener, ChooseClothListener chooseClothListener) {
        this.context = context;
        this.itemslist = itemslist;
        this.subItemChildAdapterListener = subItemChildAdapterListener;
        this.chooseClothListener = chooseClothListener;
        this.parentPosition = parentPosition;
    }

    public interface SubItemChildAdapterListener {
        void onSaveValue(String trim, ItemsItem itemsItem);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.work_log_child_itemlist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ItemsItem ItemsItem = itemslist.get(position);
        holder.item_name.setText(ItemsItem.getItemName());

       /* if (ItemsItem.getProviderSubitem()!=null&&ItemsItem.getProviderSubitem().size()>0)
        {
            holder.item_price.setText(""+ItemsItem.getProviderSubitem().get(0).getPrice());
        }
        else
             {
                   holder.item_price.setText("");
             }*/

        if (ChooseClothesActivity.getInstance().Subitemdetailslist.get(parentPosition).getItems().get(position).getProviderSubitem() != null && ChooseClothesActivity.getInstance().Subitemdetailslist.get(parentPosition).getItems().get(position).getProviderSubitem().size() > 0) {
            holder.item_price.setText("" + ChooseClothesActivity.getInstance().Subitemdetailslist.get(parentPosition).getItems().get(position).getProviderSubitem().get(0).getPrice());

        } else {
            holder.item_price.setText("");

        }


        holder.item_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // subItemChildAdapterListener.onSaveValue(holder.item_price.getText().toString().trim(), itemslist.get(position));
                //  chooseClothListener.updateClothingList(parentPosition,position, s.toString());
                float price;
                if(TextUtils.isEmpty(s.toString())){
                    price= (float) 0.00;
                }else
                    {
                         price=Float.parseFloat(s.toString());
                }
                if(ChooseClothesActivity.getInstance().Subitemdetailslist.get(parentPosition).getItems().get(position).getProviderSubitem() != null&& ChooseClothesActivity.getInstance().Subitemdetailslist.get(parentPosition).getItems().get(position).getProviderSubitem().size() > 0)
                {
                    ChooseClothesActivity.getInstance().Subitemdetailslist.get(parentPosition).getItems().get(position).getProviderSubitem().get(0).setPrice(price);
                }
                else
                    {
                    ProviderSubitemPriceModel providerSubitemPriceModel=new ProviderSubitemPriceModel(price);
                    ChooseClothesActivity.getInstance().Subitemdetailslist.get(parentPosition).getItems().get(position).getProviderSubitem().add(providerSubitemPriceModel);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView item_name;
        EditText item_price;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_name = itemView.findViewById(R.id.item_name);
            item_price = itemView.findViewById(R.id.item_price);
        }
    }
}