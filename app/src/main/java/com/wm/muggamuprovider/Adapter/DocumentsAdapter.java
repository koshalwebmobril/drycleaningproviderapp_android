package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamuprovider.Activities.ViewDocumentsActivity;
import com.wm.muggamuprovider.Interface.OnClickDelete;
import com.wm.muggamuprovider.Models.getdocumentmodel.DocumentsItemModel;
import com.wm.muggamuprovider.R;

import java.util.ArrayList;

public class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.MyViewHolder>
{
    ArrayList<DocumentsItemModel> documentlist;
    Context context;
    OnClickDelete onClickDelete;

    public DocumentsAdapter(Context context,ArrayList documentlist,OnClickDelete onClickDelete)
    {
        this.context = context;
        this.documentlist=documentlist;
        this.onClickDelete=onClickDelete;
    }

    @Override
    public DocumentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_documents, parent, false);
        DocumentsAdapter.MyViewHolder vh = new DocumentsAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final DocumentsAdapter.MyViewHolder holder, final int position)
    {
        final DocumentsItemModel documentsItemModel = documentlist.get(position);
        holder.document_name.setText(documentsItemModel.getName());
        holder.upload_time.setText(documentsItemModel.getCreatedAt());

        if(documentsItemModel.getIsApproved()==0)
        {
            holder.document_status_rejected.setVisibility(View.VISIBLE);
            holder.document_status_approved.setVisibility(View.GONE);
        }
        else
        {
            holder.document_status_rejected.setVisibility(View.GONE);
            holder.document_status_approved.setVisibility(View.VISIBLE);
        }
        holder.delete_icon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onClickDelete.delete(documentsItemModel.getId(),position);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, ViewDocumentsActivity.class);
                i.putExtra("document_path",documentsItemModel.getPath());
                i.putExtra("document_name",documentsItemModel.getName());
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return documentlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView document_name,upload_time,document_status_approved,document_status_rejected;
        ImageView delete_icon;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            document_name= itemView.findViewById(R.id.document_name);
            upload_time=itemView.findViewById(R.id.upload_time);
            delete_icon=itemView.findViewById(R.id.delete_icon);
            document_status_approved=itemView.findViewById(R.id.document_status_approved);
            document_status_rejected=itemView.findViewById(R.id.document_status_rejected);
        }
    }
}