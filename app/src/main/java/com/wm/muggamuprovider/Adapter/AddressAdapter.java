package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamuprovider.Activities.NewAddressActivity;
import com.wm.muggamuprovider.R;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder>
{
   /* ArrayList<ChatsModel> chatslist;*/
    Context context;
    ImageView menu_icon;

    public AddressAdapter(Context context)
    {
        this.context = context;
    }

    @Override
    public AddressAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address, parent, false);
        AddressAdapter.MyViewHolder vh = new AddressAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final AddressAdapter.MyViewHolder holder, final int position)
    {
       /* final ChatsModel albumslistmodel = chatslist.get(position);*/
       // final String name= albumslistmodel.getName();
      //  holder.name.setText(name);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*Intent i=new Intent(context, BookingDetailsActivity.class);
                context.startActivity(i);*/
            }
        });


        menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                PopupMenu popup = new PopupMenu(context, v, Gravity.END);
                popup.inflate(R.menu.poupup_menu_address);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        switch (item.getItemId())
                        {
                            case R.id.edit:
                                Intent i=new Intent(context, NewAddressActivity.class);
                                context.startActivity(i);
                                break;

                            case R.id.remove:
                                //handle menu2 click
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            name= itemView.findViewById(R.id.name);
            menu_icon=itemView.findViewById(R.id.menu);
        }
    }
}