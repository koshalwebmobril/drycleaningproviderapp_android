package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamuprovider.Interface.TimeSelectListener;
import com.wm.muggamuprovider.Models.availabilitymanagmentmodel.SlotsItem;
import com.wm.muggamuprovider.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridTimeListAdapter extends RecyclerView.Adapter<GridTimeListAdapter.GridTimeViewHolder> {
    Context context;
    List<SlotsItem> timeGridList;
    TimeSelectListener timeSelectListene;
    int datestatus;
    int parentPosition;

    public GridTimeListAdapter(Context context, int parentPosition, List<SlotsItem> timeGridList, TimeSelectListener timeSelectListene, int datestatus) {
        this.context = context;
        this.timeGridList = timeGridList;
        this.timeSelectListene = timeSelectListene;
        this.datestatus = datestatus;
        this.parentPosition = parentPosition;
    }

    @NonNull
    @Override
    public GridTimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.grid_time_item, parent, false);
        return new GridTimeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GridTimeViewHolder holder, int position) {
        SlotsItem timeItem = timeGridList.get(position);
        holder.timeTV.setText(timeItem.getSlot());
        holder.itemView.setOnClickListener(view ->
        {
            if (timeItem.getAvailability_status().equals("1"))
            {
                timeSelectListene.onSetAvailabilityStatus(parentPosition, position, "2");
                timeGridList.get(position).setAvailability_status("2");
                notifyDataSetChanged();
            }
            else if (timeItem.getAvailability_status().equals("2"))
            {
                timeSelectListene.onSetAvailabilityStatus(parentPosition, position, "1");
                timeGridList.get(position).setAvailability_status("1");
                notifyDataSetChanged();
            }
        });

        if (timeItem.getAvailability_status().equals("1"))
        {
            holder.timeTV.setBackground(ContextCompat.getDrawable(context, R.drawable.un_selected_time_bg));
            holder.timeTV.setTextColor(ContextCompat.getColor(context, R.color.login_bg_color));
        }
        else if (timeItem.getAvailability_status().equals("2"))
        {
            holder.timeTV.setBackground(ContextCompat.getDrawable(context, R.drawable.unselectable_time_bg));
            holder.timeTV.setTextColor(ContextCompat.getColor(context, R.color.white));
        }

        else if (timeItem.getAvailability_status().equals("3"))
        {
            holder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(timeItem.getAvailability_status().equals("3"))
                    {
                        Toast.makeText(context, context.getString(R.string.cannotselectpasttime), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return timeGridList.size();
    }

    public class GridTimeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.timeTV)
        TextView timeTV;

        public GridTimeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
