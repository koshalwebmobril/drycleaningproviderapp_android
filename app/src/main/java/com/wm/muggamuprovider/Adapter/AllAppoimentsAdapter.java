package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamuprovider.Activities.AppoinmentDetailsActivity;
import com.wm.muggamuprovider.Models.GetAllAppoiments.AppointmentsItem;
import com.wm.muggamuprovider.R;


import java.util.List;

public class AllAppoimentsAdapter extends RecyclerView.Adapter<AllAppoimentsAdapter.MyViewHolder>
{
    List<AppointmentsItem> appoimentlist;
    Context context;
    public AllAppoimentsAdapter(Context context, List appoimentlist)
    {
        this.context = context;
        this.appoimentlist = appoimentlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_appoiment, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final AppointmentsItem getallbookingmodel = appoimentlist.get(position);
        holder.linear_all_data.setVisibility(View.VISIBLE);
        Glide.with(context)
                .load(getallbookingmodel.getUserProfileImage()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile))
                .into(holder.user_image);
        holder.time.setText(getallbookingmodel.getBookedAt());
        holder.user_mobile_number.setText(getallbookingmodel.getUserMobile());
        holder.register_no.setText(getallbookingmodel.getAppointmentId());
        holder.date_and_time.setText(getallbookingmodel.getDate());
        holder.service_time.setText(getallbookingmodel.getTime());
        holder.service_name.setText(getallbookingmodel.getService_name());

        if(TextUtils.isEmpty(getallbookingmodel.getUserName()))
        {
            holder.user_name.setText(R.string.user);
        }
        else
        {
            holder.user_name.setText(String.valueOf(getallbookingmodel.getUserName()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, AppoinmentDetailsActivity.class);
                i.putExtra("booking_id",String.valueOf(getallbookingmodel.getId()));
                i.putExtra("order_no",String.valueOf(getallbookingmodel.getAppointmentId()));
                i.putExtra("user_name",holder.user_name.getText().toString().trim());
                i.putExtra("profile_image",String.valueOf(getallbookingmodel.getUserProfileImage()));
                i.putExtra("address",getallbookingmodel.getAddress());
                i.putExtra("dateandtime",getallbookingmodel.getDate());
                i.putExtra("isprovideraccepted",String.valueOf(getallbookingmodel.getIsProviderAccepted()));
                i.putExtra("page_open_status","1");
                i.putExtra("user_id",String.valueOf(getallbookingmodel.getUserId()));
                i.putExtra("user_token",String.valueOf(getallbookingmodel.getUser_device_token()));
                i.putExtra("appimentid",String.valueOf(getallbookingmodel.getId()));
                i.putExtra("appoiment_status",String.valueOf(getallbookingmodel.getAppointment_status()));
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount() {
        return appoimentlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_name,txtstatus,user_name,time,user_mobile_number,register_no,Price,date_and_time,service_time,address;
        ImageView user_image;
        LinearLayout linear_all_data;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            user_image=itemView.findViewById(R.id.user_image);
            user_name=itemView.findViewById(R.id.user_name);
            time=itemView.findViewById(R.id.time);
            user_mobile_number=itemView.findViewById(R.id.user_mobile_number);
            register_no=itemView.findViewById(R.id.register_no);
            service_name=itemView.findViewById(R.id.service_name);
            Price=itemView.findViewById(R.id.Price);
            date_and_time=itemView.findViewById(R.id.date_and_time);
            service_time=itemView.findViewById(R.id.service_time);
            txtstatus=itemView.findViewById(R.id.txtstatus);
            service_name=itemView.findViewById(R.id.service_name);
            linear_all_data=itemView.findViewById(R.id.linear_all_data);
        }
    }
}