package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamuprovider.Models.bookingdetailsmodel.BookingDetailsParentModel;
import com.wm.muggamuprovider.R;

import java.util.List;

public class BookingDetailsParentAdapter extends RecyclerView.Adapter<BookingDetailsParentAdapter.MyViewHolder>
{
    Context context;
    List<BookingDetailsParentModel> bookingDetailsParentModelList;
    public BookingDetailsParentAdapter(Context context, List bookingDetailsParentModelList)
    {
        this.context = context;
        this.bookingDetailsParentModelList=bookingDetailsParentModelList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_details_parent, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
        BookingDetailsParentModel detailslist = bookingDetailsParentModelList.get(position);
        holder.service_name.setText(String.valueOf(detailslist.getServiceName()));

        holder.recycler_child.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        BookingDetailsChildAdapter orderDetailsChildAdapter = new BookingDetailsChildAdapter(context,detailslist.getItems());
        holder.recycler_child.setAdapter(orderDetailsChildAdapter);
    }
    @Override
    public int getItemCount()
    {
        return bookingDetailsParentModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_name;
        RecyclerView recycler_child;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_name=itemView.findViewById(R.id.service_name);
            recycler_child=itemView.findViewById(R.id.recycler_child);
        }
    }
}