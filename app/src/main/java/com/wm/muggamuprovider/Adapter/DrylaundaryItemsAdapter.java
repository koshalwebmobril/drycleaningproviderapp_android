package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.wm.muggamuprovider.Models.getserviceslistmodel.ProviderServicesItemModel;
import com.wm.muggamuprovider.R;

import java.util.List;
public class DrylaundaryItemsAdapter extends RecyclerView.Adapter<DrylaundaryItemsAdapter.MyViewHolder>
{
    List<ProviderServicesItemModel> providerServicesItemModelslist;
    Context context;
    public DrylaundaryItemsAdapter(Context context,List providerServicesItemModelslist)
    {
        this.context = context;
        this.providerServicesItemModelslist = providerServicesItemModelslist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.drylaundry_item_view, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         final ProviderServicesItemModel serviceitemmodel = providerServicesItemModelslist.get(position);
         String upperString = serviceitemmodel.getName().substring(0, 1).toUpperCase() + serviceitemmodel.getName().substring(1).toLowerCase();
         holder.name.setText(upperString);
         Glide.with(context).load(serviceitemmodel.getThumbnailPath()).into(holder.service_img);
    }
    @Override
    public int getItemCount()
    {
        return providerServicesItemModelslist.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        ImageView service_img;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.service_name);
            service_img=itemView.findViewById(R.id.service_img);
        }

    }
}