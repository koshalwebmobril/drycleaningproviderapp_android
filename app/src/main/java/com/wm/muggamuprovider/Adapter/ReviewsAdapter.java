package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wm.muggamuprovider.Models.getreviewmodel.ReviewListItem;
import com.wm.muggamuprovider.R;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder>
{
   List<ReviewListItem> reviewlist;
    Context context;

    public ReviewsAdapter(Context context, List reviewlist)
    {
        this.context = context;
        this.reviewlist= reviewlist;
    }

    @Override
    public ReviewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reviews, parent, false);
        ReviewsAdapter.MyViewHolder vh = new ReviewsAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final ReviewsAdapter.MyViewHolder holder, final int position)
    {
        final ReviewListItem reviewlistmodel = reviewlist.get(position);
        Glide.with(context).load(reviewlistmodel.getUserProfileImage()).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(holder.user_profile);
        holder.txt_username.setText(reviewlistmodel.getUserName());
        Glide.with(context).load(reviewlistmodel.getImage()).error(R.drawable.clothes).placeholder(R.drawable.clothes)
                .into(holder.clothes);
        holder.service_name.setText(reviewlistmodel.getBookingServiceName());
        holder.service_date.setText(reviewlistmodel.getBookingDate());
        holder.service_time.setText(reviewlistmodel.getBookingTime());
        holder.review_feedback.setText(reviewlistmodel.getReview());
        holder.rating_bar.setRating(reviewlistmodel.getRating());
        holder.created_at.setText(reviewlistmodel.getCreatedAt());


        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               /* Intent i=new Intent(context, PaymentDetailsActivity.class);
                context.startActivity(i);*/
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return reviewlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_username,service_name,service_date,service_time,review_feedback,created_at;
        ImageView user_profile,clothes;
        RatingBar rating_bar;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            txt_username= itemView.findViewById(R.id.txt_username);
            user_profile=itemView.findViewById(R.id.user_profile);
            rating_bar=itemView.findViewById(R.id.rating_bar);
            clothes=itemView.findViewById(R.id.clothes);
            service_name=itemView.findViewById(R.id.service_name);
            service_date=itemView.findViewById(R.id.service_date);
            service_time=itemView.findViewById(R.id.service_time);
            review_feedback=itemView.findViewById(R.id.review_feedback);
            created_at=itemView.findViewById(R.id.created_at);
        }
    }
}