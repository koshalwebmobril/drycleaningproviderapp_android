package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamuprovider.Models.bidacceptmodel.PostDetailsItem;
import com.wm.muggamuprovider.R;

import java.util.List;

public class AddPostDetailsAdapter extends RecyclerView.Adapter<AddPostDetailsAdapter.MyViewHolder>
{
    Context context;
    List<PostDetailsItem> cartitemlist;

    public AddPostDetailsAdapter(Context context, List cartitemlist)
    {
        this.context = context;
        this.cartitemlist=cartitemlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_post_recycler, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         PostDetailsItem parentmodel = cartitemlist.get(position);
         holder.txt_qty.setText(String.valueOf(parentmodel.getSubitemQuantity()) +" "+"x"+" "+ String.valueOf(parentmodel.getSubitemName()));
    }
    @Override
    public int getItemCount()
    {
        return cartitemlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_name,txt_qty,txtitemname;
        LinearLayout linear_parent_name;
        public MyViewHolder(View itemView)
        {
            super(itemView);
          //  service_name=itemView.findViewById(R.id.service_name);
            txt_qty=itemView.findViewById(R.id.txt_qty);
         //   txtitemname=itemView.findViewById(R.id.txtitemname);
         //   linear_parent_name=itemView.findViewById(R.id.linear_parent_name);
        }
    }
}