package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamuprovider.Models.updateprofilemodel.DaysItemListModel;

import com.wm.muggamuprovider.R;



import java.util.List;

public class UserSelectDaysAdapter extends RecyclerView.Adapter<UserSelectDaysAdapter.MyViewHolder> {

    List<DaysItemListModel> daysitemlist;
    Context context;
    public ItemCheckListner itemCheckListner;
    public UserSelectDaysAdapter(Context context, List<DaysItemListModel> daysitemlist) {
        this.context = context;
        this.daysitemlist = daysitemlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selectdays, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        context = holder.itemView.getContext();

        DaysItemListModel selectlangaugeItem = daysitemlist.get(position);

        final String name = selectlangaugeItem.getDayname();
        String getstatus_selected = String.valueOf(selectlangaugeItem.getValue());

        if(getstatus_selected.equals("1"))
       {
           holder.relative_day.setBackgroundColor(Color.parseColor("#0079A9"));
           holder.day_name.setText(name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase());
           holder.day_name.setTextColor(Color.parseColor("#FFFFFF"));
       }
       else
       {
           holder.relative_day.setBackgroundResource(R.drawable.selector_edittext_days);
           holder.day_name.setText(name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase());
           holder.day_name.setTextColor(Color.parseColor("#FF000000"));
       }


       holder.relative_day.setOnClickListener(view ->
        {
            if (getstatus_selected.equals("0"))
            {
                holder.relative_day.setBackgroundResource(R.drawable.selector_edittext_days);
                holder.day_name.setTextColor(Color.parseColor("#FF000000"));
                itemCheckListner.onItemSelect(position,1);
            }
            else
                {
                    holder.relative_day.setBackgroundColor(Color.parseColor("#0079A9"));
                    holder.day_name.setTextColor(Color.parseColor("#FFFFFF"));
                    itemCheckListner.onItemSelect(position,0);
                }
        });


        holder.day_name.setOnClickListener(view ->
        {
            if (getstatus_selected.equals("0"))
            {
                holder.relative_day.setBackgroundResource(R.drawable.selector_edittext_days);
                holder.day_name.setTextColor(Color.parseColor("#FF000000"));
                itemCheckListner.onItemSelect(position,1);
            }
            else
            {
                holder.relative_day.setBackgroundColor(Color.parseColor("#0079A9"));
                holder.day_name.setTextColor(Color.parseColor("#FFFFFF"));
                itemCheckListner.onItemSelect(position,0);
            }
        });
    }

    public interface ItemCheckListner {
        void setCheckedList(String list);

        void remove(DaysItemListModel daysselectitem);

        void onItemSelect(int position, int i);
    }


    public void setCheckedItemListner(ItemCheckListner itemCheckListner) {
        this.itemCheckListner = itemCheckListner;
    }

    @Override
    public int getItemCount() {
        return daysitemlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView day_name;
        RelativeLayout relative_day;
        ImageView checked, checked_afterselect;
        String user_selected_language_id;

        public MyViewHolder(View itemView) {
            super(itemView);
            day_name = itemView.findViewById(R.id.day_name);
            relative_day = itemView.findViewById(R.id.relative_day);
        }
    }
}