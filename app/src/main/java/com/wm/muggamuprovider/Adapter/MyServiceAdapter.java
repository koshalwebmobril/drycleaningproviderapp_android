package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wm.muggamuprovider.Activities.AddServicesActivity;
import com.wm.muggamuprovider.Models.getservicecategorymodel.GetServiceCategoryModel;
import com.wm.muggamuprovider.R;
import com.wm.muggamuprovider.Utils.Singleton;

import java.util.List;

public class MyServiceAdapter extends RecyclerView.Adapter<MyServiceAdapter.MyViewHolder> {
    List<GetServiceCategoryModel> myservicelist;
    Context context;
    Singleton singleton = Singleton.getInstance();

    public MyServiceAdapter(Context context, List myservicelist) {
        this.context = context;
        this.myservicelist = myservicelist;
    }

    @Override
    public MyServiceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_service, parent, false);
        MyServiceAdapter.MyViewHolder vh = new MyServiceAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyServiceAdapter.MyViewHolder holder, final int position) {
        final GetServiceCategoryModel albumslistmodel = myservicelist.get(position);
        holder.laundary_name.setText(albumslistmodel.getTitle());
        holder.description.setText(albumslistmodel.getDetails());

        Glide.with(context).load(albumslistmodel.getThumbnailPath()).error(R.drawable.drylaundary).placeholder(R.drawable.drylaundary)
                .into(holder.user_image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddServicesActivity.class);
                i.putExtra("category_id", String.valueOf(albumslistmodel.getId()));
                i.putExtra("image", albumslistmodel.getThumbnailPath());
                i.putExtra("heading_name", albumslistmodel.getTitle());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myservicelist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView laundary_name, description, created_at;
        ImageView user_image, img_service;

        public MyViewHolder(View itemView) {
            super(itemView);
            laundary_name = itemView.findViewById(R.id.laundary_name);
            user_image = itemView.findViewById(R.id.user_image);
            description = itemView.findViewById(R.id.description);
            img_service = itemView.findViewById(R.id.img_service);
            created_at = itemView.findViewById(R.id.created_at);
        }
    }
}