package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wm.muggamuprovider.Activities.BookingDetailsActivity;
import com.wm.muggamuprovider.Models.getbookingmodel.BookingsItemModel;
import com.wm.muggamuprovider.R;

import java.util.List;

public class PriviousBookingManagementAdapter extends RecyclerView.Adapter<PriviousBookingManagementAdapter.MyViewHolder>
{
    List<BookingsItemModel> bookingmanagmentlist;
    Context context;

    public PriviousBookingManagementAdapter(Context context, List bookingmanagmentlist)
    {
        this.context = context;
        this.bookingmanagmentlist = bookingmanagmentlist;
    }

    @Override
    public PriviousBookingManagementAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_managment, parent, false);
        PriviousBookingManagementAdapter.MyViewHolder vh = new PriviousBookingManagementAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final PriviousBookingManagementAdapter.MyViewHolder holder, final int position)
    {
        final BookingsItemModel bookingmanagmentmodel = bookingmanagmentlist.get(position);
        Glide.with(context).load(bookingmanagmentmodel.getUserProfileImage()).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(holder.user_image);
        holder.username.setText(bookingmanagmentmodel.getUserName());
        holder.dateandtime.setText(bookingmanagmentmodel.getDeliveryDate()+" at " + bookingmanagmentmodel.getDeliveryTime());
        holder.order_no.setText(bookingmanagmentmodel.getOrderId());

        if(bookingmanagmentmodel.getIsProviderAccepted()==1)
        {
            holder.booking_status_reject.setVisibility(View.GONE);
            holder.booking_status_accept.setVisibility(View.VISIBLE);
        }

        else if(bookingmanagmentmodel.getIsProviderAccepted()==0)
        {
            holder.booking_status_reject.setVisibility(View.VISIBLE);
            holder.booking_status_accept.setVisibility(View.GONE);
        }

        else
        {
            holder.booking_status_reject.setVisibility(View.GONE);
            holder.booking_status_accept.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, BookingDetailsActivity.class);
                i.putExtra("booking_id",String.valueOf(bookingmanagmentmodel.getId()));
                i.putExtra("booking_status",String.valueOf(bookingmanagmentmodel.getIsProviderAccepted()));
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return bookingmanagmentlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView username,order_no,dateandtime,booking_status_accept,booking_status_reject;
        ImageView user_image;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            user_image=itemView.findViewById(R.id.user_image);
            username=itemView.findViewById(R.id.username);
            order_no=itemView.findViewById(R.id.order_no);
            dateandtime=itemView.findViewById(R.id.dateandtime);
            booking_status_accept=itemView.findViewById(R.id.booking_status_accept);
            booking_status_reject=itemView.findViewById(R.id.booking_status_reject);
        }
    }
}