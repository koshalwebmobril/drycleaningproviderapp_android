package com.wm.muggamuprovider.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamuprovider.Models.bookingdetailsmodel.BookingDetailsChildModel;
import com.wm.muggamuprovider.R;

import java.util.List;

public class BookingDetailsChildAdapter extends RecyclerView.Adapter<BookingDetailsChildAdapter.MyViewHolder> {
    List<BookingDetailsChildModel> itemslist;
    Context context;
    public BookingDetailsChildAdapter(Context context, List itemslist)
    {
        this.context = context;
        this.itemslist = itemslist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_details_child, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        BookingDetailsChildModel ItemsItem = itemslist.get(position);
        holder.txt_qty.setText(String.valueOf(ItemsItem.getSubitemQuantity()+" X "+ItemsItem.getSubitemName()));
        holder.price.setText(context.getString(R.string.currency_icon)+" "+ String.valueOf(ItemsItem.getSubitemTotalPrice()));
    }

    @Override
    public int getItemCount() {
        return itemslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_qty,price;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            txt_qty=itemView.findViewById(R.id.txt_qty);
            price=itemView.findViewById(R.id.price);
        }
    }
}