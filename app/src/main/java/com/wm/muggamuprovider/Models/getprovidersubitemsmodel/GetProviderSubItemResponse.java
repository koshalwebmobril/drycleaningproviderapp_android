package com.wm.muggamuprovider.Models.getprovidersubitemsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetProviderSubItemResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("subitemDetails")
	private List<SubitemDetailsModel> subitemDetails;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<SubitemDetailsModel> getSubitemDetails(){
		return subitemDetails;
	}

	public boolean isError(){
		return error;
	}
}