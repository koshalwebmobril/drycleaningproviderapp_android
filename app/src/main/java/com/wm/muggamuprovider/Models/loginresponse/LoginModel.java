package com.wm.muggamuprovider.Models.loginresponse;

import com.google.gson.annotations.SerializedName;

public class LoginModel {

	@SerializedName("gender")
	private Object gender;

	@SerializedName("date_of_birth")
	private Object dateOfBirth;

	@SerializedName("latitude")
	private Object latitude;

	@SerializedName("open_time")
	private String openTime;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("profile_image_path")
	private Object profileImagePath;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("type")
	private int type;

	@SerializedName("is_admin")
	private int isAdmin;

	@SerializedName("profile_image_name")
	private Object profileImageName;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("document_uploaded")
	private String documentUploaded;

	@SerializedName("longitude")
	private Object longitude;

	@SerializedName("address")
	private String address;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("verified")
	private int verified;

	@SerializedName("email_verified_at")
	private Object emailVerifiedAt;

	@SerializedName("close_time")
	private String closeTime;

	@SerializedName("document_verified")
	private String documentVerified;

	@SerializedName("token")
	private String token;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("name")
	private String name;

	@SerializedName("category_id")
	private String category_id;

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	@SerializedName("is_approved")
	private int isApproved;

	@SerializedName("status")
	private int status;

	public Object getGender(){
		return gender;
	}

	public Object getDateOfBirth(){
		return dateOfBirth;
	}

	public Object getLatitude(){
		return latitude;
	}

	public String getOpenTime(){
		return openTime;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public Object getProfileImagePath(){
		return profileImagePath;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public int getType(){
		return type;
	}

	public int getIsAdmin(){
		return isAdmin;
	}

	public Object getProfileImageName(){
		return profileImageName;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public String getDocumentUploaded(){
		return documentUploaded;
	}

	public Object getLongitude(){
		return longitude;
	}

	public String getAddress(){
		return address;
	}

	public String getMobile(){
		return mobile;
	}

	public int getVerified(){
		return verified;
	}

	public Object getEmailVerifiedAt(){
		return emailVerifiedAt;
	}

	public String getCloseTime(){
		return closeTime;
	}

	public String getDocumentVerified(){
		return documentVerified;
	}

	public String getToken(){
		return token;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public String getName(){
		return name;
	}

	public int getIsApproved(){
		return isApproved;
	}

	public int getStatus(){
		return status;
	}
}