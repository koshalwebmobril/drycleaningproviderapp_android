package com.wm.muggamuprovider.Models.timelistmodel;

import com.wm.muggamuprovider.Models.timeitems.TimeItem;

import java.util.List;

public class TimeListItem {

    List<TimeItem> gridList;
    public TimeListItem(List<TimeItem> gridTimeList) {
        this.gridList=gridTimeList;
    }

    public List<TimeItem> getGridList() {
        return gridList;
    }

    public void setGridList(List<TimeItem> gridList) {
        this.gridList = gridList;
    }

}
