package com.wm.muggamuprovider.Models.uploaddocumentmodel;

import com.google.gson.annotations.SerializedName;

public class UploadDocumentResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}