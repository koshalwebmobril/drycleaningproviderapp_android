package com.wm.muggamuprovider.Models.getdocumentmodel;

import com.google.gson.annotations.SerializedName;

public class DocumentsItemModel {

	@SerializedName("path")
	private String path;

	@SerializedName("size")
	private int size;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("mime_type")
	private String mimeType;

	@SerializedName("name")
	private String name;

	@SerializedName("is_approved")
	private int isApproved;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private int status;

	public String getPath(){
		return path;
	}

	public int getSize(){
		return size;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getUserId(){
		return userId;
	}

	public String getMimeType(){
		return mimeType;
	}

	public String getName(){
		return name;
	}

	public int getIsApproved(){
		return isApproved;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public int getStatus(){
		return status;
	}
}