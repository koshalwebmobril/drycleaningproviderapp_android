package com.wm.muggamuprovider.Models.getbookingmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetBookingResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("bookings")
	private List<BookingsItemModel> bookings;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}

	public List<BookingsItemModel> getBookings(){
		return bookings;
	}
}