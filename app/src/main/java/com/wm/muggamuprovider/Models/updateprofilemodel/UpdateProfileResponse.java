package com.wm.muggamuprovider.Models.updateprofilemodel;

import com.google.gson.annotations.SerializedName;

public class UpdateProfileResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("providerInfo")
	private GetProfileModel getprofilemodel;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}

	public GetProfileModel getprofilemodel(){
		return getprofilemodel;
	}
}