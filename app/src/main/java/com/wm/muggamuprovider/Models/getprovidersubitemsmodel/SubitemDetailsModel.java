package com.wm.muggamuprovider.Models.getprovidersubitemsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamuprovider.Models.chooseclothesmodel.ItemsItem;

public class SubitemDetailsModel {
	@SerializedName("id")
	private int id;

	@SerializedName("category")
	private String category;

	@SerializedName("items")
	private List<ItemsItem> items;

	public int getId(){
		return id;
	}

	public String getCategory(){
		return category;
	}

	public List<ItemsItem> getItems(){
		return items;
	}




}