package com.wm.muggamuprovider.Models.bidrejectedmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamuprovider.Models.bidacceptmodel.AllPostItemsModel;

public class BidRejectedResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("rejectedBids")
	private List<AllPostItemsModel> rejectedBids;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<AllPostItemsModel> getRejectedBids(){
		return rejectedBids;
	}

	public boolean isError(){
		return error;
	}
}