package com.wm.muggamuprovider.Models.chooseclothesmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ItemsItem{

	@SerializedName("provider_subitem")
	private List<ProviderSubitemPriceModel> providerSubitem;

	@SerializedName("item_name")
	private String itemName;

	@SerializedName("id")
	private int id;

	@SerializedName("item_category")
	private String itemCategory;

	@SerializedName("status")
	private int status;

	private boolean isSelected = false;

	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean selected) {
		isSelected = selected;
	}


	public List<ProviderSubitemPriceModel> getProviderSubitem(){
		return providerSubitem;
	}

	public String getItemName(){
		return itemName;
	}

	public int getId(){
		return id;
	}

	public String getItemCategory(){
		return itemCategory;
	}

	public int getStatus(){
		return status;
	}
}