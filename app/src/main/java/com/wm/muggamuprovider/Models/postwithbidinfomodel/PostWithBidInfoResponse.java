package com.wm.muggamuprovider.Models.postwithbidinfomodel;

import com.google.gson.annotations.SerializedName;

public class PostWithBidInfoResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("providerBidResult")
	private PostWithBidInfoModel postWithBidInfoModel;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public PostWithBidInfoModel getPostWithBidInfoModel(){
		return postWithBidInfoModel;
	}
}