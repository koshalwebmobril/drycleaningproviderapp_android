package com.wm.muggamuprovider.Models.updateprofilemodel;

import com.google.gson.annotations.SerializedName;

public class GetProfileResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("providerInfo")
	private GetProfileModel getProfileModel;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public GetProfileModel getGetProfileModel(){
		return getProfileModel;
	}
}