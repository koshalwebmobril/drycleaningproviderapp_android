package com.wm.muggamuprovider.Models.getservicemodel;

import com.google.gson.annotations.SerializedName;

public class ProviderServicesItem{

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("service_name")
	private String serviceName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private int status;

	public int getCategoryId(){
		return categoryId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getServiceName(){
		return serviceName;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public int getStatus(){
		return status;
	}
}