package com.wm.muggamuprovider.Models.bookingdetailsmodel;

import com.google.gson.annotations.SerializedName;

public class BookingDetailsModel {

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("user_image")
	private String userImage;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("delivery_fee")
	private double deliveryFee;

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("user_mobile")
	private String userMobile;

	@SerializedName("is_provider_accepted")
	private int isProviderAccepted;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("booking_status")
	private int booking_status;

	@SerializedName("id")
	private int id;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("order_id")
	private String orderId;


	@SerializedName("user_device_token")
	private String user_device_token;

	public String getPickupLocation(){
		return pickupLocation;
	}

	public String getUserImage(){
		return userImage;
	}

	public String getUserName(){
		return userName;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public double getDeliveryFee(){
		return deliveryFee;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}


	public String getUser_device_token(){
		return user_device_token;
	}

	public int getUserId(){
		return userId;
	}



	public int getBooking_status(){
		return booking_status;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public String getUserMobile(){
		return userMobile;
	}

	public int getIsProviderAccepted(){
		return isProviderAccepted;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getOrderId(){
		return orderId;
	}
}