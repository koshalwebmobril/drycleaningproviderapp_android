package com.wm.muggamuprovider.Models.getCategorymodel;

import com.google.gson.annotations.SerializedName;

public class CategoriesItem{

	@SerializedName("thumbnail_path")
	private String thumbnailPath;

	@SerializedName("thumbnail_name")
	private String thumbnailName;

	@SerializedName("details")
	private String details;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	@SerializedName("type")
	private int type;

	@SerializedName("thumbnail_mime")
	private String thumbnailMime;

	@SerializedName("thumbnail_size")
	private int thumbnailSize;

	@SerializedName("status")
	private int status;

	public String getThumbnailPath(){
		return thumbnailPath;
	}

	public String getThumbnailName(){
		return thumbnailName;
	}

	public String getDetails(){
		return details;
	}

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}

	public int getType(){
		return type;
	}

	public String getThumbnailMime(){
		return thumbnailMime;
	}

	public int getThumbnailSize(){
		return thumbnailSize;
	}

	public int getStatus(){
		return status;
	}
}