package com.wm.muggamuprovider.Models.registermodel;

import com.google.gson.annotations.SerializedName;
import com.wm.muggamuprovider.Models.updateprofilemodel.GetProfileModel;
import com.wm.muggamuprovider.Models.forgotpasswordmodel.ForgotPasswordResponse;

public class ResponseAuthentication
{
	@SerializedName("result")
	private RegisterResponse registerResponse;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("category_id")
	private String category_id;

	@SerializedName("message")
	private String message;

	@SerializedName("providerInfo")
	private GetProfileModel getProfileModel;

	@SerializedName("forgotPasswordResult")
	private ForgotPasswordResponse forgotPasswordResponse;

	public RegisterResponse getRegisterResponse(){
		return registerResponse;
	}

	public GetProfileModel getResultLogin()
	{
		return getProfileModel;
	}

	public ForgotPasswordResponse getForgotPasswordResponse()
	{
		return forgotPasswordResponse;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}





}