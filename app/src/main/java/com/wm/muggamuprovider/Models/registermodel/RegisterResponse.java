package com.wm.muggamuprovider.Models.registermodel;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

	@SerializedName("address")
	private String address;

	@SerializedName("name")
	private String name;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("otp")
	private int otp;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	@SerializedName("category_id")
	private String category_id;

	@SerializedName("email")
	private String email;

	@SerializedName("token")
	private String token;

	public String getAddress(){
		return address;
	}

	public String getName(){
		return name;
	}

	public String getMobile(){
		return mobile;
	}

	public int getOtp(){
		return otp;
	}

	public int getId(){
		return id;
	}

	public String getType(){
		return type;
	}

	public String getEmail(){
		return email;
	}

	public String getToken(){
		return token;
	}
}