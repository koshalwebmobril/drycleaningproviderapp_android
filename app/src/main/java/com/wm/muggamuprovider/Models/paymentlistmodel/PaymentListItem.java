package com.wm.muggamuprovider.Models.paymentlistmodel;

import com.google.gson.annotations.SerializedName;

public class PaymentListItem{

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("user_profile_image")
	private Object userProfileImage;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("delivery_time")
	private String deliveryTime;

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public Object getUserProfileImage(){
		return userProfileImage;
	}

	public int getUserId(){
		return userId;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public String getUserName(){
		return userName;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}
}