package com.wm.muggamuprovider.Models.getbookingmodel;

import com.google.gson.annotations.SerializedName;

public class BookingsItemModel {

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("user_profile_image")
	private Object userProfileImage;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("user_mobile")
	private String userMobile;

	@SerializedName("is_provider_accepted")
	private int isProviderAccepted;

	@SerializedName("booked_at")
	private String bookedAt;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("order_id")
	private String orderId;

	public String getPickupLocation(){
		return pickupLocation;
	}

	public String getUserName(){
		return userName;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public Object getUserProfileImage(){
		return userProfileImage;
	}

	public int getUserId(){
		return userId;
	}

	public String getUserMobile(){
		return userMobile;
	}

	public int getIsProviderAccepted(){
		return isProviderAccepted;
	}

	public String getBookedAt(){
		return bookedAt;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getOrderId(){
		return orderId;
	}
}