package com.wm.muggamuprovider.Models.availabilitymanagmentmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AvailabilityManagmentResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("timeSlots")
	private List<TimeSlotsItem> timeSlots;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<TimeSlotsItem> getTimeSlots(){
		return timeSlots;
	}

	public boolean isError(){
		return error;
	}
}