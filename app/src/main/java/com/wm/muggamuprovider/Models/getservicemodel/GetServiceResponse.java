package com.wm.muggamuprovider.Models.getservicemodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamuprovider.Models.getserviceslistmodel.ProviderServicesItemModel;

public class GetServiceResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("providerServices")
	private List<ProviderServicesItemModel> providerServices;

	@SerializedName("error")
	private boolean error;
	@SerializedName("message")
	private String message;


	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}

	public List<ProviderServicesItemModel> getProviderServices(){
		return providerServices;
	}

	public boolean isError(){
		return error;
	}
}