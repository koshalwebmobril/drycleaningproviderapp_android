package com.wm.muggamuprovider.Models.paymentlistmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PaymentListResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("message")
	private String message;

	@SerializedName("error")
	private boolean error;

	@SerializedName("paymentList")
	private List<PaymentListItem> paymentList;

	public int getCode(){
		return code;
	}

	public String getMessage()
	{
		return message;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public boolean isError(){
		return error;
	}

	public List<PaymentListItem> getPaymentList(){
		return paymentList;
	}
}