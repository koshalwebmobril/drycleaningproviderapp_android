package com.wm.muggamuprovider.Models.updateprofilemodel;

import com.google.gson.annotations.SerializedName;

public class DaysItemListModel {

	@SerializedName("dayname")
	private String dayname;

	@SerializedName("value")
	private int value;

	public String getDayname(){
		return dayname;
	}

	public int getValue(){
		return value;
	}

	private boolean isSelected = false;

	public boolean isSelected() {
		return isSelected;
	}

	public void setValue(int value) {
		this.value = value;
	}
}