package com.wm.muggamuprovider.Models.getdocumentmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetDocumentResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("providerDocuments")
	private List<DocumentsItemModel> providerDocuments;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<DocumentsItemModel> getProviderDocuments(){
		return providerDocuments;
	}

	public boolean isError(){
		return error;
	}
}