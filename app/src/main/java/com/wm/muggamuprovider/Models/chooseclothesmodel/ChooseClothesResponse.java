package com.wm.muggamuprovider.Models.chooseclothesmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ChooseClothesResponse
{
	@SerializedName("code")
	private int code;

	@SerializedName("subitemList")
	private List<SubitemListItem> subitemList;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<SubitemListItem> getSubitemList(){
		return subitemList;
	}

	public boolean isError(){
		return error;
	}
}