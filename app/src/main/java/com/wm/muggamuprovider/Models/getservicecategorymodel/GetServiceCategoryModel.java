package com.wm.muggamuprovider.Models.getservicecategorymodel;

import com.google.gson.annotations.SerializedName;

public class GetServiceCategoryModel {

	@SerializedName("thumbnail_path")
	private String thumbnailPath;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("thumbnail_name")
	private String thumbnailName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("details")
	private String details;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	@SerializedName("thumbnail_mime")
	private String thumbnailMime;



	@SerializedName("thumbnail_size")
	private int thumbnailSize;

	@SerializedName("status")
	private int status;

	public String getThumbnailPath(){
		return thumbnailPath;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getThumbnailName(){
		return thumbnailName;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDetails(){
		return details;
	}

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}



	public String getThumbnailMime(){
		return thumbnailMime;
	}

	public int getThumbnailSize(){
		return thumbnailSize;
	}

	public int getStatus(){
		return status;
	}
}