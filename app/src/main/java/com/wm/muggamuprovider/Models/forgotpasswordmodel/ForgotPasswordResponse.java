package com.wm.muggamuprovider.Models.forgotpasswordmodel;

import com.google.gson.annotations.SerializedName;

public class ForgotPasswordResponse
{
	@SerializedName("otp")
	private int otp;

	public int getOtp()
	{
		return otp;
	}
}