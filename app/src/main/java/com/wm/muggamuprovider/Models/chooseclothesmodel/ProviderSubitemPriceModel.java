package com.wm.muggamuprovider.Models.chooseclothesmodel;

import com.google.gson.annotations.SerializedName;

public class ProviderSubitemPriceModel
{

	public ProviderSubitemPriceModel(float price) {
		this.price=price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@SerializedName("price")
	private float price;

	@SerializedName("service_id")
	private int serviceId;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("subitem_id")
	private int subitemId;

	@SerializedName("status")
	private int status;

	public float getPrice(){
		return price;
	}

	public int getServiceId(){
		return serviceId;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public int getSubitemId(){
		return subitemId;
	}

	public int getStatus(){
		return status;
	}
}