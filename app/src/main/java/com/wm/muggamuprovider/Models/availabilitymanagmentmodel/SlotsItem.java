package com.wm.muggamuprovider.Models.availabilitymanagmentmodel;

import com.google.gson.annotations.SerializedName;

public class SlotsItem{

	@SerializedName("id")
	private int id;

	@SerializedName("slot")
	private String slot;

	@SerializedName("status")
	private int status;

	@SerializedName("availability_status")
	private String availability_status;

	public String getAvailability_status()
	{
		return availability_status;
	}

	public void setAvailability_status(String availability_status) {
		this.availability_status = availability_status;
	}

	private boolean isSelected = false;

	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean selected) {
		isSelected = selected;
	}


	public int getId(){
		return id;
	}

	public String getSlot(){
		return slot;
	}

	public int getStatus(){
		return status;
	}
}