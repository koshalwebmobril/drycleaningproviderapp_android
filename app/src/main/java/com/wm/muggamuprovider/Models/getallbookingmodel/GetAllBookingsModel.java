package com.wm.muggamuprovider.Models.getallbookingmodel;

import com.google.gson.annotations.SerializedName;

public class GetAllBookingsModel {

	@SerializedName("cancel_reason_id")
	private Object cancelReasonId;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("delivery_fee")
	private double deliveryFee;

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("user_mobile")
	private String userMobile;

	@SerializedName("picked_up_at")
	private Object pickedUpAt;

	@SerializedName("service_id")
	private int serviceId;

	@SerializedName("id")
	private int id;

	@SerializedName("confirmed_at")
	private Object confirmedAt;

	@SerializedName("delivered_at")
	private Object deliveredAt;

	@SerializedName("shipped_at")
	private Object shippedAt;

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("service_name")
	private String serviceName;

	@SerializedName("pickup_driver_id")
	private Object pickupDriverId;

	@SerializedName("payment_status")
	private int paymentStatus;

	@SerializedName("booking_status")
	private String bookingStatus;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("in_process")
	private Object inProcess;

	@SerializedName("booked_at")
	private String bookedAt;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("delivery_driver_id")
	private Object deliveryDriverId;


	@SerializedName("user_profile_image")
	private String userprofile;

	public Object getCancelReasonId(){
		return cancelReasonId;
	}

	public String getUserName(){
		return userName;
	}

	public String getUserProfile(){
		return userprofile;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public double getDeliveryFee(){
		return deliveryFee;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getUserMobile(){
		return userMobile;
	}

	public Object getPickedUpAt(){
		return pickedUpAt;
	}

	public int getServiceId(){
		return serviceId;
	}

	public int getId(){
		return id;
	}

	public Object getConfirmedAt(){
		return confirmedAt;
	}

	public Object getDeliveredAt(){
		return deliveredAt;
	}

	public Object getShippedAt(){
		return shippedAt;
	}

	public String getPickupLocation(){
		return pickupLocation;
	}

	public String getServiceName(){
		return serviceName;
	}

	public Object getPickupDriverId(){
		return pickupDriverId;
	}

	public int getPaymentStatus(){
		return paymentStatus;
	}

	public String getBookingStatus(){
		return bookingStatus;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public int getUserId(){
		return userId;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public Object getInProcess(){
		return inProcess;
	}

	public String getBookedAt(){
		return bookedAt;
	}

	public int getProviderId(){
		return providerId;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getOrderId(){
		return orderId;
	}

	public Object getDeliveryDriverId(){
		return deliveryDriverId;
	}
}