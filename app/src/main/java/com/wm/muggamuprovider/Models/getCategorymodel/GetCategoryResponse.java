package com.wm.muggamuprovider.Models.getCategorymodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetCategoryResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	public boolean isError(){
		return error;
	}
}