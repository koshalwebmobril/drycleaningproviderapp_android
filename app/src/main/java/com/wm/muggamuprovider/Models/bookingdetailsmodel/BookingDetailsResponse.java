package com.wm.muggamuprovider.Models.bookingdetailsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BookingDetailsResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("itemDetails")
	private List<BookingDetailsParentModel> itemDetails;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("bookingDetails")
	private BookingDetailsModel bookingDetailsModel;

	public int getCode(){
		return code;
	}

	public List<BookingDetailsParentModel> getItemDetails(){
		return itemDetails;
	}

	public boolean isError(){
		return error;
	}

	public BookingDetailsModel getBookingDetailsModel(){
		return bookingDetailsModel;
	}
}