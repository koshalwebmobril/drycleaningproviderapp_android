package com.wm.muggamuprovider.Models.getserviceslistmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamuprovider.Models.getservicemodel.ProviderServicesItem;

public class GetServiceListResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("providerServices")
	private List<ProviderServicesItem> providerServices;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<ProviderServicesItem> getProviderServices(){
		return providerServices;
	}

	public boolean isError(){
		return error;
	}
}