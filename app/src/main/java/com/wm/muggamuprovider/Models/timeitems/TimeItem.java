package com.wm.muggamuprovider.Models.timeitems;

public class TimeItem {

    String time;

    int timeStatus;

    public TimeItem(String time, int timeStatus) {
        this.time = time;
        this.timeStatus = timeStatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getTimeStatus() {
        return timeStatus;
    }

    public void setTimeStatus(int timeStatus) {
        this.timeStatus = timeStatus;
    }


}
