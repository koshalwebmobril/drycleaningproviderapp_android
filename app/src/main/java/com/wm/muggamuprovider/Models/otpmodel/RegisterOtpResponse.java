package com.wm.muggamuprovider.Models.otpmodel;

import com.google.gson.annotations.SerializedName;

public class RegisterOtpResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("category_id")
	private String category_id;

	public String getCategory_id()
	{
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	@SerializedName("providerInfo")
	private ProviderInfo providerInfo;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}

	public ProviderInfo getProviderInfo(){
		return providerInfo;
	}
}