package com.wm.muggamuprovider.Models.getreviewmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class GetReviewResponse{

	@SerializedName("reviewList")
	private List<ReviewListItem> reviewList;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;


	@SerializedName("message")
	private String  message;

	public List<ReviewListItem> getReviewList(){
		return reviewList;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}