package com.wm.muggamuprovider.Models.loginresponse;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("providerInfo")
	private LoginModel loginModel;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}

	public LoginModel getLoginModel(){
		return loginModel;
	}
}