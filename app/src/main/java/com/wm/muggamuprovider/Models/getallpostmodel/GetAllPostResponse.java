package com.wm.muggamuprovider.Models.getallpostmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamuprovider.Models.bidacceptmodel.AllPostItemsModel;

public class GetAllPostResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("postResult")
	private List<AllPostItemsModel> postResult;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<AllPostItemsModel> getPostResult(){
		return postResult;
	}

	public boolean isError(){
		return error;
	}
}