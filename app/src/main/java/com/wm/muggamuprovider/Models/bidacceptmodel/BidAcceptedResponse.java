package com.wm.muggamuprovider.Models.bidacceptmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BidAcceptedResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("acceptedBids")
	private List<AllPostItemsModel> acceptedBids;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<AllPostItemsModel> getAcceptedBids(){
		return acceptedBids;
	}

	public boolean isError(){
		return error;
	}
}