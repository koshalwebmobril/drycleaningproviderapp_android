package com.wm.muggamuprovider.Models.postwithbidinfomodel;

import com.google.gson.annotations.SerializedName;

public class PostWithBidInfoModel {

	@SerializedName("user_action")
	private Object userAction;

	@SerializedName("post_id")
	private int postId;

	@SerializedName("bid_amount")
	private float bidAmount;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("accepted_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private int status;

	public Object getUserAction(){
		return userAction;
	}

	public int getPostId(){
		return postId;
	}

	public float getBidAmount(){
		return bidAmount;
	}

	public int getProviderId(){
		return providerId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public int getStatus(){
		return status;
	}
}