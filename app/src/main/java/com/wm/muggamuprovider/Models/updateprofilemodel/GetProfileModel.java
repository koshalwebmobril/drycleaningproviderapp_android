package com.wm.muggamuprovider.Models.updateprofilemodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetProfileModel {

	@SerializedName("gender")
	private Object gender;

	@SerializedName("date_of_birth")
	private Object dateOfBirth;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("open_time")
	private String openTime;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("profile_image_path")
	private Object profileImagePath;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("type")
	private int type;

	@SerializedName("is_admin")
	private int isAdmin;

	@SerializedName("profile_image_name")
	private String profileImageName;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("document_uploaded")
	private String documentUploaded;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("address")
	private String address;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("verified")
	private int verified;

	@SerializedName("email_verified_at")
	private Object emailVerifiedAt;

	@SerializedName("close_time")
	private String closeTime;

	@SerializedName("document_verified")
	private String documentVerified;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("name")
	private String name;

	@SerializedName("is_approved")
	private int isApproved;

	@SerializedName("days")
	private List<DaysItemListModel> days;

	@SerializedName("status")
	private int status;

	@SerializedName("token")
	private String token;

	public Object getGender(){
		return gender;
	}

	public Object getDateOfBirth(){
		return dateOfBirth;
	}

	public Object getLatitude(){
		return latitude;
	}

	public String getOpenTime(){
		return openTime;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getProfileImagePath(){
		return (String) profileImagePath;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public int getType(){
		return type;
	}

	public int getIsAdmin(){
		return isAdmin;
	}

	public Object getProfileImageName(){
		return profileImageName;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public String getDocumentUploaded(){
		return documentUploaded;
	}

	public Object getLongitude(){
		return longitude;
	}

	public String getAddress(){
		return address;
	}

	public String getMobile(){
		return mobile;
	}

	public int getVerified(){
		return verified;
	}

	public Object getEmailVerifiedAt(){
		return emailVerifiedAt;
	}

	public String getCloseTime(){
		return closeTime;
	}

	public String getDocumentVerified(){
		return documentVerified;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public String getName(){
		return name;
	}

	public int getIsApproved(){
		return isApproved;
	}

	public List<DaysItemListModel> getDays(){
		return days;
	}

	public int getStatus(){
		return status;
	}

	public String getToken(){
		return token;
	}
}