package com.wm.muggamuprovider.Models.GetAllAppoiments;

import com.google.gson.annotations.SerializedName;

public class AppointmentsItem
{

	@SerializedName("date")
	private String date;

	@SerializedName("provider_mobile")
	private String providerMobile;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("appointment_id")
	private String appointmentId;

	@SerializedName("service_name")
	private String service_name;

	@SerializedName("user_profile_image")
	private Object userProfileImage;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("user_mobile")
	private String userMobile;

	@SerializedName("booked_at")
	private String bookedAt;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("time")
	private String time;

	@SerializedName("provider_profile_image")
	private String providerProfileImage;


	@SerializedName("user_device_token")
	private String user_device_token;

	@SerializedName("provider_name")
	private String providerName;

	@SerializedName("user_address")
	private String useraddress;




	@SerializedName("is_provider_accepted")
	private int isProviderAccepted;

	@SerializedName("appointment_status")
	private int appointment_status;

	public int getAppointment_status() {
		return appointment_status;
	}

	public String getDate(){
		return date;
	}

	public String getProviderMobile(){
		return providerMobile;
	}

	public String getUserName(){
		return userName;
	}

	public String getAppointmentId(){
		return appointmentId;
	}


	public String getUser_device_token(){
		return user_device_token;
	}

	public Object getUserProfileImage(){
		return userProfileImage;
	}

	public int getUserId(){
		return userId;
	}

	public String getUserMobile(){
		return userMobile;
	}

	public String getBookedAt(){
		return bookedAt;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public String getTime(){
		return time;
	}

	public String getProviderProfileImage(){
		return providerProfileImage;
	}

	public String getProviderName(){
		return providerName;
	}

	public String getAddress(){
		return useraddress;
	}

	public String getService_name(){
		return service_name;
	}

	public int getIsProviderAccepted(){
		return isProviderAccepted;
	}
}