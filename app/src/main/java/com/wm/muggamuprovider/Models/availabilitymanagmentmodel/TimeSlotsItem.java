package com.wm.muggamuprovider.Models.availabilitymanagmentmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TimeSlotsItem{

	@SerializedName("slots")
	private List<SlotsItem> slots;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;




	public List<SlotsItem> getSlots(){
		return slots;
	}

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}
}