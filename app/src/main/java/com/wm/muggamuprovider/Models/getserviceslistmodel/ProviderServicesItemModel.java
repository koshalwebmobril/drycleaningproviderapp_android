package com.wm.muggamuprovider.Models.getserviceslistmodel;

import com.google.gson.annotations.SerializedName;

public class ProviderServicesItemModel {

	@SerializedName("service_image_path")
	private String thumbnailPath;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("service_image_name")
	private String thumbnailName;

	@SerializedName("service_name")
	private String name;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("thumbnail_mime")
	private String thumbnailMime;

	@SerializedName("thumbnail_size")
	private int thumbnailSize;

	@SerializedName("status")
	private int status;

	@SerializedName("service_id")
	private int serviceId;

	public String getThumbnailPath(){
		return thumbnailPath;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public int getServiceId(){
		return serviceId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getThumbnailName(){
		return thumbnailName;
	}

	public String getName(){
		return name;
	}

	public int getProviderId(){
		return providerId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public String getThumbnailMime(){
		return thumbnailMime;
	}

	public int getThumbnailSize(){
		return thumbnailSize;
	}

	public int getStatus(){
		return status;
	}
}