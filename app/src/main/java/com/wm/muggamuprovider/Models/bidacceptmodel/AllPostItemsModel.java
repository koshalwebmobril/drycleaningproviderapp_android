package com.wm.muggamuprovider.Models.bidacceptmodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllPostItemsModel {

	@SerializedName("service_time")
	private String serviceTime;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("service_date")
	private String serviceDate;

	@SerializedName("service_id")
	private int serviceId;

	@SerializedName("service_name")
	private String name;

	@SerializedName("user_name")
	private String user_name;

	@SerializedName("id")
	private int id;

	@SerializedName("post_attachment")
	private String postAttachment;

	@SerializedName("service_address")
	private String serviceAddress;

	@SerializedName("remarks")
	private String remarks;

	@SerializedName("profile_image")
	private String profile_image;

	@SerializedName("is_bid_done")
	private String is_bid_done;


	@SerializedName("created_at")
	private String created_at;


	@SerializedName("status")
	private int status;

	public double getBidAmount(){
		return bidAmount;
	}

	@SerializedName("post_details")
	private List<PostDetailsItem> postDetails;


	@SerializedName("bid_amount")
	private double bidAmount;

	public String getServiceTime(){
		return serviceTime;
	}

	public int getUserId(){
		return userId;
	}

	public String getServiceDate(){
		return serviceDate;
	}

	public int getServiceId(){
		return serviceId;
	}

	public String getName(){
		return name;
	}

	public String getUser_name(){
		return user_name;
	}


	public int getId(){
		return id;
	}

	public String getPostAttachment(){
		return postAttachment;
	}

	public String getServiceAddress(){
		return serviceAddress;
	}

	public String getRemarks(){
		return remarks;
	}

	public String getProfile_image(){
		return profile_image;
	}


	public String getCreated_at(){
		return created_at;
	}

	public String getIs_bid_done()
	{
		return is_bid_done;
	}

	public int getStatus(){
		return status;
	}

	public List<PostDetailsItem> getPostDetails(){
		return postDetails;
	}

}