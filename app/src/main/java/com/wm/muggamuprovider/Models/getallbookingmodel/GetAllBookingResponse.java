package com.wm.muggamuprovider.Models.getallbookingmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetAllBookingResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("bookings")
	private List<GetAllBookingsModel> bookings;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public List<GetAllBookingsModel> getBookings(){
		return bookings;
	}
}