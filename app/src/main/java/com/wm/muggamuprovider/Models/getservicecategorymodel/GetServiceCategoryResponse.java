package com.wm.muggamuprovider.Models.getservicecategorymodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetServiceCategoryResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("categories")
	private List<GetServiceCategoryModel> categories;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public List<GetServiceCategoryModel> getCategories(){
		return categories;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}