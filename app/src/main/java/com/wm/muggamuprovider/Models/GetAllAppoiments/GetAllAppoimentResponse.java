package com.wm.muggamuprovider.Models.GetAllAppoiments;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetAllAppoimentResponse{

	@SerializedName("appointments")
	private List<AppointmentsItem> appointments;

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;

	@SerializedName("error")
	private boolean error;

	public List<AppointmentsItem> getAppointments(){
		return appointments;
	}

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}

	public boolean isError(){
		return error;
	}
}