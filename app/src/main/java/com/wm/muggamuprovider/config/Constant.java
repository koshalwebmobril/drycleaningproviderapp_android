package com.wm.muggamuprovider.config;

public interface Constant {
    String PREFS_NAME = "mazad_data";
    String TAG_TOKEN = "firebase_token";
    String FRAGMENT_TYPE = "fragment_type";
    String RECENT_FIREBASE = "recents";
}
